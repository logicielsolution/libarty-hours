<?php 

ini_set('display_errors',"1");

if(!isset($_SESSION)) {
     session_start();
}

include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
include('Mailer.php');
include("Teacher.php");
include("Student.php");

$assignmentId = isset($_POST['assignment_id']) ? $_POST['assignment_id'] : null;
$submitType = isset($_POST['submit_type']) ? $_POST['submit_type'] : null;
$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$eventDate = isset($_POST['date']) ? $_POST['date'] : null;
$teacherId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

$teacherObj = new Teacher;
$studentObj = new Student;
$teacher = $teacherObj->getTeacherById($teacherId);
$student = $studentObj->getStudentById($studentId);


$email = $student->username . Common::LIBERTY_EMAIL_DOMAIN;
$enviroment = Common::APP_ENV;
if ($enviroment == 'local') {
	$email = Common::STUDENT_TEST_EMAIL;
}

$mail = new Mailer;
$mail->addAddress($email);

if ($submitType == Common::ASSIGNMENT_REQUETS_ALLOW) {
	$query = "UPDATE assignments SET unassigned = $1 WHERE id = $2";
		
	$result = pg_prepare($dbh, "update_query", $query);
	$result = pg_execute($dbh, "update_query", [0, $assignmentId]);
		
	if(!$result) {
		$response['error'] = true;
		$response['message'] = "Assignment request not accepted. Please try again.";

		echo json_encode($response);
	}

	updateAssignmentRequestStatus($dbh, Common::STATUS_ACCEPT, $studentId, $eventDate);

	$mail->sendAllowAssignmentRequestMailToStudent($student, $teacher, $eventDate);

	$successMsg = 'Assignment request accept successfully';

} else {
	$query = "DELETE from assignments WHERE id = $1";
		
	$result = pg_prepare($dbh, "delete_query", $query);
	$result = pg_execute($dbh, "delete_query", [$assignmentId]);
		
	if(!$result) {
		$response['error'] = true;
		$response['message'] = "Assignment request not denied. Please try again.";

		echo json_encode($response);
	}

	updateAssignmentRequestStatus($dbh, Common::STATUS_DENY, $studentId, $eventDate);

	$mail->sendDenyAssignmentRequestMailToStudent($student, $teacher, $eventDate);

	$successMsg = 'Assignment request denied successfully';
}

$response['error'] = false;
$response['message'] = $successMsg;

echo json_encode($response);


/**
 * update status in student request table for assignment
 * @param  connection  $dbh  database connection
 * @param  string  $status  accept/deny
 * @param  int  $studentId
 * @param  date  $date
 * @return bool
 */
function updateAssignmentRequestStatus($dbh, $status, $studentId, $date)
{
	$date = date("Y-m-d", strtotime($date));
	
	$query = "UPDATE student_requests SET status=$1 WHERE student_id=$2 AND date=$3";
	
	$result = pg_prepare($dbh, "update_student_request_query", $query);
	$result = pg_execute($dbh, "update_student_request_query", [$status, $studentId, $date]);
	
	if(!$result) {
		return false;
	}

	return true;
}



