<?php
ini_set('display_errors',"1");

if(!isset($_SESSION)) {
     session_start();
}

include("php/Authentication.php");
include("php/Constants.php");
include("php/js-constants.php");
include("dbConnection.php");
include("php/Assignment.php");
include("php/Reports.php");
include("php/clubs/Club.php");

function gen_header($title, $css="", $printcss="", $backLink="/", $jsscript="") {
    if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
        $libertyEagle = "Eagle" ;
    }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
        $libertyEagle = "Liberty";
    }elseif ($_SESSION['school_id'] != ""){
        $libertyEagle  = "Liberty/Eagle";
    }else{
        $libertyEagle = "No School";
    }
?>

    <!DOCTYPE html> 
    <html> 
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <meta name="description" content="">
            <meta name="author" content="">

            <link rel="icon" href="../../favicon.ico">
            <title><?php echo $title; ?></title>

            <!-- Bootstrap core CSS -->
            <link href="css/bootstrap.css" rel="stylesheet">
            <link href="css/jquery-ui.css" rel="stylesheet">
            <link href="css/genericfunctionstech.css" rel="stylesheet" type="text/css" />
            <link rel='stylesheet' href='css/jquery.qtip.min.css' />
            <link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
            <link rel="stylesheet" href="css/bootstrap-datepicker3.min.css">
            <link rel='stylesheet' href='css/fullcalendar.css' />
            <link href="css/select2.min.css" rel="stylesheet">
            <link rel='stylesheet' href='css/custom.css' />

            <!-- Bootstrap core JS -->
            <script src="https://code.jquery.com/jquery-latest.js"></script>
            <script src="js/jquery-ui.js"></script>

            <style>
              .actli{ background-color: #e7e7e7; color:#2b2b2b;  }
              .acta{ color:#2b2b2b !important;  }
            </style>

        </head>
        <body>
            <div id="loader"></div>
            <div id="blur-backgroung"></div>
            <div id="container">  
                <div id="header">
                    <nav class="navbar navbar-custom navbar-static-top">
                        <div class="container">
                            <div id="logo">
                                <?php 
                                if (isset($_SESSION['role']) 
                                        && ($_SESSION['role'] == Common::TEACHER 
                                        || $_SESSION['role'] == Common::STUDENT) ) 
                                {
                                ?>
                                    <a href="http://leh.liberty.k12.mo.us/" 
                                        target="_blank">
                                        <img src="img/LEH_logo.png" 
                                            alt="LPS " 
                                            border="0">
                                    </a>
                                <?php } else { ?>
                                    <a href="http://leh.liberty.k12.mo.us/" 
                                        target="_blank">
                                        <img src="img/liberty_logo.png" 
                                            alt="LPS " 
                                            border="0">
                                    </a>
                                <?php } ?>            
                                <?php if ($_SESSION['user_id'] != ""){ ?>
                                    <span id="login-status">
                                        Logged in as: <?php echo $_SESSION['full_user_name']; ?>
                                        <br> 
                                        <a href="logout.php">Log Out</a>
                                    </span>
                                <?php } ?>
                            </div>
                            <div class="navbar-header">
                                <button type="button" 
                                    class="navbar-toggle collapsed" 
                                    data-toggle="collapse" 
                                    data-target="#navbar" 
                                    aria-expanded="false" 
                                    aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">
                                    <?php 
                                        if ((isset($_SESSION['school_id']) && $_SESSION['school_id'] != "") 
                                          || (isset($_SESSION['school']) && $_SESSION['school'] != "")
                                          || (isset($_SESSION['role']))){ 

                                            echo "$libertyEagle Hour 2017-2018"; 
                                        } else { 
                                            echo "Liberty/Eagle Hour Login";
                                        }
                                    ?>
                                </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <?php if ($_SESSION['user_type'] == "student"){ ?>
                                        <li>
                                            <a href="/studentAssignments.php">Assignments</a>
                                        </li>
                                        <li>
                                            <a href="/clubs.php">Clubs</a>
                                        </li>
                                    <?php }elseif(Common::isTeacher()){ ?>
                                        <li>
                                            <a href="/assignment-list.php">Assignments</a>
                                        </li>
                                        <li>
                                            <a href="/teacherCalendar.php">
                                                Assign Students
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/absent-students.php">Absences</a>
                                        </li>  
                                        <li>
                                            <a href="/clubs.php">Clubs</a>
                                        </li>
                                        <?php
                                            $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
                                            $commonObj = new Common;
                                            if ($commonObj->isTeacherHasClub($userId)) { 
                                        ?>
                                            <li>
                                                <a href="club-report.php">Club Report</a>
                                            </li>
                                        <?php } ?>
                                    <?php }elseif(Common::isSuperAdmin() || Common::isAdmin()){ ?>
                                        <li>
                                            <a href="/assignment-list.php">Assignments</a>
                                        </li>
                                        <li>
                                            <a href="/assignStudents.php">
                                                Assign Students
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/absent-students.php">Absences</a>
                                        </li>
                                        <li>
                                            <a href="/clubs.php">Clubs</a>
                                        </li>
                                        <li>
                                            <a href="/blackout-days.php">Blackout Days</a>
                                        </li>
                                        <li>
                                            <a href="/student-requests.php">Student Requests History</a>
                                        </li>
                                        <li class="dropdown">

                                            <a href="#" 
                                                class="dropdown-toggle" 

                                                data-toggle="dropdown" 

                                                role="button" 

                                                aria-haspopup="true" 
                                                aria-expanded="false">

                                                More 
                                                <span class="caret"></span>

                                            </a>

                                            <ul class="dropdown-menu">

                                                <li>
                                                    <a href="detention-list.php">Detentions</a>
                                                </li>
                                                <li>
                                                    <a href="report-list.php">Assignments Report</a>
                                                </li>
                                                <li>
                                                    <a href="club-report.php">Club Report</a>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
                <div class="container">        
<?php

} // END gen_header

function gen_footer() {
    include("assign-teachers-popup.php");
    include("modal-popups.php");
?>

        </body>
    </html>
<?php

} // END gen_footer