<?php
session_start();

include("header.php");

?>
<?php function entry_html($title, $dbh) {
?>

<div class="">

    <?php include('php/clubs/club-header.php'); ?>

    <!-- Search teacher -->
    <div style="margin-top: 50px;">
       <h4><b>Clubs Request</b></h4>
    </div>

    <!--Start Table -->
     <div style="margin-top: 25px;">
        <table class="table table-bordered" style="margin-top: 20px;">
            <colgroup>
                <col style="width: 10%;" />
                <col style="width: 40%;" />
                <col style="width: 30%;" />
                <col style="width: 20%;" />
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Group Name</th>
                    <th>Student Name</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php

                    $groupbyFields = 'gr.id,gr.group_id,gr.student_id,gr.status.gr.date,gr.request_type';
                    $query="SELECT gr.*, g.name, s.full_name as student_name FROM student_requests as gr INNER JOIN groups as g ON gr.group_id = g.id INNER JOIN students as s ON gr.student_id = s.id INNER JOIN admin_to_group as atg ON gr.group_id = atg.group_id WHERE atg.admin_id = " . $_SESSION['user_id'] . " AND gr.request_type = '" . Common::GROUP_REQUETS . "'";

                    if (Common::isSuperAdmin()) {
                        $query="SELECT gr.*, g.name, s.full_name as student_name FROM student_requests as gr INNER JOIN groups as g ON gr.group_id = g.id INNER JOIN students as s ON gr.student_id = s.id WHERE gr.request_type = '" . Common::GROUP_REQUETS . "'";
                    }

                    $result = pg_query($dbh, $query);
                    if (!$result) {
                        echo "An error occurred.\n";
                        exit;
                    }

                    $groups = pg_fetch_all($result);

                    if ($groups) {
                        $showedList = [];
                        foreach ($groups as $key => $group) {

                            if(in_array($group['id'], $showedList)){
                                continue;
                            }

                            $showedList[] = $group['id'];
                            
                ?>
                    <tr>
                        <td class="assigned-admin-id"><?php echo $group['id']; ?></td>
                        <td><?php echo $group['name']; ?></td>
                        <td><?php echo $group['student_name']; ?></td>
                        <td class="text-center">
                            <?php if ($group['status'] == Common::STATUS_PENDING) { ?>
                                <a class="btn btn-primary btn-xs allow-request-btn" 
                                    data-group-id="<?php echo $group['group_id']; ?>"
                                    data-group-name="<?php echo $group['name']; ?>"
                                    data-confirmation-type="<?php echo Common::STATUS_ACCEPT; ?>"
                                    data-student-id="<?php echo $group['student_id']; ?>"
                                    style="cursor: pointer;">
                                    Allow
                                </a>
                                <a class="btn btn-danger btn-xs deny-request-btn" 
                                    data-group-id="<?php echo $group['group_id']; ?>"
                                    data-group-name="<?php echo $group['name']; ?>"
                                    data-confirmation-type="<?php echo Common::STATUS_DENY; ?>"
                                    data-student-id="<?php echo $group['student_id']; ?>"
                                    style="cursor: pointer;">
                                    Deny
                                </a>
                            <?php } ?>
                            <?php if ($group['status'] == Common::STATUS_ACCEPT) { ?>
                                <button class="btn btn-success btn-xs disabled">Accepted</button>
                            <?php } ?>
                            <?php if ($group['status'] == Common::STATUS_DENY) { ?>
                                <button class="btn btn-danger btn-xs disabled">Denied</button>
                            <?php } ?>
                        </td>
                    </tr>
                <?php 
                        }
                    } else {
                        echo "<tr><td colspan='4' class='text-center'>No Record Found</td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
    <!--End Table -->

</div>

<?php
}
?>


<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Club Request";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }

    include("footer.php");
?>