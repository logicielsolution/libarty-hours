<?php

/*

   Generic log in for the Technology directory
   Created by Brian Harmon
   02/09/2006

*/
ini_set('display_errors',"1");

session_start();

include("genericfunctionstech.php");
include("php/Constants.php");

if (isset($_POST['submit']) && $_POST['submit'] == "Log In") {
    /*$options = [
        'salt' => '1bbdb208b9ce5b54bf88589bbdb49170aeeb2653', //write your own code to generate a suitable salt
        'cost' => 10 // the default cost is 10
    ];*/
    $salt=substr('1bbdb208b9ce5b54bf88589bbdb49170aeeb2653',0,22);
    $cost=10;

    /* BEGIN LOG IN */
    $re = '/^(?=(?:.*[0-9]){4})\w+$/m';

    if(preg_match($re, $_POST['username'])){
        $_SESSION['username'] = $username = ereg_replace("[^a-zA-Z0-9]", '', $_POST['username']);
        $password = ereg_replace("[\'\"]", '', $_POST['password']);
        //$hash = password_hash($password, PASSWORD_DEFAULT, $options);
        $hash = crypt($password, '$2a$' . $cost . '$' . $salt);
        /*echo $hash;
        echo "<br/>";
        echo $newhash;
        echo "<br/>";
        echo $password;
        exit;*/
       
        $constr = "dbname=".Common::DB_NAME." user=".Common::DB_USER." password=".Common::DB_USER_PASSWORD." host=".Common::HOST;
        $dbh = pg_connect($constr) or die("Unable to connect to database\n\n");
        $SQL = "SELECT id, username, first, last, middle, building_id, full_name, sasiid, grade, telephone FROM students WHERE username='$username' AND passwordhash='$hash'";
        $SQLresult = pg_exec($dbh, $SQL);
   
        if(pg_numrows($SQLresult)) {
            $user_data = pg_fetch_row($SQLresult);
            $_SESSION['user_id'] = $user_data[0];
            $user_first = $user_data[2];
            $user_last = $user_data[3];
            $user_middle = $user_data[4];
            $_SESSION['username'] = $username;
            $_SESSION['user_first_name'] = $user_first;
            $_SESSION['full_user_name'] = $user_data[6];
            $_SESSION['login_attempted'] = 1;
            $_SESSION['school_id'] = $user_data[5];
            $_SESSION['grade'] = $user_data[8];
      
            $_SESSION['user_type'] = "student";
            $_SESSION['role'] = Common::STUDENT;
            
            if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
                $libertyEagle = "Eagle" ;
            }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
                $libertyEagle = "Liberty";
            }elseif ($_SESSION['school_id'] != ""){
                $libertyEagle  = "Liberty/Eagle";
            }else{
                $libertyEagle = "No School";
            }

            /*$buildingId = $_SESSION['school_id'];
            if($buildingId) {
                $query = "select * from schools where sasi_building_code = $buildingId";
                $schoolQuery = pg_exec($dbh, $query);
                $result = pg_fetch_assoc($schoolQuery);

                if($result) {
                    $_SESSION['school_id'] = $result['id'];
                }
                
            }*/
   
        } else { // Ooops, we're not in the db, try again
            gen_header("Liberty Eagle Hour - Log In Failure");
            ?>
            <span class="text-large-black" style="font-size: 20px; font-weight: bold;">
                ERROR
            </span>
            <br/>
            <br/>
            <div style="width: 600px; height: 400px; text-align: left;" 
                class="text-med-black">
                I'm sorry, either you do not have permission to access this page or I
                couldn't find you in the database based on the credentials that you sent me.<br/>
                <br/>
                Please use your browser's BACK button and try again. &nbsp;If you continue to have difficulty logging in, please contact
                the 
                <a href="mailto:webmaster@liberty.k12.mo.us" class="accentfeedback">
                    Webmaster
                </a>.
            </div>
            <?php
                session_unset();
                session_destroy();

                if(isset($dbh)) {    
                    pg_close($dbh);
                }
                exit;
        }
   
    } elseif (ctype_alpha($_POST['username'])){ 
        $_SESSION['username'] = $username = ereg_replace("[^a-zA-Z0-9]", '', $_POST['username']);
        $password = ereg_replace("[\'\"]", '', $_POST['password']);
        $hash = crypt($password, '$2a$' . $cost . '$' . $salt);
   
        $constr = "dbname=".Common::DB_NAME." user=".Common::DB_USER." password=".Common::DB_USER_PASSWORD." host=".Common::HOST;

        $dbh = pg_connect($constr) or die("Unable to connect to database\n\n");
   
        $SQL = "SELECT id, username, first, last, full_name, building_id, school_name FROM users WHERE username='$username' AND passwordhash='$hash'";

        $SQLresult = pg_exec($dbh, $SQL);
   
        //$user_data = pg_fetch_row($SQLresult);

        if(pg_numrows($SQLresult)) {
            $user_data = pg_fetch_row($SQLresult);

            $_SESSION['user_id'] = $user_data[0];
            $user_first = $user_data[2];
            $user_last = $user_data[3];
            $_SESSION['username'] = $username;
            $_SESSION['user_first_name'] = $user_first;
            $_SESSION['full_user_name'] = $user_data[4];
            $_SESSION['login_attempted'] = 1;
            $_SESSION['school_id'] = $user_data[5];
            $_SESSION['school_name'] = $user_data[6];

            $_SESSION['user_type'] = "teacher";


            if ( $_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
                $libertyEagle = "Eagle" ;
            }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
                $libertyEagle = "Liberty";
            }elseif ($_SESSION['school_id'] != ""){
                $libertyEagle  = "Liberty/Eagle";
            }else{
                $libertyEagle = "No School";
            }

            $commonClassObj = new Common;
            $commonClassObj->setUserRole($user_data);

        } else { // Ooops, we're not in the db, try again
            gen_header("Liberty Eagle Hour - Log In Failure");
            ?>
            <span class="text-large-black" style="font-size: 20px; font-weight: bold;">
                ERROR
            </span>
            <br/>
            <br/>
            <div style="width: 600px; height: 400px; text-align: left;" 
                class="text-med-black">
                I'm sorry, either you do not have permission to access this page or I
                couldn't find you in the database based on the credentials that you sent me.
                <br/>
                <br/>
                Please use your browser's BACK button and try again. &nbsp;If you continue to have difficulty logging in, please contact
                the 
                <a href="mailto:webmaster@liberty.k12.mo.us" class="accentfeedback">
                    Webmaster
                </a>.
            </div>
            <?php
            session_unset();
            session_destroy();
            if(isset($dbh)) {    
                pg_close($dbh);
            }
            exit;
        }

    } else { // Ooops, we're not in the db, try again
        gen_header("Liberty Eagle Hour - Log In Failure");
        ?>
        <span class="text-large-black" style="font-size: 20px; font-weight: bold;">
            ERROR
        </span>
        <br/>
        <br/>
        <div style="width: 600px; height: 400px; text-align: left;" 
            class="text-med-black">
            I'm sorry, either you do not have permission to access this page or I
            couldn't find you in the database based on the credentials that you sent me.
            <br/>
            <br/>
            Please use your browser's BACK button and try again. &nbsp;If you continue to have difficulty logging in, please contact
            the 
            <a href="mailto:webmaster@liberty.k12.mo.us" class="accentfeedback">
                Webmaster
            </a>.
        </div>
        <?php
        session_unset();
        session_destroy();
        if(isset($dbh)) {    
            pg_close($dbh);
        }
        exit;
    } // Done checking
   
    pg_close($dbh);
    if (isset($_GET['REQUEST_URI'])) {
        $header_location = $_GET['REQUEST_URI'];
    } else {
        $header_location = $_SERVER['PHP_SELF'];
    }
    
    header("Location: " . $header_location) or die("unable to set session");
    exit;
    /* END LOG IN */
}

if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") {
    header('Location: index.php');
}

gen_header("");
?>

<div class="modal-dialog">
    <div class="loginmodal-container">
        <h1>Enter LEH Login</h1><br>
        <form name="login" method="POST" action="login.php?REQUEST_URI=<?php echo urlencode($_SERVER['REQUEST_URI']); ?>">
            <input type="text" name="username" placeholder="Username">
            <input type="password" name="password" placeholder="Password">
            <input type="submit" name="submit" value="Log In" class="login loginmodal-submit">
        </form>
                    
        <div class="login-help">
            Having issues? Contact LPS Help Desk x7078
        </div>
    </div>
</div>
<script language="JavaScript">
<!--

document.login.username.focus();

//-->
</script>
