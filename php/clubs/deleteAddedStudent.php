<?php

session_start();

ini_set('display_errors',"1");
include("Club.php");

$id = isset($_POST['id']) ? $_POST['id'] : null;
$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$groupId = isset($_POST['group_id']) ? $_POST['group_id'] : null;

$clubObj = new Club;
$response = $clubObj->deleteAddedStudent($id, $groupId, $studentId);

echo $response;
exit;