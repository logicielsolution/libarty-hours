<?php
session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_STUDENT_REQUEST);

?>
<?php function entry_html($title, $dbh) {
?>

<div class="">

    <!-- Search teacher -->
    <div style="margin-top: 50px;">
       <h4><b>Student Requests</b></h4>
    </div>

    <hr>

    <!-- filter -->
     <form class="form-inline" style="margin-top: 20px;">
        <div class="row">
            
            <!-- Student name -->
            <div class="form-group col-sm-4">
                <label for="sName">Student Name:</label>
                <input type="text" 
                    class="form-control" 
                    id="sName"
                    name="sName"
                    value="<?php echo isset($_GET['sName']) ? $_GET['sName'] : null; ?>">
            </div>
        
            <!-- Request for -->
            <?php 
                $requestFor = [Common::ASSIGNMENT_REQUETS, Common::GROUP_REQUETS]; 
            ?>
            <div class="form-group col-sm-3">
                  <label for="sel1">Request For:</label>
                  <select class="form-control" id="sel1" name="request-for">
                        <option value="">---Select Option---</option>
                        <?php
                            $selectedRequestType = isset($_GET['request-for']) ? $_GET['request-for'] : null; 
                            foreach ($requestFor as $key => $rfor) { 
                        ?>
                            <option value="<?php echo $rfor; ?>"
                                <?php if($selectedRequestType == $rfor) {echo 'selected="selected"'; } ?>>
                                <?php echo ($rfor=="group")?"Club": ucfirst($rfor); ?>
                            </option>
                        <?php } ?>
                  </select>
            </div> 
            
            <div class="col-sm-offset-3 col-sm-2">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <a href="/student-requests.php" class="btn btn-default">Reset</a>
                </div>
            </div>
        </div>        
    </form>

    <!--Start Table -->
     <div style="margin-top: 25px;">
        <table class="table table-bordered" style="margin-top: 20px;">
            <colgroup>
                <col style="width: 10%;" />
                <col style="width: 30%;" />
                <col style="width: 20%;" />
                <col style="width: 20%;" />
                <col style="width: 20%;" />
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Student Name</th>
                    <th>Date</th>
                    <th>Request For</th>
                    <th class="text-center">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $commonObj = new Common;

                    $sName = isset($_GET['sName']) ? $_GET['sName'] : null;
                    $selectFor = isset($_GET['request-for']) ? $_GET['request-for'] : null;
                    
                    $buildingId = Common::getSchoolId();
                    $buildingId = $commonObj->getBuildingCodeById($buildingId);

                    $query="SELECT sr.*, g.name as group_name, s.full_name as student_name 
                            FROM student_requests as sr 
                            LEFT JOIN groups as g ON sr.group_id = g.id 
                            INNER JOIN students as s ON sr.student_id = s.id";

                    if ($buildingId) {
                        $query .= " AND s.building_id=$buildingId";
                    }

                    if ($sName) {
                        $query .= " AND s.full_name ILIKE '$sName%'";
                    }
                    
                    if ($selectFor) {
                        $query .= " AND sr.request_type='$selectFor'";
                    }

                    $query .= " ORDER BY sr.date desc";
                    
                    $result = pg_query($dbh, $query);
                    if (!$result) {
                        echo "An error occurred.\n";
                        exit;
                    }

                    $records = pg_fetch_all($result);
                    
                    if ($records) {
                        foreach ($records as $key => $record) {
                            
                ?>
                    <tr>
                        <td class="assigned-admin-id"><?php echo $record['id']; ?></td>
                        <td><?php echo $record['student_name']; ?></td>
                        <td><?php echo date('d-m-Y', strtotime($record['date'])); ?></td>
                        <td>
                            <?php 
                                if ($record['request_type'] == Common::GROUP_REQUETS) {
                                    echo "Club"; 
                                } else {
                                    echo ucfirst($record['request_type']); 
                                }
                            ?>
                        </td>
                        <td class="text-center">
                            <?php if ($record['status'] == Common::STATUS_PENDING) { ?>
                                <button class="btn btn-primary btn-xs disabled">Pending</button>
                            <?php } ?>
                            <?php if ($record['status'] == Common::STATUS_ACCEPT) { ?>
                                <button class="btn btn-success btn-xs disabled">Accepted</button>
                            <?php } ?>
                            <?php if ($record['status'] == Common::STATUS_DENY) { ?>
                                <button class="btn btn-danger btn-xs disabled">Denied</button>
                            <?php } ?>
                        </td>
                    </tr>
                <?php 
                        }
                    } else {
                        echo "<tr><td colspan='5' class='text-center'>No Record Found</td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
    <!--End Table -->

</div>

<?php
}
?>

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Student Request";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include("footer.php");
?>