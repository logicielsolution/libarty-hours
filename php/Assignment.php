<?php 

if(!isset($_SESSION)) {
     session_start();
}

class Assignment
{
  
    private $dbh;
    protected $to;
    protected $from;
    protected $studentName;
    protected $teacherName;
    
    
	
	function __construct()
	{
	    include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
        include('Teacher.php');
        include('Student.php');
        
        $this->dbCon = $dbh;
		$this->teacher = new Teacher;
		$this->student = new Student;
	}

	/**
	 * Delete single assignment from calander
	 * 
	 * @param  int  $id  		assignment id
	 * @return json $response
	 */
	public function delete($id)
	{
		if (!$id) {
			return false;
		}

		$query = "DELETE FROM assignments WHERE id = $id";
		$SQLresult = pg_query($this->dbCon, $query);
		if(!$SQLresult) {
			$response['error'] = true;
			$response['message'] = "Assignment could not be deleted. Please try again.";

			return $response;
		}

		$response = [
			'error' => false,
			'message' => "Assignment saved successfully."
		];

		return json_encode($response);
	}

	/**
	 * Get assignments list selected date to end of month
	 * 
	 * @param  date  $startDate  	selected date
	 * @param  date  $endDate  		end date of month
	 * @return array $events
	 */
	public function getAssignmentList($startDate, $endDate, $note, $assignmentType)
	{
		$begin = new DateTime(str_replace('/', '-', $startDate));
		$end = new DateTime(str_replace('/', '-', $endDate));
		$end = $end->modify( '+1 day' );

		$startDate = $begin->format('d/m/Y');
		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);
		
		$assignmentTypes = [Common::FULL_DAY_ASSIGNMENT, Common::FIRST_HALF_ASSIGNMENT, Common::SECOND_HALF_ASSIGNMENT];

		$events = [];
		foreach($daterange as $date){
			foreach ($assignmentTypes as $key => $aType) {
				$event = [
					'color' 	 => ($date->format('d/m/Y') == $startDate) ? 'green' : 'red',
					'title' 	 => 'Liberty School',
					'teacher' 	 => '',
					'note'		 => $note,
					'event_date' => $date->format("Y-m-d")
				];
				
				if ($assignmentType == Common::FIRST_HALF_ASSIGNMENT) {
					$date->setTime(9, 30);
					$event['start'] = $date->format("Y-m-d H:i:s");
					$date->setTime(12, 30);
					$event['end'] = $date->format("Y-m-d H:i:s");
					$event['assignment_type'] = Common::FIRST_HALF_ASSIGNMENT;
				} elseif ($assignmentType == Common::SECOND_HALF_ASSIGNMENT) {
					$date->setTime(13, 30);
					$event['start'] = $date->format("Y-m-d H:i:s");
					$date->setTime(15, 30);
					$event['end'] = $date->format("Y-m-d H:i:s");
					$event['assignment_type'] = Common::SECOND_HALF_ASSIGNMENT;
				} elseif ($assignmentType == Common::FULL_DAY_ASSIGNMENT){
					$event['start'] = $date->format("Y-m-d");
					$event['assignment_type'] = Common::FULL_DAY_ASSIGNMENT;
				} else {
					if ($aType == Common::FIRST_HALF_ASSIGNMENT) {
						$date->setTime(9, 30);
						$event['start'] = $date->format("Y-m-d H:i:s");
						$event['assignment_type'] = Common::FIRST_HALF_ASSIGNMENT;
					} elseif ($aType == Common::SECOND_HALF_ASSIGNMENT) {
						$date->setTime(13, 30);
						$event['start'] = $date->format("Y-m-d H:i:s");
						$event['assignment_type'] = Common::SECOND_HALF_ASSIGNMENT;
					} else {
						$event['start'] = $date->format("Y-m-d");
						$event['assignment_type'] = Common::FULL_DAY_ASSIGNMENT;
					}
				}

			$events[] = $event;
			}

		}
		
		return $events;
	}

	/**
	 * Get students saved assignments
	 * 
	 * @param  date $date
	 * @param  int  $studentId  	selected student id
	 * @param  int  $teacherId  	selected teacher id
	 * @return array $assignments  	saved assignments
	 */
	public function getStudentAssignments($date, $studentId, $teacherId, $assignmentType=null)
	{
		$date = new DateTime($date);
		$date = $date->format('Y-m-d');
		$getAssignmentSql = "SELECT assignments.*, teachers.full_name as teacher_name 
								FROM assignments 
								LEFT JOIN teachers ON assignments.teacher_id = teachers.id
								WHERE assignments.student_id = '$studentId'
									AND assignments.date >= '$date'";
		if ($teacherId) {	
			$getAssignmentSql .= " AND assignments.teacher_id='$teacherId'";
		}

		if ($assignmentType) {
			$getAssignmentSql .= " AND assignments.assignment_type='$assignmentType'";
		}

		/*if (Common::isSuperAdmin() || Common::isAdmin()) {
			$getAssignmentSql .= " AND assignments.unassigned IS NULL";	
		}*/


		//get normal assignments
		$getNormalAssignmentSql = $getAssignmentSql . " AND group_id IS NULL ORDER BY assignments.date";

		$SQLresult = pg_query($this->dbCon, $getNormalAssignmentSql);
	   	/*if(!pg_numrows($SQLresult)) {
	   		return [];
		}*/
		$normalAssignments = pg_fetch_all($SQLresult);
		if (!$normalAssignments) {
			$normalAssignments = [];
		}


		// get group assignments
		$getGroupAssignmentSql = $getAssignmentSql . " AND group_id IS NOT NULL ORDER BY assignments.date";
		
		$SQLresult = pg_query($this->dbCon, $getGroupAssignmentSql);
	   	/*if(!pg_numrows($SQLresult)) {
	   		return [];
		}*/
		$groupAssignments = pg_fetch_all($SQLresult);
		if (!$groupAssignments) {
			$groupAssignments = [];
		}

		$assignments = array_merge($groupAssignments, $normalAssignments);
		
		return $assignments;
	}

	/**
	 * Set assignment status if sabed then color blue if selected then set color green
	 * 	if unselected then set color red.
	 * 	
	 * @param array  $dates       events list
	 * @param array  $assignments students saved assignments
	 * @param bool   $isSaved     if true then return only saved assignments
	 * @param int    $teacherId   teacher id
	 */
	public function setAssignmentStatus($dates, $assignments, $isSaved, $teacherId)
	{	
		$teacher = $this->teacher->getTeacherById($teacherId);

		if($teacher) {
			foreach ($dates as $key => $date) {
				$dates[$key]['teacher'] = $teacher->full_name;
				$dates[$key]['teacher_id'] = $teacherId;
			}
		}

		if(empty($assignments) && $isSaved) {
			return [];
		}

		if(empty($assignments)) {
			return $dates;
		}


		$assignedDates = [];
		$assignedTeacher = [];
		$assignedTeacherIds = [];
		$assignmentNote = [];
		$assignmentGroupIds = [];
		$assignmentTypes = [];
		$assignmentUnassigned = [];
		$assignmentAbsent = [];
		
		foreach ($assignments as $assignment) {
			$groupId = isset($assignment['group_id'])? $assignment['group_id'] : null;
			$groupName = isset($assignment['group_name'])? $assignment['group_name'] : null;
			$assignmentType = isset($assignment['assignment_type'])? $assignment['assignment_type'] : Common::FULL_DAY_ASSIGNMENT;

			$dateObj = new DateTime($assignment['date']);
			if ($assignmentType == Common::FIRST_HALF_ASSIGNMENT) {
				$dateObj->setTime(9, 30);
				$assignment['date'] = $dateObj->format("Y-m-d H:i:s");
			} elseif ($assignmentType == Common::SECOND_HALF_ASSIGNMENT) {
				$dateObj->setTime(13, 30);
				$assignment['date'] = $dateObj->format("Y-m-d H:i:s");
			} else {
				$assignment['date'] = $dateObj->format("Y-m-d");
			}

			$assignedDates[$assignment['id']] = $assignment['date'];
			$assignedTeacher[$assignment['date']] = $assignment['teacher_name'];
			$assignedTeacherIds[$assignment['date']] = $assignment['teacher_id'];
			$assignmentNote[$assignment['date']] = $assignment['note'];
			$assignmentGroupIds[$assignment['date']] = $groupId;
			$assignmentGroupNames[$assignment['date']] = $groupName;
			$assignmentTypes[$assignment['date']] = $assignmentType;
			$assignmentUnassigned[$assignment['date']] = $assignment['unassigned'];
			$assignmentAbsent[$assignment['date']] = $assignment['absent'];
		}

		$assignedIds = array_flip($assignedDates);

		$savedAssignments = [];
		foreach ($dates as $key => $date) {
			// $startDate = date('Y-m-d', strtotime($date['start']));
			$startDate = $date['start'];
			
			$dateObj = new DateTime($startDate);
			
			if (in_array($startDate, $assignedDates)) {
				$dates[$key]['color'] = '';
				$dates[$key]['assignment_id'] = $assignedIds[$startDate];
				$dates[$key]['teacher'] = $assignedTeacher[$startDate];
				$dates[$key]['teacher_id'] = $assignedTeacherIds[$startDate];
				$dates[$key]['group_id'] = $assignmentGroupIds[$startDate];
				$dates[$key]['group_name'] = $assignmentGroupNames[$startDate];
				$dates[$key]['assignment_unassigned'] = $assignmentUnassigned[$startDate];
				$dates[$key]['absent'] = $assignmentAbsent[$startDate];
				
				if ($assignmentTypes[$startDate] == Common::FIRST_HALF_ASSIGNMENT) {
					$dateObj->setTime(9, 30);
					$dates[$key]['start'] = $dateObj->format("Y-m-d H:i:s");
					$dateObj->setTime(12, 30);
					$dates[$key]['end'] = $dateObj->format("Y-m-d H:i:s");
				} elseif ($assignmentTypes[$startDate] == Common::SECOND_HALF_ASSIGNMENT) {
					$dateObj->setTime(13, 30);
					$dates[$key]['start'] = $dateObj->format("Y-m-d H:i:s");
					$dateObj->setTime(15, 30);
					$dates[$key]['end'] = $dateObj->format("Y-m-d H:i:s");
					
				}
			}
			if($isSaved && in_array($startDate, $assignedDates)) {
				$dates[$key]['isSaved'] = $isSaved;
				$dates[$key]['note'] = $assignmentNote[$startDate];
				$dates[$key]['assignment_type'] = $assignmentTypes[$startDate];
				
				$savedAssignments[] = $dates[$key];
			} else if(!$isSaved && in_array($startDate, $assignedDates)) {
				$dates[$key]['isSaved'] = true;
				
				$savedAssignments[] = $dates[$key];
			} else if(!$isSaved && !in_array($startDate, $assignedDates)) {
				// $dates[$key]['color'] = 'red';
				$dates[$key]['isSaved'] = false;
				$dates[$key]['assignment_id'] = null;
				$savedAssignments[] = $dates[$key];
			}
		}

		return $savedAssignments;
	}

	/**
	 * Save assignments in database
	 * 
	 * @param  array  $studentAssignments selected assignments
	 * @return json   $response 
	 */
	public function saveAssignments($studentAssignments)
	{	
		$loggedInUserId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
		
		$sIds = [];
		$tIds = [];
		$existAssignmentDates = [];
		foreach ($studentAssignments as $key => $assignment) {

			$date 			= $assignment['date'];
			$studentId 		= $assignment['student_id'];
			$teacherId 		= $assignment['teacher_id'];
			$note			= $assignment['note'];
			$assignmentType	= $assignment['assignment_type'];

			array_push($sIds, $studentId);
			array_push($tIds, $teacherId);

			// if assignment is already created then no need to create assignment.
			if($this->isAssignmentAlreadyCreated($studentId, $teacherId, $date, $assignmentType)) {
				$dateObj = new DateTime($date);
            	$date = $dateObj->format('d/m/y');
				array_push($existAssignmentDates, $date);
				unset($studentAssignments[$key]);
				continue;
			}

			// if requested assignment exist then delete deny requested assignment and save new assignment
			if ($this->isRequestedAssignmentExist($studentId, $date)) {
				$this->deleteRequestedAssignment($studentId, $date);
				$this->updateRequestedAssignmentStatus($studentId, $date);
			}
			
			$addAssignmentQuery = "INSERT INTO assignments (student_id, teacher_id, date, assigner_id, note, assignment_type) VALUES ('$studentId', '$teacherId', '$date', '$loggedInUserId', '$note', '$assignmentType')";

			$result = pg_query($this->dbCon, $addAssignmentQuery);
			if (!$result) {
				$respose = [ 
					'error' => true,
					'message_type' => 'error',
					'message' => "Assignments could not be saved. Please try again."
				];
				
				return json_encode($respose);
			}


		}

		$sIds = array_unique($sIds);
		$tIds = array_unique($tIds);
		if (!empty($studentAssignments)) {
			$this->student->sendEmail($sIds, $studentAssignments); //send mail to student
			$this->teacher->sendEmail($tIds, $studentAssignments); //send mail to teacher
		}

		$message = 'Assignment is saved successfully.';
		$messageType = 'success';
		if (!empty($existAssignmentDates)) {
			$studentId = current($sIds);
			$student = $this->student->getStudentById($studentId);
			
			$message = 'Sorry, You can not assign '.$student->full_name.' on '.implode(',', $existAssignmentDates).' as it is already assigned.';	
			$messageType = 'error';
		}

		$respose = [
			'error' => false,
			'message_type' => $messageType,
			'message' => $message
		];
		
		return json_encode($respose);
	}

	/**
	 * Check assignment already exist in database
	 * 
	 * @param  int  $studentId 	Selected student id
	 * @param  int  $teacherId  Selected teacher id
	 * @param  date $date       
	 * @return bool  			Is exist or not  
	 */
	public function isAssignmentAlreadyCreated($studentId, $teacherId, $date, $assignmentType)
	{
		/*$assignmentQuery = "SELECT * FROM assignments WHERE teacher_id = '$teacherId'";*/
		$assignmentQuery = "SELECT * FROM assignments WHERE 1=1";
		$assignmentQuery .= " AND student_id = '$studentId' AND date = '$date'";

		if ($assignmentType == Common::FULL_DAY_ASSIGNMENT) {
			$assignmentQuery .= " AND assignment_type IN ('$assignmentType', '" . Common::FIRST_HALF_ASSIGNMENT . "', '" . Common::SECOND_HALF_ASSIGNMENT . "')";			
		} else {
			$assignmentQuery .= " AND assignment_type IN ('$assignmentType', '" . Common::FULL_DAY_ASSIGNMENT . "')";
		}

		$SQLresult = pg_query($this->dbCon, $assignmentQuery);
	   	if(!pg_numrows($SQLresult)) {
			return false;
		}

		return true;
	}

	/**
	 * Check date has requested event or not
	 * 
	 * @param  int 	$studentId
	 * @param  stiring	 $date]
	 * @return boolean
	 */
	public function isRequestedAssignmentExist($studentId, $date)
	{
		$assignmentQuery = "SELECT * FROM assignments WHERE student_id = '$studentId' AND date = '$date' AND unassigned = 't'";

		$SQLresult = pg_query($this->dbCon, $assignmentQuery);
	   	if(!pg_numrows($SQLresult)) {
			return false;
		}

		return true;
	}

	/**
	 * delete requested assignment
	 * 
	 * @param  int $studentId
	 * @param  string $date
	 * @return bool
	 */
	public function deleteRequestedAssignment($studentId, $date)
	{
		$query = "DELETE from assignments WHERE student_id=$1 AND date=$2 AND unassigned=$3";
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$studentId, $date, 't']);

		return true;
	}

	/**
	 * Update student assignment request status
	 * 
	 * @param  int $studentId
	 * @param  string $date
	 * @return bool
	 */
	public function updateRequestedAssignmentStatus($studentId, $date)
	{
		$query = "UPDATE student_requests SET status=$1 WHERE student_id=$2 AND date=$3";
		$result = pg_prepare($this->dbCon, "update_student_request_query", $query);
		$result = pg_execute($this->dbCon, "update_student_request_query", [Common::STATUS_DENY, $studentId, $date]);

		return true;
	}

	/**
	 * Update note in saved assingment
	 * @param  int  $assignmentId
	 * @param  string  $note
	 * @return json  response
	 */
	public function updateNote($assignmentId, $note)
	{
		$query = "UPDATE assignments SET note = $1 WHERE id = $2";
		$result = pg_prepare($this->dbCon, "update_note_query", $query);
		$result = pg_execute($this->dbCon, "update_note_query", [$note, $assignmentId]);

		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Note could not be updated. Please try again.";

			return $response;
		}

		$response = [
			'error' => false,
			'message' => "Note updated successfully."
		];

		return json_encode($response);
	}

    /**
     * Make query for get overall assignments lists
     * @param  date  $fromdate
     * @param  date  $todate
     * @param  string  $sname  Student name
     * @param  string $tname  Teacher name
     * @return string $query
     */
    public function makeQuery($fromdate=null, $todate=null, $sname=null , $tname=null )
    {
        $this->to = $todate;
        $this->from = $fromdate;
        $this->studentName = $sname;
        $this->teacherName=$tname;

        
        $query = "SELECT a.*,s.id as student_id ,  s.full_name AS student_name, t.full_name AS teacher_name 
            FROM assignments AS a 
              INNER JOIN students AS s ON s.id = cast(a.student_id as int)
              INNER JOIN teachers AS t ON t.id = a.teacher_id WHERE 1=1";

        // if log in through admin or super admin
        $objCheck = new Common();
        if( $objCheck->isAdmin())
        {
            $buildId = $_SESSION['school_id'];
            $q="SELECT id FROM teachers WHERE building_id=".$buildId;

            $res = pg_query($this->dbCon, $q);
            if (!$res) {
                echo "An error occurred.\n";
                exit;
            }
            
            $teachId = pg_fetch_all($res);
            if ($teachId) {
	            $valuesSql = implode(",",  array_column($teachId,'id') ) ;
	            $query .= " and a.teacher_id in ({$valuesSql})" ;
            }
        }
        else if(  $objCheck->isTeacher()   )
        { 
          $query .= " and a.teacher_id= ".$_SESSION['user_id'];
        }




        if(!empty($this->studentName) ) {
           $query .= " and s.full_name ILIKE '".$this->studentName."%'";
        }
          
        if(!empty($this->teacherName) ) {
           $query .= " and t.full_name ILIKE '".$this->teacherName."%'";
        }

        if ( ! ( empty($this->from) && empty($this->to) ) ) {
            $fromDate = new DateTime($this->from);
            $fromDate = $fromDate->format('Y-m-d');
              
            $toDate = new DateTime($this->to);
            $toDate = $toDate->format('Y-m-d');
              
            $query = $query . " and  a.date>='$fromDate' and a.date<='$toDate'";  
        } 

        return $query;
    }

    /**
     * get assignments list pagination
     * @param  string  $query  
     * @param  int  $nopages
     * @param  int $start  
     * @return array $assignments
     */
    public function getAssignmentListPagination($query,$nopages, $start )
    {
        $query .= " order by a.date desc";
        $query.=" LIMIT $nopages OFFSET $start;";

        $SQLresult = pg_query($this->dbCon, $query);

        $assigns = [];
        if(pg_numrows($SQLresult)) 
        {
            $assigns = pg_fetch_all($SQLresult);
        }

        return $assigns;
    }

}
