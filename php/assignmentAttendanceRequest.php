<?php

ini_set('display_errors',"1");

if(!isset($_SESSION)) {
     session_start();
}

include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
include('Mailer.php');
include("Student.php");

$assignmentId = isset($_POST['assignment_id']) ? $_POST['assignment_id'] : null;
$attendance = isset($_POST['attendance']) ? $_POST['attendance'] : null;
$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$teacherName = isset($_POST['teacher']) ? $_POST['teacher'] : null;
$assignmentDate = isset($_POST['assignment_date']) ? $_POST['assignment_date'] : null;

$studentObj = new Student;
$student = $studentObj->getStudentById($studentId);

//set email
$email = $student->username . Common::LIBERTY_EMAIL_DOMAIN;
$enviroment = Common::APP_ENV;
if ($enviroment == 'local') {
	$email = Common::STUDENT_TEST_EMAIL;
}

$mail = new Mailer;
$mail->addAddress($email);

$absent = ($attendance == 'absent') ? 1 : 0;

$query = "UPDATE assignments SET absent = $1 WHERE id = $2";
		
$result = pg_prepare($dbh, "update_attendance_query", $query);
$result = pg_execute($dbh, "update_attendance_query", [$absent, $assignmentId]);

if(!$result) {
	$response['error'] = true;
	$response['message'] = "Assignment attendance not updated. Please try again.";

	echo json_encode($response);
}

if ($attendance == 'absent') {
	$mail->sendAbsentAttendanceMailToStudent($student, $teacherName, $assignmentDate);
}

$response['error'] = false;
$response['absent'] = $absent;
$response['message'] = "Assignment attendance updated successfully.";

echo json_encode($response);
