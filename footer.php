			<div>
			    <p class="text-muted">Place sticky footer content here.</p>
			</div>
		</div>
    </div>

     <!-- Bootstrap core JS -->
    <script src="js/bootstrap.js"></script>
    <script src="js/assignment-list.js"></script>
    <script src='lib/moment.min.js'></script>
    <script src='lib/moment-timezone.min.js'></script>
    <script src='lib/moment-timezone-with-data.min.js'></script>
    <script src='js/jquery.qtip.min.js'></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src='js/fullcalendar.js'></script>
    <script src="js/select2.min.js"></script>
    <script src='js/app.js'></script>
    <script src='js/assign-student.js'></script>
    <script src='js/student.js'></script>
    <script src='js/absent.js'></script>
    <script src='js/blackout-days.js'></script>
    <script src="js/Report.js"></script>
    <script src="js/clubs.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var filename = $(location).attr('pathname');
           function activeLi()
           {
           var countli= $('.nav li').length;
            for(var i =1 ; i < countli ; i++)
            {
                var htmlCode= $('.nav li:nth-child('+i+') a').attr('href') ;

                if(htmlCode==filename)
                {
                    $('.nav li:nth-child('+i+')').addClass('actli')
                    $('.nav li:nth-child('+i+') a').addClass('acta')
                }

            }

           }
           activeLi();
        })
    </script>

    <?php 
    	include("assign-teachers-popup.php");
    	include("modal-popups.php");
    ?>
</body>
</html>