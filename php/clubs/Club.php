<?php 

if(!isset($_SESSION)) {
     session_start();
}

class Club
{
	private $dbh;

	function __construct($con=null)
	{	
		if ($con) {
			$this->dbCon = $con;
		} else {
			include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
			$this->dbCon = $dbh;
		}
	}

	/**
	 * get club list match with name
	 * @param  string $clubName
	 * @return json club list
	 */
	public function getClubList($clubName)
	{
		// $commonObj = new Common;

		$schoolId = Common::getSchoolId();
		$schoolId = Common::getBuildingCodeById($schoolId);
		
		$query = "SELECT * FROM groups WHERE 1=1";
		if ($clubName) {
			$query .= " AND name ILIKE '$clubName%'";
		}

		if ($schoolId) {
            $query .= " and building = $schoolId";
        }


		$SQLresult = pg_query($this->dbCon, $query);

		$clubs = [];
		if(pg_numrows($SQLresult)) {
			$clubs = pg_fetch_all($SQLresult);
		}
		echo json_encode($clubs);
	}

	/**
	 * Get student's club list
	 * @param  int  $studentId  Student id
	 * @return array  $clubs
	 */
	public function getStudentClubsList($studentId)
	{
		$query = "select g.id, g.name from groups as g inner join student_to_group as stg on g.id = stg.group_id where cast(stg.student_id as int)=$studentId order by g.name";


		$SQLresult = pg_query($this->dbCon, $query);

		$clubs = [];
		if(pg_numrows($SQLresult)) {
			$clubs = pg_fetch_all($SQLresult);
		}
		$clubs = array_column($clubs, 'name');

		return $clubs;
	}

	/**
	 * Get student's club assignments
	 * @param  int  $studentId
	 * @param  date  $startDate
	 * @param  date  $endDate
	 * @return array  club assignments
	 */
	public function getStudentClubAssignments($studentId, $startDate, $endDate)
	{
		$endDate = date('Y-m-d', strtotime(str_replace('/', '-', $endDate)));
		$startDate = new DateTime($startDate);
		$endDate = new DateTime($endDate);

		$sDate = $startDate->format('Y-m-d');
		$eDate = $endDate->format('Y-m-d');

		$query = "SELECT 
					g.id as group_id, 
					g.admin_id as assigner_id, 
					cds.id as date_id, 
					g.*, 
					cds.date as date, 
					sg.student_id 
				from groups as g
				INNER JOIN student_to_group as sg
					ON g.id = sg.group_id 
				INNER JOIN club_dates as cds
					ON g.id = cds.group_id";

		$query .= " WHERE sg.student_id=$studentId 
					AND cds.date >= '$sDate'
					AND cds.date <= '$eDate'";

		$result = pg_query($this->dbCon, $query);

		// pg_numrows($SQLresult)
		$items = pg_fetch_all($result);

		if(!$items) return [];

		foreach ($items as $key => $item) {
			$groupId = $item['group_id'];
			$teacherName = $this->getClubAdminsNameByClubId($groupId);
			$items[$key]['teacher_name'] = $teacherName;
			$items[$key]['id'] = $groupId .'-'. $item['date_id'];
			$items[$key]['absent'] = null;
			$items[$key]['detention'] = null;
			$items[$key]['unassigned'] = null;
			$items[$key]['teacher_id'] = null;
			$items[$key]['note'] = $item['name'] . ": " . $item['description'];
		}
		return $items;
	}

	/**
	 * add club details to club assignments like club name/description/teachers
	 * @param array  $allAssignments
	 * @param string  $lastDateOfMonth  Last date of month
	 * @return array  $allAssignments  all assignments with updated club data
	 */
	public function addClubDetailsToClubAssignment($allAssignments, $lastDateOfMonth)
	{
		$lastDateOfMonth = date('Y-m-d', strtotime(str_replace('/', '-', $lastDateOfMonth)));
		$lastDateOfMonth = new DateTime($lastDateOfMonth);
		$lastDateOfMonth = $lastDateOfMonth->format('Y-m-d');

		foreach ($allAssignments as $key => $assignment) {
			if (!$assignment['group_id'] || $assignment['date'] > $lastDateOfMonth) {
				continue;
			}

			$query = "SELECT 
						g.name as group_name,
						g.description as group_description 
						from groups as g 
						where id = " . $assignment['group_id'];

			$result = pg_query($this->dbCon, $query);

			$group = pg_fetch_assoc($result);
			
			$allAssignments[$key]['group_name'] = $group['group_name'];
			$allAssignments[$key]['note'] = $group['group_description'];
			$teacherName = $this->getClubAdminsNameByClubId($assignment['group_id']);
			$allAssignments[$key]['teacher_name'] = $teacherName;
		}

		return $allAssignments;
	}

	private function getClubAdminsNameByClubId($clubId)
	{
		$tQuery = "SELECT t.full_name FROM admin_to_group as ag
					INNER JOIN teachers as t
						ON t.id=ag.admin_id
					WHERE ag.group_id = $clubId";
		$tResult = pg_query($this->dbCon, $tQuery);
		$teachers = pg_fetch_all($tResult);

		if (!$teachers) {
			return;
		}
		
		$names = [];
		foreach ($teachers as $key => $teacher) {
			$names[] = $teacher['full_name'];
		}

		return implode(', ', $names);
	}

	public function saveClub($club)
	{

		$time = time();
		$date = date("Y-m-d",$time);

		// $buildingId = Common::getBuildingCodeById($club['clubBuilding']);
		$buildingId = $club['clubBuilding'];

		$query = "INSERT INTO groups (name, description, admin_id, building, created) VALUES ('" .$club['clubName']."', '".$club['clubDescription']."', ".$club['clubAdminID'].", ".$buildingId.", '$date') RETURNING id";
		
		$SQLresult = pg_query($this->dbCon, $query);

		if(!$SQLresult) {
			$response['error'] = true;
			$response['message'] = "Club not created successfully. Please try again.";

			return json_encode($response);;
		}
		
		$row = pg_fetch_row($SQLresult); 
		$groupId = $row['0'];

		$clubAdmins = $club['clubAdmins'];
		array_unshift($clubAdmins, $club['clubAdminID']);

		// add entries in admin_to_group table
		$this->addClubAdmins($groupId, array_unique($clubAdmins));
		

		$this->addClubDates($groupId, $club['clubDates']);

		$response = [
			'error' => false,
			'message' => "Club successfully created."
		];

		return json_encode($response);
	}

	/**
	 * Add admins to club
	 * @param int $groupId 
	 * @param array $clubAdminIds club admins ids
	 */
	public function addClubAdmins($groupId, $clubAdminIds)
	{
		$addAdminQuery = "INSERT INTO admin_to_group (admin_id, group_id) VALUES ($1, $2)";
		$result = pg_prepare($this->dbCon, "add_admin_query", $addAdminQuery);

		foreach ($clubAdminIds as $key => $clubAdminId) {
			$result = pg_execute($this->dbCon, "add_admin_query", [$clubAdminId, $groupId]);
			
			if(!$result) {
				$response['error'] = true;
				$response['message'] = "Admin not assigned to club.";

				json_encode($response);
			}
		}

		return true;
	}

	/**
	 * Get club dates by group
	 * @param  int  $groupId
	 * @return array  data
	 */
	public function getClubDates($groupId)
	{
		$query = "SELECT date FROM club_dates WHERE group_id=$groupId";

		$result = pg_query($this->dbCon, $query);

		if(!$result) {
			return false;
		}
		
		$row = pg_fetch_all($result);

		return $row;
	}

	/**
	 * Add dates to club
	 * @param int  $groupId  created club id
	 * @param string  $dates  selected dates list
	 */
	public function addClubDates($groupId, $dates)
	{
		$dates = explode(',', $dates);

		$query = "INSERT INTO club_dates (group_id, date) VALUES ($1, $2)";
		$result = pg_prepare($this->dbCon, "add_date_query", $query);

		foreach ($dates as $key => $date) {
			$date = str_replace('/', '-', $date);
			$date = date("Y-m-d", strtotime($date));
		
			$result = pg_execute($this->dbCon, "add_date_query", [$groupId, $date]);

			if(!$result) {
				$response['error'] = true;
				$response['message'] = "Date not assigned to Club.";

				json_encode($response);
			}
		}

		return true;
	}

	/**
	 * delete club dates
	 * @param  int  $clubId  group id
	 * @return bool
	 */
	public function deleteClubDates($clubId)
	{
	    $q = "DELETE FROM club_dates WHERE group_id=$1";
	    $result = pg_prepare($this->dbCon, "delete_club_dates", $q);
	    $result = pg_execute($this->dbCon, "delete_club_dates", [$clubId]);

	    if(!$result) {
			return false;
		}

		return true;
	}

	/**
	 * Delete all club admins
	 * @param  int  $clubId
	 * @return bool
	 */
	public function deleteClubAdmins($clubId)
	{
		$q = "DELETE FROM admin_to_group WHERE group_id=$1";
	    $result = pg_prepare($this->dbCon, "delete_club_admins", $q);
	    $result = pg_execute($this->dbCon, "delete_club_admins", [$clubId]);

	    if(!$result) {
			return false;
		}

		return true;	
	}

	/**
	 * Delete club
	 * @param  int $id  group id from groups table
	 * @return json  response
	 */
	public function deleteClub($id)
	{
		$query = "DELETE FROM groups WHERE id=$1";
		
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$id]);
		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Club could not be deleted. Please try again.";

			json_encode($response);
		}

		//Delete group assignments
		$delQuery = "DELETE FROM assignments WHERE group_id=$1";
		$result2 = pg_prepare($this->dbCon, "delete_assignments_query", $delQuery);
		$result2 = pg_execute($this->dbCon, "delete_assignments_query", [$id]);
		if(!$result2) {
			$response['error'] = true;
			$response['message'] = "Club Assignments are not deleted when delete club.";

			return json_encode($response);
		}

		$response = [
			'error' => false,
			'message' => "Club student deleted successfully."
		];

		return json_encode($response);
	}

	/**
	 * Add admin to club
	 * @param int  $teacherId
	 * @param int  $groupId  group id from groups table
	 * @return json  response
	 */
	public function addAdminToClub($teacherId, $groupId)
	{
		$query = "INSERT INTO admin_to_group (admin_id, group_id) VALUES ($1, $2)";
		
		$result = pg_prepare($this->dbCon, "insert_query", $query);
		$result = pg_execute($this->dbCon, "insert_query", [$teacherId, $groupId]);
		
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Admin not assigned to club. Please try again.";

			json_encode($response);
		}

		$response = [
			'error' => false,
			'message' => "Admin successfully assigned."
		];

		return json_encode($response);
	}

	/**
	 * Delete added admin from club
	 * @param  int  $id
	 * @return json  response
	 */
	public function deleteAssignedAdminToClub($id)
	{
		$query = "DELETE FROM admin_to_group WHERE id=$1";
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$id]);
		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Assidned admin could not be deleted. Please try again.";

			json_encode($response);
		}

		$response = [
			'error' => false,
			'message' => "Assigned admin deleted successfully."
		];

		return json_encode($response);
	}

	/**
	 * Add student to club
	 * @param int  $studentId
	 * @param int  $groupId  group id from groups table
	 * @return json response
	 */
	public function addStudentToClub($studentId, $groupId)
	{
		$query = "INSERT INTO student_to_group (student_id, group_id) VALUES ($1, $2)";
		
		$result = pg_prepare($this->dbCon, "insert_query", $query);
		$result = pg_execute($this->dbCon, "insert_query", [$studentId, $groupId]);
		
		if(!$result) {
				$response['error'] = true;
				$response['message'] = "Student not assigned to club. Please try again.";

				return json_encode($response);
		}

		//save group date assignments
		$saveAssignment = $this->saveGroupAssignment($studentId, $groupId);
		if (!$saveAssignment) {
			$response['error'] = true;
			$response['message'] = "Group assignments could not be sa saved. Please try again.";

			return json_encode($response);	
		}

		$response['error'] = false;
		$response['message'] = "Student successfully assigned to club.";

		return json_encode($response);
	}

	/**
	 * Save group dates in assingment when add student to group
	 * @param  int  $studentId
	 * @param  int  $groupId
	 * @return resoponse
	 */
	public function saveGroupAssignment($studentId, $groupId)
	{
		$clubDates = $this->getClubDates($groupId);
		
		$teacherId = $_SESSION['user_id'];
		if (!Common::isTeacher()) {
			$clubTeachers = $this->getClubTeachers($groupId);
			$teacherId = $clubTeachers[0]['id'];
		}

		$insertQuery = "INSERT INTO assignments (student_id, teacher_id, group_id, date, assigner_id) VALUES ($1, $2, $3, $4, $5)";

		$result = pg_prepare($this->dbCon, "save_assignment_query", $insertQuery);
		foreach ($clubDates as $key => $clubDate) {

			if ($this->isRequestedAssignmentExist($studentId, $clubDate['date'])) {
				$this->deleteRequestedAssignment($studentId, $clubDate['date']);
				$this->updateRequestedAssignmentStatus($studentId, $clubDate['date']);
			}

			$result = pg_execute($this->dbCon, "save_assignment_query", [$studentId, $teacherId, $groupId, $clubDate['date'], $teacherId]);

			if(!$result) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Get club dates by group
	 * @param  int  $groupId
	 * @return array  data
	 */
	public function getClubTeachers($groupId)
	{
		$query = "SELECT t.* FROM teachers as t INNER JOIN admin_to_group as atg on t.id = atg.admin_id where atg.group_id=$groupId";

		$result = pg_query($this->dbCon, $query);

		if(!$result) {
			return false;
		}
		
		$teachers = pg_fetch_all($result);

		return $teachers;
	}

	/**
	 * Delete student from club
	 * @param  int  $id
	 * @return json  response
	 */
	public function deleteAddedStudent($id, $groupId, $studentId)
	{
		$query = "DELETE FROM student_to_group WHERE id=$1";
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$id]);
		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Assidned student could not be deleted. Please try again.";

			return json_encode($response);
		}

		//Delete group assignments that assigned to student
		$delQuery = "DELETE FROM assignments WHERE group_id=$1 and student_id=$2";
		$result2 = pg_prepare($this->dbCon, "delete_assignment_query", $delQuery);
		$result2 = pg_execute($this->dbCon, "delete_assignment_query", [$groupId, $studentId]);
		if(!$result2) {
			$response['error'] = true;
			$response['message'] = "Assidned student club assignments could not be deleted. Please try again.";

			return json_encode($response);
		}

		$response = [
			'error' => false,
			'message' => "Assigned student deleted successfully."
		];

		return json_encode($response);
	}

	/**
	 * Add enrty to group request table
	 * @param int $groupId   
	 * @param int $studentId 
	 * @return bool
	 */
	public function addEntryToGroupRequest($groupId, $studentId)
	{
		$query = "INSERT INTO student_requests (group_id, student_id, status, date, request_type) VALUES ($1, $2, $3, $4, $5)";
		
		$result = pg_prepare($this->dbCon, "insert_query", $query);
		$result = pg_execute($this->dbCon, "insert_query", [$groupId, $studentId, Common::STATUS_PENDING, date("Y/m/d"), Common::GROUP_REQUETS]);
		
		if(!$result) {
			return false;
		}

		return true;
	}

	/**
	 * Allow and Deny student request for club joining
	 * @param  int $groupId
	 * @param  int $studentId
	 * @param  string $status accept/deny 
	 * @return json response
	 */
	public function allowDenyClubRequest($groupId, $studentId, $status)
	{
		$query = "UPDATE student_requests SET status=$1 WHERE group_id=$2 AND student_id=$3";

		$result = pg_prepare($this->dbCon, "update_query", $query);
		$result = pg_execute($this->dbCon, "update_query", [$status, $groupId, $studentId]);

		if (!$result) {
			$response = [
				'error' => true,
				'message' => 'Status not successfully updated.'
			];
			
			return json_encode($response);
		}

		if ($status == Common::STATUS_ACCEPT) {
			$this->addStudentToClub($studentId, $groupId);
		}

		$response = [
			'error' => false,
			'message' => "Status successfully updated."
		];

		return json_encode($response);

		return true;
	}

	/**
	 * add assignment when add new date in club
	 * @param int  $groupId
	 * @param array  $newDates  new selected dates array
	 */
	public function addAssignmentWhenAddDate($groupId, $newDates)
	{
		$teacherId = $_SESSION['user_id'];
		if (!Common::isTeacher()) {
			$clubTeachers = $this->getClubTeachers($groupId);
			$teacherId = $clubTeachers[0]['id'];
		}

		$clubStudents = $this->getClubStudents($groupId);
		if (!$clubStudents) {
			return false;
		}
		$insertQuery = "INSERT INTO assignments (student_id, teacher_id, group_id, date, assigner_id) VALUES ($1, $2, $3, $4, $5)";

		$result = pg_prepare($this->dbCon, "save_assignment_query", $insertQuery);
		foreach ($clubStudents as $key => $student) {
			foreach ($newDates as $key2 => $date) {

				if ($this->isRequestedAssignmentExist($student['id'], $date)) {
					$this->deleteRequestedAssignment($student['id'], $date);
					$this->updateRequestedAssignmentStatus($student['id'], $date);
				}

				$result = pg_execute($this->dbCon, "save_assignment_query", [$student['id'], $teacherId, $groupId, $date, $teacherId]);

				if(!$result) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Get club dates by group
	 * @param  int  $groupId
	 * @return array  data
	 */
	public function getClubStudents($groupId)
	{
		$query = "SELECT s.* FROM students as s INNER JOIN student_to_group as stg on s.id = cast(stg.student_id as int) where stg.group_id=$groupId";

		$result = pg_query($this->dbCon, $query);

		if(!$result) {
			return false;
		}
		
		$students = pg_fetch_all($result);

		return $students;
	}

	/**
	 * delete assignment when delete club date
	 * @param  int  $groupId
	 * @param  array  $deletedDates deleted dates
	 * @return bool
	 */
	public function deleteAssignmentWhenDeleteDate($groupId, $deletedDates)
	{
		$q = "DELETE FROM assignments WHERE group_id=$1 AND date=$2";
	    $result = pg_prepare($this->dbCon, "delete_assignment_query", $q);

	    foreach ($deletedDates as $key => $date) {
		    $result = pg_execute($this->dbCon, "delete_assignment_query", [$groupId, $date]);

		    if(!$result) {
				return false;
			}
	    }

		return true;
	}

	/**
	 * Check date has requested event or not
	 * 
	 * @param  int 	$studentId
	 * @param  stiring	 $date]
	 * @return boolean
	 */
	public function isRequestedAssignmentExist($studentId, $date)
	{
		$assignmentQuery = "SELECT * FROM assignments WHERE student_id = '$studentId' AND date = '$date' AND unassigned = 't'";

		$SQLresult = pg_query($this->dbCon, $assignmentQuery);
	   	if(!pg_numrows($SQLresult)) {
			return false;
		}

		return true;
	}

	/**
	 * delete requested assignment
	 * 
	 * @param  int $studentId
	 * @param  string $date
	 * @return bool
	 */
	public function deleteRequestedAssignment($studentId, $date)
	{
		$query = "DELETE from assignments WHERE student_id=$1 AND date=$2 AND unassigned=$3";
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$studentId, $date, 't']);

		return true;
	}

	/**
	 * Update student assignment request status
	 * 
	 * @param  int $studentId
	 * @param  string $date
	 * @return bool
	 */
	public function updateRequestedAssignmentStatus($studentId, $date)
	{
		$query = "UPDATE student_requests SET status=$1 WHERE student_id=$2 AND date=$3";
		$result = pg_prepare($this->dbCon, "update_student_request_query", $query);
		$result = pg_execute($this->dbCon, "update_student_request_query", [Common::STATUS_DENY, $studentId, $date]);

		return true;
	}
}