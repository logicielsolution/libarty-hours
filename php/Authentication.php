<?php

class Authentication
{
	
	/**
	 * check user has permission for page if not then redirect to home page
	 * @param  string  $page  page name
	 * @return bool
	 */
	public function hasPermission($page)
	{
		$adminPages = [
						Common::PAGE_ASSIGNMENTS_LIST,
						Common::PAGE_ASSIGN_STUDENT,
						Common::PAGE_ABSENCES,
						Common::PAGE_CLUBS,
						Common::PAGE_BLACKOUT_DAYS,
						Common::PAGE_STUDENT_REQUEST,
						Common::PAGE_DETENTION_LIST	,
						Common::PAGE_ASSIGNMENT_REPORTLIST,
						Common::PAGE_CLUB_REPORT
					];

		$teacherPages = [
						Common::PAGE_ASSIGNMENTS_LIST,
						Common::PAGE_TEACHER_CALENDER,
						Common::PAGE_ABSENCES,
						Common::PAGE_CLUBS,	
						Common::PAGE_ASSIGNMENT_REPORTLIST,
						Common::PAGE_CLUB_REPORT
					];

		$studentPages = [
							Common::PAGE_STUDENT_ASSIGNMENTS, 
							Common::PAGE_CLUBS
						];
		
		$authenticate = false;
		if (Common::isSuperAdmin() || Common::isAdmin()) {
			$authenticate = in_array($page, $adminPages);
		} elseif (Common::isTeacher()) {
			$authenticate = in_array($page, $teacherPages);
		} else {
			$authenticate = in_array($page, $studentPages);
		}

		if (!$authenticate) {
			header('Location: /'); //redirect to home page
		}

		return true;
	}
}