<?php

ini_set('display_errors',"1");
include('Assignment.php');

if(!isset($_POST)) {
	echo "";
	exit;
}

$id = $_POST['id'];

$assignmentObj = new Assignment;
$response = $assignmentObj->delete($id);

echo $response;
exit;