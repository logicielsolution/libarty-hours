<?php
session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_STUDENT_ASSIGNMENTS);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//BEGIN entry_html();
?>
<?php function entry_html() { ?>

<!-- begin content -->
<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->
<script>
    var STUDENT_ID = "<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;?>";
	var SCHOOL_ID = "<?php echo isset($_SESSION['school_id']) ? $_SESSION['school_id'] : null;?>";
</script>
<div class="student-calendar-page">
	<h4><b>Assignments</b></h4>

    <div class="alert fade in flash-message">
      <p></p>
    </div>

	<!-- implement full calendar for assign students -->
	<div id='student-calendar' style="margin-top: 30px;"></div>
</div>


<?php
}

if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!--Content-->

 <!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") {
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Assignments";

        gen_header($title);
       
        entry_html();

    } else {
        header('Location: login.php');
        exit;
    }
    include('footer.php');
?>