<?php

if(!isset($_SESSION)) {
     session_start();
}

ini_set('display_errors',"1");

include_once('Constants.php');
include_once "Teacher.php";
require_once "Mailer.php";

class Student
{
	private $dbh;

	function __construct()
	{
		include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
		$this->dbCon = $dbh;
	}

	/**
	 * Get student list if name available then find by name
	 * @param  string  $fullName
	 * @return json $students
	 */
	public function getStudents($fullName, $data=null, $schoolId = null)
	{
		if (!$schoolId) {
			$schoolId = Common::getSchoolId();
			$schoolId = (array)Common::getBuildingCodeById($schoolId);
		}

		$schoolId = (array)$schoolId;
		$query = "SELECT * FROM students WHERE 1=1 ";
		if ($fullName) {
			$query .= " AND full_name ILIKE '%$fullName%'";
		}

		// is logged in user has building id 
		// then get that perticular builder students.
		if($schoolId) {
			$ids = implode(',', $schoolId);
			$query .= " AND building_id in ($ids)";
		}

		if ($data) {
			$groupId = $data['group_id'];
			$studentQuery = "SELECT s.id, a.date FROM students as s JOIN assignments as a ON cast(a.student_id as int)=s.id WHERE a.date IN (SELECT date from club_dates WHERE group_id=$groupId)";

			$result = pg_query($this->dbCon, $studentQuery);
			$studentIds = pg_fetch_all($result);
			
			if ($studentIds) {
				$ids= [];
		        foreach ($studentIds as $key => $student) {
		            array_push($ids, $student['id']);
		        }
		        $ids = implode(',', $ids);
			
				$query .= " AND id NOT IN ($ids)";
			}
		}

	    $SQLresult = pg_query($this->dbCon, $query);

	   	$students = [];
	   	if(pg_numrows($SQLresult)) {
	      	$students = pg_fetch_all($SQLresult);
		}

		return json_encode($students);
	}

	/**
	 * get student detail by id
	 * @param  int  $id
	 * @return object $student
	 */
	public function getStudentById($id)
	{
		$query = "SELECT * FROM students WHERE id = '$id'";
		
		$SQLresult = pg_query($this->dbCon, $query);

	   	$student = [];
	   	if(pg_numrows($SQLresult)) {
			$student = pg_fetch_object($SQLresult);
		}

		return $student;
	}

	/**
	 * Get student assignments 
	 * @param  int  $studentId
	 * @param  int  $teacherId
	 * @return json $assinments
	 */
	public function getStudentAssignments($studentId, $teacherId)
	{
		$query = "SELECT * FROM assignments WHERE student_id = '$studentId' AND teacher_id = '$teacherId'";
		
		$SQLresult = pg_query($this->dbCon, $query);

	   	$assignments = [];
	   	if(pg_numrows($SQLresult)) {
			$assignments = pg_fetch_all($SQLresult);
			$assignments = json_encode($assignments); //convert array to json format
		}

		return $assignments;	
	}

	/**
	 * Send assignment email to student
	 * @param  arrat  $studentIds  Selected student ids
	 * @param  array  $assignments
	 */
	public function sendEmail($studentIds, $assignments)
	{	
		$studentId = current($studentIds);
		$student = $this->getStudentById($studentId);	
		
		$records = [];	
		foreach ($assignments as $key => $assignment) {
			$date = $assignment['date'];
			$teacher = Teacher::getTeacherById($assignment['teacher_id']);

			$data = "<tr>
						<td>$date</td>
						<td>$teacher->full_name</td>
					</tr>";

			array_push($records, $data);
			
		}
		$records = implode(" ",$records);

		$email = $student->username . Common::LIBERTY_EMAIL_DOMAIN;

		$enviroment = Common::APP_ENV;
		if ($enviroment == 'local') {
			$email = Common::STUDENT_TEST_EMAIL;
		}

		$mail = new Mailer;

		$mail->addAddress($email);

		$mail->sendAssignmentMailToStudent($student, $records);

	}
}