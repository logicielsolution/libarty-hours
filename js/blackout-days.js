$( function() {
    var blackoutDaysObj = blackoutDays();
    blackoutDaysObj.bindObjects();
});

function blackoutDays() {
	return new function() {
		this.addBtn 				= $('#add-blackout-day'),
		this.addBDayForm  			= $('#blackoutDaysForm'),
		this.dateInput  			= $('#blackout-day-date'),
		this.deleteBtn  			= $('.delete-blackout-day'),
		this.editBtn  				= $('.edit-blackout-day'),
		this.editblackoutDaysForm 	= $('#editblackoutDaysForm'),
		this.blackoutDay    		= [],
		this.bindObjects = function() {
            var obj = this;
            
            // set date picker
            /*obj.dateInput.datepicker(
            	{ 
            		dateFormat: 'dd-mm-yy'
            	}
            ).datepicker("setDate", new Date());*/

            //add
			obj.addBtn.click(function() {
			  	obj.addBlackoutDayPopup();

	            // add datepicker
	            if (obj.dateInput.length < 1) {
	                return;
	            }
	            obj.dateInput.datepicker({
	                format: "dd-mm-yyyy",
	                todayHighlight: true
	            }).datepicker('setDate', [new Date()]);
			});

			//delete
			obj.deleteBtn.click(function(){
				var id = $(this).data('id');
				if (confirm("Do you want to delete?")) {
			  		obj.deleteBlackoutDay(id);
                }
			});

			//edit
			obj.editBtn.click(function(){
				var id = $(this).data('id');
			
				obj.getBlackoutDayByID(id);
				
				setTimeout(function() {
					obj.editBlackoutDayPopup(obj.blackoutDay);
				}, 500);
			});

			// when modal hidden
			$("#addBlackoutDayModal").on('hidden.bs.modal', function () {
		        $(this).find('.add-holyday').val('');
		        $(this).data('bs.modal', null);
		        $('.blackout-days-checkbox').prop('checked', false);
		        $('.building-selection-error').html('');
		    });
        },
		this.getBlackoutDayByID = function(id) {
			var obj = this;
            $.ajax({
                url: 'php/getBlackoutDays.php',
                dataType: 'json',
                type: 'get',
                data: {
                	id: id
                },
                beforeSend: function() {
                    showLoader();
                }
            })
            .done(function(response, textStatus, jqXHR) {
                obj.blackoutDay = response;
            })
            .always(function() {
                hideLoader();
            });
		},
		this.addBlackoutDayPopup = function() {
			var obj = this;
			$('#addBlackoutDayModal').modal();

			//enable disable save button
			var saveBtn = obj.addBDayForm.find('.add-submit-btn');
			saveBtn.attr('disabled', 'disabled')
			$('.add-holyday').on('input', function(){
		        if($(this).val().length !=0){
		            saveBtn.attr('disabled', false);            
		        }
		        else{
		            saveBtn.attr('disabled',true);
		        }
		    });


			obj.addBDayForm.off('submit');
			obj.addBDayForm.on('submit', function() {
                var $this = $(this);

                //checkbox validation
                if ($('.blackout-days-checkbox').length > 0 && $('.blackout-days-checkbox:checked').length == 0) {
			    	$('.building-selection-error').html('Please select building first.');
			    	return false;	
		        }

                var url = $this.attr('action');
                var params = $this.serialize();
           
                var saveBlackoutDayPromise = $.ajax({
	                url: url,
	                dataType: "json",
	                type: 'POST',
	                data: params,
	                beforeSend: function() {
                        showLoader();
                    }
	            });

	            saveBlackoutDayPromise.done(function(response, textStatus, jqXHR) {
	            	
	                if(response.error == false) {
	                     location.reload();
	                } else {
	                    alert(response.message);
	                }
	            }).always(function() {
                    hideLoader();
                });

                return false;   
            });
		},
		this.editBlackoutDayPopup = function(blackoutDay){
			var obj = this;
			var date = moment(blackoutDay[0].date).format('DD-MM-YYYY');

			$('#editBlackoutDayModal').modal();

			//enable disable save button
			var updateBtn = obj.editblackoutDaysForm.find('.update-submit-btn');
			updateBtn.removeAttr('disabled');
			$('.edit-holyday').on('input', function(){
		        if($(this).val().length !=0)
		            updateBtn.attr('disabled', false);            
		        else
		            updateBtn.attr('disabled',true);
		    });

			$('.edit-date').datepicker({
	                format: "dd-mm-yyyy",
	                todayHighlight: true
	            }).datepicker('setDate', date);

			$('.edit-holyday').val(blackoutDay[0].holyday);

			obj.updateBlackoutDay(blackoutDay[0].id);

		},
		this.updateBlackoutDay = function(id) {
			var obj = this;

			obj.editblackoutDaysForm.off('submit');
			obj.editblackoutDaysForm.on('submit', function() {
                var $this = $(this);
                var url = $this.attr('action');
                var params = $this.serialize();
                params += "&id=" + id;
           
                var updateBlackoutDayPromise = $.ajax({
	                url: url,
	                dataType: "json",
	                type: 'POST',
	                data: params,
	                beforeSend: function() {
                        showLoader();
                    }
	            });

	            updateBlackoutDayPromise.done(function(response, textStatus, jqXHR) {
	            	
	                if(response.error == false) {
	                     location.reload();
	                } else {
	                    alert(response.message);
	                }

	            }).always(function() {
                    hideLoader();
                });

                return false;   
            });
		},
		this.deleteBlackoutDay = function(id){
            var $promise = $.ajax({
                url: 'php/blackout-days/deleteBlackoutDay.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
        }

	};
}