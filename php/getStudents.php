<?php

ini_set('display_errors',"1");
include("Student.php");

$fullName = isset($_GET['name']);
if ($fullName) {
    $fullName = $_GET['name'];
}

$data = [];
if (isset($_GET['search_for'])) {
	$groupId = isset($_GET['group_id']) ? $_GET['group_id'] : null;
	$data = [
		'group_id' => $groupId
	];
}

$school_id = isset($_GET['school_id']) ? $_GET['school_id'] : null;

$studentObj = new Student;
$response = $studentObj->getStudents($fullName, $data, $school_id);
	
echo $response;
exit;