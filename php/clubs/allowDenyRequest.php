<?php 

if(!isset($_SESSION)) {
     session_start();
}

ini_set('display_errors',"1");

include("Club.php");
include($_SERVER['DOCUMENT_ROOT'].'/php/Constants.php');
include($_SERVER['DOCUMENT_ROOT'].'/php/Mailer.php');
include($_SERVER['DOCUMENT_ROOT'].'/php/Student.php');

$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$groupId = isset($_POST['group_id']) ? $_POST['group_id'] : null;
$groupName = isset($_POST['group_name']) ? $_POST['group_name'] : null;
$confirmationType = isset($_POST['confirmation_type']) ? $_POST['confirmation_type'] : null;

$clubObj = new Club;
$mail = new Mailer;
$studentObj = new Student;

$student = $studentObj->getStudentById($studentId);

$email = $student->username . Common::LIBERTY_EMAIL_DOMAIN;

$enviroment = Common::APP_ENV;
if ($enviroment == 'local') {
	$email = Common::STUDENT_TEST_EMAIL;
}

$mail->addAddress($email);

if ($confirmationType == Common::STATUS_ACCEPT) {
	$status = Common::STATUS_ACCEPT;
	$response = $clubObj->allowDenyClubRequest($groupId, $studentId, $status);
	$mail->sendAllowRequestEmail($student, $groupName);
} else {
	$status = Common::STATUS_DENY;
	$response = $clubObj->allowDenyClubRequest($groupId, $studentId, $status);
	$mail->sendDenyRequestEmail($student, $groupName);
}

echo $response;
exit;