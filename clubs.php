<?php

session_start();

ini_set('display_errors',"1");
include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_CLUBS);

// This is the previous version of session timeout--changed back to test

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION['LAST_ACTIVITY'] = time();
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//BEGIN entry_html();
function entry_html() {
    // begin content
    if (empty($_SESSION['LAST_ACTIVITY']))
    {
        $_SESSION['LAST_ACTIVITY'] = time();
    }
?>
    <h2>Club Manager</h2>
    <hr>
    <?php if (Common::isSuperAdmin() || Common::isAdmin()) { ?>
        <a class="btn btn-primary" href="?createClub=yes" 
            role="button">
            Create Club
        </a>
    <?php } ?>
    <?php if (!Common::isStudent()) { ?>
        <a class="btn btn-primary" 
            href="?viewManagedClubs=yes" 
            role="button" 
            style="margin-right: 5px;">
            Manage Clubs
        </a>
        <a class="btn btn-primary" 
            href="/clubs-request.php" 
            role="button" 
            style="margin-right: 5px;">
            Clubs Request 
        </a>
    <?php } ?>
    <?php if (Common::isStudent()) { ?>
        <a class="btn btn-primary" 
            href="?viewYourClubs=yes" 
            role="button">
            View your Clubs
        </a> 
    <?php } ?>
        <a class="btn btn-primary" href="clubs.php" role="button">View All Clubs</a>
    
<?php 

}

// END entry_html();

function viewClubs($dbh, $libertyEagle) { 
?>
    <br/>
    <br/>
    <p>
        <?php echo $libertyEagle;?> 
        Hour has a few clubs that you can join. Below are the available clubs. Clicking on the club name will give a description. You can email the club admin directly by clicking request to join. 
    </p>
    <br/>

    <div class="panel-group" id="accordion">

        <?php

            $commonObj = new Common;

            $getPendingRequestsQuery = "SELECT group_id from student_requests where student_id=" . $_SESSION['user_id'] . " AND status='" . Common::STATUS_PENDING . "'";
            $result1 = pg_exec($dbh, $getPendingRequestsQuery) or die("I can't SELECT for some reason. Here's the SQL statement: " . $getPendingRequestsQuery);
            $pendingToJoinGroupIds = pg_fetch_all($result1);
            $pendingIds = [];
            if ($pendingToJoinGroupIds) {
                foreach ($pendingToJoinGroupIds as $key => $pendingToJoinGroupId) {
                    array_push($pendingIds, $pendingToJoinGroupId['group_id']);
                }
            }

            $getUsersGroupIdQuery = "SELECT group_id FROM student_to_group WHERE student_id='" . $_SESSION['user_id'] . "'";

            $result2 = pg_exec($dbh, $getUsersGroupIdQuery) or die("I can't SELECT for some reason. Here's the SQL statement: " . $getUsersGroupIdQuery);
            $getUsersGroupIds = pg_fetch_all($result2);
            $userGroupIds = [];
            if ($getUsersGroupIds) {
                foreach ($getUsersGroupIds as $key => $getUsersGroupIds) {
                    array_push($userGroupIds, $getUsersGroupIds['group_id']);
                }
            }
            
            //$constr = "dbname=liberty_school user=dbadmin password=root host=liberty-school-postgres";
            //$dbh = pg_connect($constr) or die("Unable to connect to database\n\n");
            if (Common::isSuperAdmin()) {
                $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id  ORDER BY name DESC";
            } elseif(Common::isAdmin() || Common::isTeacher()) {
                $schoolId = isset($_SESSION['school_id']) ? $_SESSION['school_id'] : 0;
                $buildingId = $commonObj->getBuildingCodeById($schoolId);
                
                $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id WHERE building IN ('". $buildingId . "', '0') ORDER BY name DESC";
                
            } else {
                $schoolId = isset($_SESSION['school_id']) ? $_SESSION['school_id'] : 0;
                $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id WHERE building IN ('". $schoolId . "', '0') ORDER BY name DESC";
                
            }
            /*print_r($sql);
            exit();*/
            $i =1;
            $result = pg_exec($dbh, $sql) or die("I can't SELECT for some reason. Here's the SQL statement: " . $sql);
            while ($row = pg_fetch_assoc($result)) {
                        
        ?>    
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" 
                            data-parent="#accordion" 
                            href="#collapse<?php echo $i;?>">
                            <?php echo $row['name']; 
                                if(($_SESSION['user_type'] == "admin" && $_SESSION['school_id'] != Common::LIBERTY_NORTH_HIGH_SCHOOL) 
                                    || $_SESSION['school_id'] != Common::LIBERTY_HIGH_SCHOOL) {

                                    echo " - " . $row['building'];
                                }
                            ?> 
                        </a>
                    </h4>
                    
                    <?php if (!in_array($row['id'], $pendingIds) && !in_array($row['id'], $userGroupIds) && ($_SESSION['user_type'] == "student")) { ?>
                        <a style="display: inline-block; position: relative; float: right; top: -24px;" 
                            class="btn btn-primary btn-sm ask-to-join-btn"
                            data-group-id="<?php echo $row['id']; ?>"
                            data-group-name="<?php echo $row['name']; ?>"
                            role="button">
                            Ask to Join
                        </a>
                    <?php } ?>
                </div>
                <div id="collapse<?php echo $i;?>" class="panel-collapse collapse">
                    <div class="panel-body"><?php echo $row['description']; ?></div>
                </div>
            </div>
                    
        <?php 
                $i++;
            } 
        ?>            
    </div>

<?php
}

function viewManagedClubs($dbh, $libertyEagle) {
    $commonObj = new Common;
?>
    <br/>
    <br/>
    <p>
        <?php echo $libertyEagle;?> 
        Below these are the clubs you are in charge of/managing. You can edit and add students to each club be selecting the edit button. 
    </p>
    <br/>

    <div class="panel-group" id="accordion">
        <?php 
            //$constr = "dbname=liberty_school user=dbadmin password=root host=liberty-school-postgres";
            //$dbh = pg_connect($constr) or die("Unable to connect to database\n\n");
            if($_SESSION['username'] =="rthaler") {
                $sql = "select * from admin_to_group ag inner join groups g on ag.group_id = g.id inner join users u on ag.admin_id = u.id";
            } elseif (Common::isSuperAdmin()) {
               /*$sql = "select * from admin_to_group ag inner join groups g on ag.group_id = g.id inner join users u on ag.admin_id = u.id where ag.admin_id = '". $_SESSION['user_id'] . "'";*/
               $sql = "SELECT grp.id as group_id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id  ORDER BY name DESC";

            } elseif(Common::isAdmin()) {
                $buildingId = $commonObj->getBuildingCodeById($_SESSION['school_id']);
                $sql = "select g.id, g.name, g.building from admin_to_group ag inner join groups g on ag.group_id = g.id inner join users u on ag.admin_id = u.id where g.building = '". $buildingId . "' GROUP By g.id, g.name, g.building";
            } elseif(Common::isTeacher()) {
                $sql = "select g.id, g.name, g.building from admin_to_group ag inner join groups g on ag.group_id = g.id inner join users u on ag.admin_id = u.id where ag.admin_id = '". $_SESSION['user_id'] . "'";
            } else {
                $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id WHERE building ='". $_SESSION['school_id'] . "'ORDER BY name DESC";
            }
            $i =1;
            $result = pg_exec($dbh, $sql) or die("I can't SELECT for some reason. Here's the SQL statement: " . $sql);
            while ($row = pg_fetch_assoc($result)) {
        ?>    
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" 
                        data-parent="#accordion" 
                        href="#collapse<?php echo $i;?>">
                        <?php echo $row['name']; 
                            if($_SESSION['user_type'] =="admin" 
                                && $_SESSION['school_id'] != Common::LIBERTY_NORTH_HIGH_SCHOOL 
                                || $_SESSION['school_id'] != Common::LIBERTY_HIGH_SCHOOL) {
                        ?> 
                        - 
                        <?php echo $row['building'];}?> 
                    </a>
                </h4>
                <a data-id="<?php echo $row['id'];?>" 
                    style="position: relative; float: right; top: -24px;" 
                    class="btn btn-primary btn-sm delete-club-btn" 
                    role="button">
                    Delete
                </a>
                <a href="?editClub=yes&id=<?php echo $row['id'];?>" 
                    style="position: relative; float: right; top: -24px; margin-right: 5px;" 
                    class="btn btn-primary btn-sm" 
                    role="button">
                    Edit Club
                </a>
                <a href="add-students-to-clubs.php?group_id=<?php echo $row['id']; ?>&action=view&group_name=<?php echo $row['name']; ?>" 
                    style="position: relative; float: right; top: -24px; margin-right: 5px;" 
                    class="btn btn-primary btn-sm" 
                    role="button">
                    View Students
                </a>
                <a href="add-students-to-clubs.php?group_id=<?php echo $row['id']; ?>&group_name=<?php echo $row['name']; ?>&school_id=<?php echo $row['building']; ?>" 
                    style="position: relative; float: right; top: -24px; margin-right: 5px;" 
                    class="btn btn-primary btn-sm" 
                    role="button">
                    Add Students
                </a>
                <?php if (Common::isSuperAdmin() || Common::isAdmin()) { ?>
                 <!-- <a href="add-admins-to-clubs.php?group_id=<?php echo $row['id']; ?>&group_name=<?php echo $row['name']; ?>" 
                    style="position: relative; float: right; top: -24px; margin-right: 5px;" 
                    class="btn btn-primary btn-sm" 
                    role="button">
                    Add Club Admins
                                 </a> -->
                <?php } ?>
       
            </div>
        <div id="collapse<?php echo $i;?>" class="panel-collapse collapse">
            <div class="panel-body"><?php echo $row['description']; ?></div>
        </div>
    </div>
            
<?php 
    $i++;
    }
    if (!pg_fetch_all($result)) {
?>
    <div><b>No Club Found</b></div>
<?php 
    }
?>            
 
</div>

<?php

}

function viewYourClubs($dbh, $libertyEagle) { ?>
<br/>
<br/>
<p>These are the current clubs you are a part of for <?php echo $libertyEagle;?> Hour. If you wish to be removed from a club click on the Request Removal button to send the admin an email.</p><br/>

<div class="panel-group" id="accordion">
<?php 
    //$constr = "dbname=liberty_school user=dbadmin password=root host=liberty-school-postgres";
    //$dbh = pg_connect($constr) or die("Unable to connect to database\n\n");
    if($_SESSION['user_type'] =="admin" 
        && ($_SESSION['school_id'] != Common::LIBERTY_NORTH_HIGH_SCHOOL 
            || $_SESSION['school_id'] != Common::LIBERTY_HIGH_SCHOOL)) {
        $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id ORDER BY building DESC, name DESC ";
    }
    elseif($_SESSION['user_type'] =="admin" 
            || $_SESSION['user_type'] =="teacher") {
            $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, users.username FROM groups grp INNER JOIN users ON grp.admin_id = users.id WHERE building ='". $_SESSION['school_id'] . "'ORDER BY name DESC";
    }
    else { 
        $sql = "SELECT grp.id, grp.name, grp.description, grp.admin_id, grp.building, stg.student_id, users.username FROM groups grp INNER JOIN student_to_group stg ON grp.id = stg.group_id INNER JOIN users ON grp.admin_id = users.id WHERE building IN ('". $_SESSION['school_id'] . "', '0') AND student_id = '". $_SESSION['user_id'] ."' ORDER BY name DESC";
    }
    $i =1;
    $result = pg_exec($dbh, $sql) or die("I can't SELECT for some reason. Here's the SQL statement: " . $sql);
    while ($row = pg_fetch_assoc($result)) {
          
?>    
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>">
        <?php echo $row['name']; if($_SESSION['user_type'] =="admin" && ($_SESSION['school_id'] != Common::LIBERTY_NORTH_HIGH_SCHOOL || $_SESSION['school_id'] != Common::LIBERTY_HIGH_SCHOOL)){?> - <?php echo $row['building'];}?> </a>
      </h4>
        <!-- <a style="display: inline-block; position: relative; float: right; top: -24px;" 
            class="btn btn-primary btn-sm ask-to-remove-btn" 
            data-group-id="<?php echo $row['id']; ?>"
            data-group-name="<?php echo $row['name']; ?>"
            role="button">
            Ask to Remove
        </a> -->
       
    </div>
    <div id="collapse<?php echo $i;?>" class="panel-collapse collapse">
      <div class="panel-body"><?php echo $row['description']; ?></div>
    </div>
  </div>
            
<?php $i++; }
 
if (!pg_fetch_all($result)) {
?>
    <div><b>No Club Found</b></div>
<?php 
    }
?>
 
</div>

<?php

}


function createClub($dbh, $libertyEagle) { 
   
    ?>
<script>
$(document).ready(function(){
                    $("#clubAdmin").autocomplete({
                        source:'ajax.php',
                        minLength:2,
                        select: function(event, ui) {
                            $('#clubAdminID').val(ui.item.text);
                            }
                    });        
        });
                
</script>

<br/>
<br/>
<p>Below you are able to create new clubs for your school. Please give a good description for the club/group as these will be shown to the students explaining what they are for.</p><br/>

<form class="form-horizontal" method="post" id="createClubForm" action="/php/clubs/saveClub.php">
    <input type="hidden" id="clubAdminID" name="clubAdminID" value="<?php echo $_SESSION['user_id']; ?>">
    <?php
        $commonObj = new Common;
        $building = $commonObj->getBuildingCodeById($_SESSION['school_id']);

        if ($building) {
    ?>
        <div class="form-group">
            <label for="clubBuilding" class="control-label col-xs-2">Club Building</label>
            <div class="col-xs-10">
                <input type="hidden" id="clubBuilding" name="clubBuilding" value="<?php echo $building; ?>">
                <h5><b><?php echo $_SESSION['school_name']; ?></b></h5>
            </div>
        </div>
    <?php
        } else {
    ?>
        <div class="form-group">
            <label for="clubBuilding" class="control-label col-xs-2">Club Building</label>
            <div class="col-xs-10">
                <select class="form-control" id="clubBuilding" name="clubBuilding">
                    <?php
                        $schools = $commonObj->getSchools(); 
                        foreach ($schools as $key => $school) {
                    ?>
                        <option value="<?php echo $school['sasi_building_code']; ?>">
                            <?php echo $school['school_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <?php 
        }
    ?>
    <div class="form-group">
        <label for="clubName" class="control-label col-xs-2">Club Name</label>
        <div class="col-xs-10">
            <input type="text" class="form-control" id="clubName" name="clubName" placeholder="Name of Club">
        </div>
    </div>
    <div class="form-group">
        <label for="clubDescription" class="control-label col-xs-2">Description</label>
        <div class="col-xs-10">
            <textarea id="clubDescription" name="clubDescription" class="form-control" placeholder="Club Description"></textarea>
        </div>
    </div>
    <div class="form-group">
       <label for="clubAdmin" class="control-label col-xs-2">Club Assignment Dates</label>
       <div class="col-xs-10">
            <div class="input-group date">
                <input type="text" 
                    id="club-dates" 
                    class="form-control" 
                    name="clubDates"
                    size="40">
                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-th"></i>
                </span>
            </div>
       </div>
   </div>
   <div class="form-group">
        <label for="clubBuilding" class="control-label col-xs-2">Club Admins</label>
        <div class="col-xs-10">
            <select class="form-control" id="select-club-admins" name="admins[]" multiple="multiple">
                
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            <button type="submit" name="submit" class="btn btn-primary club-create-button" value="create">Create</button>
        </div>
    </div>
</form>

<?php

}

function createClubPost($dbh, $createClub){
   /* print_r($_POST);
    exit();*/
    
        $pv = array();
        
        // Required Fields
        if(strlen($_POST['clubName']) > 0)
        {
            $pv['name']=htmlspecialchars($_POST['clubName'], ENT_QUOTES);
        }
        else
        {
            print "<span style=\"color: red;\">Error: Required field Club Name not filled in.</style>\n";
            exit;
        }
        if(strlen($_POST['clubDescription']) > 0)
        {
            $clubDescription=iconv('CP1252', 'ASCII//TRANSLIT', $_POST['clubDescription']);
            $pv['description']=pg_escape_string($clubDescription);
        }
        else
        {
            print "<span style=\"color: red;\">Error: Required field Club Description not filled in.</style>\n";
            exit;
        }
        if(strlen($_POST['clubAdminID']) > 0)
        {
            $pv['admin_id']=htmlspecialchars($_POST['clubAdminID'], ENT_QUOTES);
        }
        else
        {
            print "<span style=\"color: red;\">Error: Required field Club Admin is not filled in.</style>\n";
            exit;
        }
        if(strlen($_POST['clubBuilding']) > 0)
        {
            $pv['building']=htmlspecialchars($_POST['clubBuilding'], ENT_QUOTES);
        }
        else
        {
            print "<span style=\"color: red;\">Error: Required field Club Building is not filled in.</style>\n";
            exit;
        }
        

        
    if ($_POST['submit'] == "create") {

        $sql = "INSERT INTO groups (";
        foreach ($pv as $key => $value) {
              $sql .= "\"" . $key . "\",";
              $value_string .= "'" . $value . "',"; 
        }
        $sql = rtrim($sql, ',');
        $sql .= ",\"created\") VALUES (";
        $value_string = rtrim($value_string, ',');
        $sql .= $value_string;
        $sql .= ", NOW()) RETURNING id, admin_id";
        $result = pg_query($dbh, $sql) or die("I can't INSERT. ".pg_last_error($result)." Here's the SQL statement: " . $sql);
        
        $row = pg_fetch_row($result); 
        $group_id = $row['0'];
        $admin_id = $row['1'];
        
        if(pg_last_error($result))
         {
             echo pg_last_error($result);
             exit;
         }
         echo $dbh;
         $sql2 = "INSERT INTO admin_to_group (\"admin_id\",\"group_id\") VALUES (".$admin_id.",".$group_id.")";
         $result2 = pg_query($dbh, $sql2) or die("I can't INSERT. ".pg_last_error($dbh)." Here's the SQL statement: " . $sql2); 
         
   ?>
    <h4>Club Created</h4><p>Redirecting to Clubs Page...<br /> If redirect doesn't work please <a href="http://leh-n1.liberty.k12.mo.us/clubs.php">click here.</a></p>
    <meta http-equiv="Refresh" content="2;URL=/clubs.php" />
<?php

}    

}

// start editClub function
function editClub($dbh, $id=0, $libertyEagle) {
    $row = array();
    if ($dbh != "0" && $id != "0") {
        $sql = "select * from admin_to_group ag inner join groups g on ag.group_id = g.id inner join users u on ag.admin_id = u.id WHERE group_id = '" . addslashes($id) . "'";
        
        $result = pg_exec($dbh, $sql) or die("I can't SELECT for some reason. Here's the SQL statement: " . $sql);
        $row = pg_fetch_assoc($result);
        foreach ($row as $key => $value) {
            $row[$key] = stripslashes($value);
        }

        $datesQuery = "SELECT date FROM club_dates WHERE group_id='$id'";
        $result2 = pg_exec($dbh, $datesQuery) or die("I can't SELECT for some reason. Here's the SQL statement: " . $datesQuery);
        if (!$result2) {
            echo "An error occurred.\n";
            exit;
        }

        $days = pg_fetch_all($result2);
        $dates = [];
        if ($days) {
            foreach ($days as $key => $day) {
                array_push($dates, (string)date("d-m-Y", strtotime($day['date'])));
            }
            $dates = json_encode($dates);
        }


        $clubAdminsQuery = "SELECT * FROM teachers as t INNER JOIN admin_to_group as atg ON t.id = atg.admin_id WHERE group_id='$id'";
        $result3 = pg_exec($dbh, $clubAdminsQuery) or die("I can't SELECT for some reason. Here's the SQL statement: " . $clubAdminsQuery);
        if (!$result3) {
            echo "An error occurred.\n";
            exit;
        }

        $clubAdmins = pg_fetch_all($result3);
        $clubAdminsJson = json_encode($clubAdmins);
        
   }

?>
    <script>
        $(document).ready(function(){
            $("#clubAdmin").autocomplete({
                source:'ajax.php',
                minLength:2,
                multiselect:true,
                select: function(event, ui) {
                    $('#clubAdminID').val(ui.item.text);
                }
            }).val('<?php echo $row['full_name']; ?>');

            $("#clubAdminID").val('<?php echo $row['admin_id']; ?>');

            var clubObj = clubs();
            clubObj.selectDatesForClub()

            <?php if ($dates) { ?>
                $('#club-dates, .input-group.date').datepicker('setDate', <?php echo $dates; ?>);
            <?php } ?>

            var selectedAdmins = <?php echo $clubAdminsJson; ?>

            $('.select-club-admins').select2('data', selectedAdmins)
        });
                    
    </script>

    <br/>
    <br/>
    <p>Below you are able to add students to the club and edit club information. </p>
    <br/>

    <form class="form-horizontal" method="post">
        <input type="hidden" name="group_id" value="<?php echo $row['group_id']; ?>">
        <input type="hidden" id="clubAdminID" name="clubAdminID" value="<?php echo $row['admin_id']; ?>">
        <div class="form-group">
            <label for="clubBuilding" class="control-label col-xs-2">Club Building</label>
            <div class="col-xs-10">
                <input type="hidden" id="clubBuilding" name="clubBuilding" value="<?php echo $row['building']; ?>">
                <input type="hidden" id="schoolId" name="schoolId" value="<?php echo $row['building_id']; ?>">
                <h5><b><?php echo $row['school_name']; ?></b></h5>
            </div>
        </div>
        <div class="form-group">
            <label for="clubName" class="control-label col-xs-2">Club Name</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="clubName" name="clubName" value="<?php echo $row['name']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="clubDescription" class="control-label col-xs-2">Description</label>
            <div class="col-xs-10">
                <textarea id="clubDescription" name="clubDescription" class="form-control" placeholder="Club Description"><?php echo $row['description']; ?></textarea>
            </div>
        </div>
        <div class="form-group">
           <label for="clubAdmin" class="control-label col-xs-2">Dates</label>
           <div class="col-xs-10">
                <div class="input-group date">
                    <input type="text" 
                        id="club-dates" 
                        class="form-control" 
                        name="clubDates"
                        size="40">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
           </div>
       </div>
       <div class="form-group">
            <label for="clubBuilding" class="control-label col-xs-2">Club Admins</label>
            <div class="col-xs-10">
                <select class="form-control" id="select-club-admins" name="admins[]" multiple="multiple">
                    <?php foreach ($clubAdmins as $key => $clubAdmin) { ?>
                        <option value="<?php echo $clubAdmin['admin_id']; ?>" selected="selected">
                            <?php echo $clubAdmin['full_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" name="submit" class="btn btn-primary" value="update">Update</button>
            </div>
        </div>
    </form>
<?php 
}
// End editClub function

// Update club function
function updateClubPost($dbh, $club)
{
    $query = "UPDATE groups SET name = $1, description = $2, admin_id = $3, building = $4 WHERE id = $5";
    $result = pg_prepare($dbh, "update_query", $query);
    $result = pg_execute($dbh, "update_query", [$club['clubName'], $club['clubDescription'], $club['clubAdminID'], $club['clubBuilding'], $club['group_id']]);

    if(!$result) {
        echo "Clubs Detail could not be updated. Please try again.";
        exit;
    }




    // update club dates
    $clubObj = new Club($dbh);

    $oldClubDates = $clubObj->getClubDates($club['group_id']);
    $oldClubDates = array_column($oldClubDates, 'date');

    $newDates = explode(',', $club['clubDates']);
    
    $updatedDates = [];
    foreach ($newDates as $key => $date) {
        $date = str_replace('/', '-', $date);
        $date = date("Y-m-d", strtotime($date));
        array_push($updatedDates, $date);
    }

    $addedOrDeletedDates = array_merge(array_diff($oldClubDates, $updatedDates), array_diff($updatedDates, $oldClubDates));

    $deletedDates = array_values(array_intersect($addedOrDeletedDates, $oldClubDates));
    $newAddedDates = array_values(array_intersect($addedOrDeletedDates, $updatedDates));


    if (count($addedOrDeletedDates) > 0) {
        $clubObj->deleteClubDates($club['group_id']);
        $clubObj->addClubDates($club['group_id'], $club['clubDates']);
        
        if (count($newAddedDates) > 0) {
            $clubObj->addAssignmentWhenAddDate($club['group_id'], $newAddedDates);        
        }

        if (count($deletedDates)) {
            $clubObj->deleteAssignmentWhenDeleteDate($club['group_id'], $deletedDates);        
        }
    }
    

    $clubObj->deleteClubAdmins($club['group_id']); //delete all admins of group

    $clubAdminIds = $club['admins']; 
    array_unshift($clubAdminIds, $club['clubAdminID']);

    $clubObj->addClubAdmins($club['group_id'], array_unique($clubAdminIds));
   
    // header('Location: clubs.php'); /* Redirect browser */
    ?>
    <h4>Club Updated</h4>
    <p>Redirecting to Clubs Page...
        <br /> If redirect doesn't work please 
        <a href="http://leh-n1.liberty.k12.mo.us/clubs.php">click here.</a>
    </p>
    <meta http-equiv="Refresh" content="2;URL=/clubs.php" />
    <?php 
    exit();
}
// End update club function


if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

?>
<!--Content-->

<!--Login Check-->
<?php 
    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }

        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Club Manager";
        
        gen_header($title);
        
        entry_html();

        if (isset($_GET['viewYourClubs']) && $_GET['viewYourClubs'] == "yes") {
            viewYourClubs($dbh, $libertyEagle);
        } elseif (isset($_POST['submit']) && $_POST['submit'] == "create") {
            createClubPost($dbh, $_POST);
        } elseif (isset($_GET['createClub']) && $_GET['createClub'] == "yes") {
            createClub($dbh, $libertyEagle);
        } elseif (isset($_GET['viewManagedClubs']) && $_GET['viewManagedClubs'] == "yes") {
            viewManagedClubs($dbh, $libertyEagle);
        } elseif (isset($_POST['submit']) && $_POST['submit'] == "update") {
            updateClubPost($dbh, $_POST);
        } elseif (isset($_GET['editClub']) && $_GET['editClub'] == "yes" && (isset($_GET['id']))) {
            editClub($dbh, $_GET['id'], $libertyEagle);
        } elseif (isset($_GET['add-students']) && $_GET['add-students'] == "yes") {
            $id = isset($_GET['id']) ? $_GET['id'] : null;
            addStudents($dbh, $id);
        } else {
            viewClubs($dbh, $libertyEagle);
        }
    } else {
        header('Location: login.php');
        exit;
    }
    include("footer.php");
?>