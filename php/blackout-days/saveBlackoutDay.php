<?php

session_start();

ini_set('display_errors',"1");
include("BlackoutDay.php");

$date = isset($_POST['date']) ? $_POST['date'] : null;
if ($date) {
	$date = date("Y-m-d", strtotime($date));
}

$holyday = isset($_POST['holyday']) ? $_POST['holyday'] : null;
$buildings = isset($_POST['building']) ? (array)$_POST['building'] : null;

$blackoutDayObj = new BlackoutDay;
$response = $blackoutDayObj->store($date, $holyday, $buildings);

echo $response;
exit;