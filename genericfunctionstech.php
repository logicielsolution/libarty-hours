<?php


function gen_header_old($title, $css="", $printcss="") {

?><!DOCTYPE html>
<html lang="en-US">
<head>
  <title><?php echo $title; ?></title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">

  <link href="css/genericfunctionstech.css" rel="stylesheet" type="text/css">
<?php
    if(!empty($css))
    {
?>
  <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>
<?php
    if(!empty($printcss))
    {
?>
  <link rel="stylesheet" media="print" href="<?php echo $css; ?>" type="text/css" />
<?php
    }
?>
  <script language="javascript" type="text/javascript" src="/datetimepicker.js"></script>



            <style>
              .actli{ background-color: #e7e7e7; color:#2b2b2b;  }
              .acta{ color:#2b2b2b !important;  }
            </style>



</head>
<body bgcolor="#FFFFFF" background="https://web.liberty.k12.mo.us/images/bk-Lines.gif">
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" id="maintablecontainer">
  <tr>
    <td align="left" valign="middle" class="text-large-black" bgcolor="#20438C">
      <div style="width: 87px; height: 59px;">
        <a href="http://web.liberty.k12.mo.us"><img src="https://web.liberty.k12.mo.us/images/lg-Main2.gif" border="0" alt="Liberty Public Schools" title="Liberty Public Schools" /></a>
      </div>
      <div style="position: absolute; left: 90px; top: 20px; right: auto; bottom: auto; vertical-align: middle; text-align: left; width: auto; height: 59px;color: #FFFFFF; font-family: Arial, sans-serif; font-weight: bold;">
        <?php echo $title; ?>
      </div>
    </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="middle" bgcolor="#6882BD" class="text-small-black" colspan="2" nowrap="nowrap">
      &nbsp;
    </td>
  </tr>
  <tr>
    <td>
      <div id="dropshadow">
      </div>
    </td>
  </tr>
  <tr>
    <td width="100%" class="text-large-black">
      <table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
        <tr>
          <td width="100%" align="left" valign="middle" class="text-large-black">
<?php

} // END gen_header

function gen_header($title, $css="", $printcss="", $backLink="/", $jsscript="") {
if ((isset($_SESSION['building']) && $_SESSION['building'] == "1070") 
    || (isset($_SESSION['building']) && $_SESSION['building'] == "22")){
    $libertyEagle = "Eagle" ;
}elseif((isset($_SESSION['building']) && $_SESSION['building'] == "510") 
    || (isset($_SESSION['building']) && $_SESSION['building'] == "3")){
    $libertyEagle = "Liberty";
}elseif (isset($_SESSION['building']) && $_SESSION['building'] != ""){
    $libertyEagle  = "Liberty/Eagle";
}else{
    $libertyEagle = "No School";
}
?>

<!DOCTYPE html> 
<html> 
  <head>
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="css/select2.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    
    <!-- Bootstrap core JS -->
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src='lib/moment.min.js'></script>
    <script src='lib/moment-timezone.min.js'></script>
    <script src='lib/moment-timezone-with-data.min.js'></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/clubs.js"></script>

    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php 
    if (!empty($jsscript))
    {
        echo '<script type="text/javascript" src="'.$jsscript.'"></script>';
    }
    ?>
    <!-- <script type="text/javascript" src="<?php echo $jsscript;?>"></script>-->
  <link href="css/genericfunctionstech.css" rel="stylesheet" type="text/css" />
<?php
    if(!empty($css))
    {
?>
  <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>
<?php
    if(!empty($printcss))
    {
?>
  <link media="print" href="<?php echo $printcss; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>

</head>
<body>
    <div id="loader"></div>
    <div id="blur-backgroung"></div>
    <div id="container">  
<div id="header">
    <nav class="navbar navbar-custom navbar-static-top">
      <div class="container">
        <div id="logo">
            <?php 
                if (isset($_SESSION['role']) 
                    && ($_SESSION['role'] == Common::TEACHER 
                                        || $_SESSION['role'] == Common::STUDENT) ) 
                    {
            ?>
                <a href="http://leh.liberty.k12.mo.us/" target="_blank">
                    <img src="img/LEH_logo.png" alt="LPS " border="0">
                </a>
            <?php } else { ?>
                <a href="http://leh.liberty.k12.mo.us/" target="_blank">
                    <img src="img/liberty_logo.png" alt="LPS " border="0">
                </a>
            <?php } ?>
        <?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != ""){ ?>
        <span id="login-status">Logged in as: <?php echo $_SESSION['full_user_name']; ?>
        <br> <a href="logout.php">Log Out</a></span>
            <?php } ?>
            
        </div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#">
                <?php 
                    if ((isset($_SESSION['building']) && $_SESSION['building'] != "") 
                        || (isset($_SESSION['school']) && $_SESSION['school'] != ""))
                    { 
                        echo "$libertyEagle Hour 2017-2018"; 
                    }else{
                        echo "Liberty/Eagle Hour Login"; 
                    }
                ?>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == "student"){ ?>
                <li><a href="/studentAssignments.php">Assignments</a></li>
                <li><a href="/clubs.php">Clubs</a></li>
            <?php }elseif(Common::isTeacher()){ ?>
                <li><a href="/assignment-list.php">Assignments</a></li>
                <li><a href="/teacherCalendar.php">Assign Students</a></li>
                <li><a href="/absent-students.php">Absences</a></li>
                <li><a href="/clubs.php">Clubs</a></li>
                 <?php
                    $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
                    $commonObj = new Common;
                    if ($commonObj->isTeacherHasClub($userId)) { 
                ?>
                    <li>
                        <a href="club-report.php">Club Report</a>
                    </li>
                <?php } ?>
            <?php }elseif(Common::isSuperAdmin() || Common::isAdmin()){ ?>
                <li><a href="/assignment-list.php">Assignments</a></li>
                <li><a href="/assignStudents.php">Assign Students</a></li>
                <li><a href="/absent-students.php">Absences</a></li>
                <li><a href="/clubs.php">Clubs</a></li>
                <li><a href="/blackout-days.php">Blackout Days</a></li>
                <li><a href="/student-requests.php">Student Requests History</a></li>
                <li class="dropdown">
                    <a href="#" 
                        class="dropdown-toggle" 
                        data-toggle="dropdown" 
                        role="button" 
                        aria-haspopup="true" 
                        aria-expanded="false">
                            More 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="detention-list.php">Detentions</a>
                        </li>
                        <li>
                            <a href="report-list.php">Assignment Report</a>
                        </li>
                        <li>
                            <a href="club-report.php">Club Report</a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
    <div class="container" >
<h2><?php echo $title; ?></h2>        
<?php

} // END gen_header

function gen_footer() {

?>
        
<div id="footer">
      <div class="container">
        <p class="muted credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p>
      </div>
    </div>
    <?php 
        /*echo '<pre>';
        var_dump($_SESSION);
        echo '</pre>';*/
    ?>
</body>
</html>
<?php

} // END gen_footer

function gen_ext_footer($links, $directinq=0) {

   /* The gen_ext_footer() allows you to place a menu of links at the bottom of your page
      Since I don't want to code a bunch here, you'll have to create an array of links in html
      e.g., <a href="blah.php" class="blah">Blah</a> and pass it to this function.
   */
   if ((!$links) || ($links == "") || (! is_array($links))) {
      $links = array();
   }
/*
   elseif (count($links) == 1) {
      $links = array($links);
   }
*/
?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" align="center">
<!--
  <tr>
    <td class="text-small-black" align="center" valign="middle">
      <?php
   $newline = 6;
   $i = 0;
   foreach ($links as $linktext => $link) {
      if ($i == $newline) {
         echo "<br />";
         $i = 0;
      }
      echo "[<a href=\"" . $link[0] . "\" class=\"accentfeedback\">" . $linktext . "</a>] ";
      $i++;
   }
   ?>
    </td>
  </tr>
-->
  <tr>
   <td class="text-small-black" align="center" valign="middle">
    <select name="woooha" onChange="location.href = this[this.selectedIndex].value">
     <option>JUMP TO...</option>
      <?php
   $logOutLink = 0;
   foreach ($links as $linktext => $link) {
      echo "<option value=\"" . $link[0] . "\">" . $linktext . "</option>\n";
      if ($logOutLink == 0) {
         echo "<option value=\"index.php?logout=1\" style=\"background-color: #FF0000; color: #FFFFFF; text-align: center;\"> --- LOG OUT HERE --- </option>\n";
         $logOutLink = 1;
      }
   }
   ?>
     </select>
    </td>
  </tr>
<?php
    if($directinq == 0)
    {
?>
  <tr>
    <td class="text-small-black">
      <hr style="margin-top: 2em; width: 575px; text-align: center;" />
      <p style="font-size: smaller; text-align: center;">Direct inquiries to: <a href="mailto:webmaster@liberty.k12.mo.us">webmaster@liberty.k12.mo.us</a>
        <!-- You are at http://web.liberty.k12.mo.us/, last updated May 12, 2004 15:03.-->
      </p>
    </td>
  </tr>
<?php
    }
?>
</table>
<br/>
</body>
</html>
<?php

} // END gen_ext_footer

function gen_css_header($title, $css="", $printcss="", $backLink="/", $jsscript="") {

?>
<!DOCTYPE html> 
<html> 
  <head> 
    <title><?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
 <script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="js/bootstrap.js"></script>
<style type="text/css">
.sbutton
{
    color: #000;
    background-color: #ddd;
    border: 1px solid #eee;
    border-bottom-color: #888;
    border-right-color: #888;
    padding: 5px;
    text-align: center;
    width: 50%;
    cursor: pointer;
    cursor: hand;
}

.sbutton a:link, a:active, a:focus
{
    color: #000;
    text-decoration: none;
    cursor: pointer;
    cursor: hand;
}
.sacontent p
{
    padding-left: 3em;
}
.noindent
{
    padding-left: 0 !important;
}
</style>
   <?php 
    if (!empty($jsscript))
    {
        echo '<script type="text/javascript" src="'.$jsscript.'"></script>';
    }
    ?>
  <!--<script type="text/javascript" src="<?php echo $jsscript;?>"></script>-->
  <link href="css/genericfunctionstech.css" rel="stylesheet" type="text/css" />
<?php
    if(!empty($css))
    {
?>
  <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>
<?php
    if(!empty($printcss))
    {
?>
  <link media="print" href="<?php echo $printcss; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>

  </head>
<body><section id="navbar">
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="row-fluid">
   		 <div class="span12">
         	<div class="span8" ><img id="LPSLogo" src="/img/web_logo.png"/></div>
         </div>
   		 
    </div>

<div id="techresourcesNav" class="navbar-inner">
      <div style="width: auto;" class="container">
        <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a href="http://web.liberty.k12.mo.us/techresources" id="brand" class="brand">Tech Resources</a>
        <div class="nav-collapse">
          <ul class="nav">

	    <li><a href="http://www.liberty.k12.mo.us" class="navLinks">District Website</a></li>
            <li><a href="logout.php" class="navLinks">Logout</a></li>

          </ul>
	  	  	  
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar -->
</section>
    <div id="contain" class="container">
<div id="contain" class="main" style="margin: 0 0 0 2em; float: left;">  <div class="content" style="font-size: 12pt; padding-top: 120px;">
<h2><?php echo $title; ?></h2>        
<?php

} // END gen_header

function gen_css_noheader($title, $css="", $printcss="", $backLink="/", $jsscript="") {

?>
<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
 <script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="js/bootstrap.js"></script>

<style type="text/css">
.sbutton
{
    color: #000;
    background-color: #ddd;
    border: 1px solid #eee;
    border-bottom-color: #888;
    border-right-color: #888;
    padding: 5px;
    text-align: center;
    width: 50%;
    cursor: pointer;
    cursor: hand;
}

.sbutton a:link, a:active, a:focus
{
    color: #000;
    text-decoration: none;
    cursor: pointer;
    cursor: hand;
}
.sacontent p
{
    padding-left: 3em;
}
.noindent
{
    padding-left: 0 !important;
}
</style>
   <?php 
    if (!empty($jsscript))
    {
        echo '<script type="text/javascript" src="'.$jsscript.'"></script>';
    }
    ?>
  <!--<script type="text/javascript" src="<?php echo $jsscript;?>"></script>-->
  <link href="css/genericfunctionstech.css" rel="stylesheet" type="text/css" />
<?php
    if(!empty($css))
    {
?>
  <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>
<?php
    if(!empty($printcss))
    {
?>
  <link media="print" href="<?php echo $printcss; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>

  </head>
<body><section id="navbar">
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="row-fluid">
   		 <div class="span12">
         	<div class="span8" ><img id="LPSLogo" src="/img/web_logo.png"/></div>
         </div>

    </div>

<div id="techresourcesNav" class="navbar-inner">
      <div style="width: auto;" class="container">
        <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a href="http://web.liberty.k12.mo.us/techresources" id="brand" class="brand">Tech Resources</a>
        <div class="nav-collapse">
          <ul class="nav">

	    <li><a href="http://www.liberty.k12.mo.us" class="navLinks">District Website</a></li>
            <li><a href="logout.php" class="navLinks">Logout</a></li>

          </ul>

        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar -->
</section>
    <div id="contain" class="container">
<div id="contain" class="main" style="margin: 0 0 0 2em; float: left;">  <div class="content" style="font-size: 12pt; padding-top: 120px;">
<?php

} // END gen_css_noheader

function gen_css_footer($links="", $directinq=0) {

?>
 </div>
</div>
    </div>
    </body>
</html>
<?php

} // END gen_css_footer

function gen_ext_css_footer($links, $directinq=0)
{

   /* The gen_ext_footer() allows you to place a menu of links at the bottom of your page
      Since I don't want to code a bunch here, you'll have to create an array of links in html
      e.g., <a href="blah.php" class="blah">Blah</a> and pass it to this function.
   */
   if ((!$links) || ($links == "") || (! is_array($links))) {
      $links = array();
   }
/*
   elseif (count($links) == 1) {
      $links = array($links);
   }
*/
?>
  </div>
  <div id="footercontainer" class="text-small-black" style="text-align: center; margin: 0; padding: 0; width: 100%;">
   <select name="woooha" onChange="location.href = this[this.selectedIndex].value">
    <option>JUMP TO...</option>
   <?php
   $logOutLink = 0;
   foreach ($links as $linktext => $link) {
      echo "<option value=\"" . $link[0] . "\">" . $linktext . "</option>\n";
      if ($logOutLink == 0) {
         echo "<option value=\"index.php?logout=1\" style=\"background-color: #FF0000; color: #FFFFFF; text-align: center;\"> --- LOG OUT HERE --- </option>\n";
         $logOutLink = 1;
      }
   }
   ?>
   </select>
  </div>
<?php
    if($directinq == 0)
    {
?>
  <div class="text-small-black" style="text-align: center; margin: 0; padding: 0; width: 100%;">
   <hr style="margin-top: 2em; width: 575px; text-align: center;" />
   <p style="font-size: smaller; text-align: center;">Direct inquiries to: <a href="mailto:webmaster@liberty.k12.mo.us">webmaster@liberty.k12.mo.us</a>
   </p>
  </div>
<?php
    }
?>
</div>



</body>
</html>
<?php

} // END gen_ext_css_footer
