$( function() {
    if ($('.student-calendar-page').length < 1) {
        return false;
    }
    var studentAssignObj = singleStudentAssignments();

    studentAssignObj.setStudentId(STUDENT_ID, SCHOOL_ID);
    studentAssignObj.bindObjects();
});

function singleStudentAssignments() {
    return new function() {
        this.$fullCalenderObj  = $('#student-calendar'),
        this.$searchTeacherInput = $( "#search-teacher-for-assignment_request" ),
        this.studentId         = null;
        this.blackoutDays      = [];
        this.blackoutDaysData         = [];
        this.selectedStudentSchoolId  = null;
    	this.bindObjects = function() {
            var obj = this;
            
            obj.blackoutDays = obj.getblackoutDays();
            
        },
        this.setStudentId = function(studentId, schoolId){
        	this.studentId = studentId;
            this.selectedStudentSchoolId = schoolId;
        },
        this.studentCalendar = function() {
        	var obj = this;
            obj.nextMonthClick();
            obj.prevMonthClick();

        	var assignments = this.getStudentAssignments();

        	obj.$fullCalenderObj.fullCalendar({
                // put your options and callbacks here
                header: {
                    left:   '',
                    center: 'title',
                    right:  'prev,next'
                },
                editable: false,
                eventRender: function(event, element) {
                    // console.log(event);
                    element.find('.fc-title').append("<br/>" + ((event.teacher && event.teacher.length > 23) ? event.teacher.substring(0,23) + '...' : event.teacher));

                    var isGroupAssignment = false;
                    var isRequestedAssignment = false;
                    
                    if(event.group_id) {
                        isGroupAssignment = true;
                    }

                    if (event.assignment_unassigned && event.assignment_unassigned == 't') {
                        isRequestedAssignment = true;
                        event.color = 'grey';
                    }
                    
                    if (event.note) {
                        var note = event.note;
                        if(note.length > 15) {
                            note = event.note.substring(0,15) + '...';
                        };
                        
                        if (!isGroupAssignment) {
                            element.find('.fc-title').append("<br/><span class='note-in-event'>NOTE: " + note + "</span>");
                            
                            // show full note in toottip
                            element.tooltip({
                                title: 'Note: ' + event.note,
                                placement: 'bottom'
                            });
                        } else {
                             element.find('.fc-title').append("<br/><span class='note-in-event'>Club Note: " + note + "</span>");
                            
                            // show full note in toottip
                            element.tooltip({
                                title: 'Club Note: ' + event.note,
                                placement: 'bottom'
                            });
                        }
                    }  

                    // get difference between current date and event date.
                    // var isFutureDates = (event.start.diff(moment().format('YYYY-MM-DD HH:mm:ss'), 'days') <= 0) ? false : true;
                    var currentDate = moment().format('YYYY-MM-DD');
                    var isFutureDates = (currentDate >= moment(event.start).format('YYYY-MM-DD')) ? false : true;
                    
                    // if group assingment then set diff color
                    if(isGroupAssignment) {
                        event.color = "#41bef4"
                    }

                    // if past dates assingment then set diff color
                    if(!isFutureDates) {
                        event.color = "#f49241";
                    }

                    if (event.absent == 't') {
                        event.color = '#e57373';
                    } else if (event.absent == 'f') {
                        event.color = '#81c784';

                    }
                    //If the event is on blackout day, don't render it.
                    if(obj.isBlackoutDay(event._start._i, obj.blackoutDays))
                    {
                        return false;
                    }
                },
                dayClick: function (date, jsEvent, view) {
                    var currentDate = moment().format('YYYY-MM-DD');
                    var selectedDate = moment(date).format();

                     //get day event
                    var dayEvents = obj.getDayEvents(date);

                    if (isWorkingDays(date) 
                        && selectedDate > currentDate
                        && !obj.isBlackoutDay(selectedDate, obj.blackoutDays)
                        && dayEvents.length <= 0) {

                         $('#assignmentRequestPopupForm').find(':submit').attr('disabled', 'disabled');
                        obj.assignmentRequestModal(selectedDate);
                    }
                },
                dayRender: function (date, cell) {
                    if (obj.isBlackoutDay(moment(date._d).format('YYYY-MM-DD'), obj.blackoutDays)) {
                        
                        var heading;
                        for (var i = 0; i < obj.blackoutDaysData.length; i++) {
                            if (moment(date).format() == obj.blackoutDaysData[i].date
                                && obj.blackoutDaysData[i].building_id == obj.selectedStudentSchoolId) {
                                
                                var blackoutDayName = obj.blackoutDaysData[i].holyday;
                                if( blackoutDayName.length > 100) {
                                    blackoutDayName = obj.blackoutDaysData[i].holyday.substring(0,100) + '...';
                                };
                                heading = "<div class='holidayTooltip'><span style='cursor: pointer;'>" + blackoutDayName + "</span></div>"
                                
                                cell.css({"background-color" : "#efefef", 
                                    "padding" : "15px", 
                                    "text-align" : "center",
                                    "vertical-align" : "middle",
                                    "z-index" : "9",
                                    "position": "relative"
                                });
                                
                                cell.append(heading);

                                // show full holiday name
                                if(obj.blackoutDaysData[i].holyday.length > 0) {
                                    cell.find('.holidayTooltip').tooltip({
                                        title: 'Holiday: ' + obj.blackoutDaysData[i].holyday,
                                        placement: 'bottom'
                                    });
                                }
                            }
                        }
                    }
                }
            });
        },
        this.assignmentRequestModal = function(sDate){
            var obj = this;
            
            $('#assignmentRequestModal').modal();

            obj.$searchTeacherInput.autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "php/getTeachers.php",
                        dataType: "json",
                        data: {
                            name: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) {
                    console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );
                    
                    //enable teacher popup add button
                    $('#assignmentRequestPopupForm').find(':submit').removeAttr('disabled');

                    var selectedTeacher = {
                        id: ui.item.id,
                        name: ui.item.full_name
                    };

                    setTimeout(function() {
                        obj.$searchTeacherInput.val(ui.item.full_name);
                        obj.addRequestedAssignment(sDate, selectedTeacher.id);
                    }, 1);

                },

                focus: function( event, ui) {
                    obj.$searchTeacherInput.val(ui.item.full_name);
                    event.preventDefault();
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                ul.css('z-index', 1100);
                return $( "<li>" ).data("item.autocomplete", item)
                .append( item.full_name )
                .appendTo( ul );
            };


        },
        this.addRequestedAssignment = function(sDate, sTeacherId){
            var obj = this;
            
            var $assignmentRequestPopupForm = $('#assignmentRequestPopupForm');
            
            
            $assignmentRequestPopupForm.unbind();
            $assignmentRequestPopupForm.on('submit', function() {
                var $this = $(this);
                var url = $this.attr('action');
                var note = $('#assignment-request-note').val();

                $('#assignmentRequestModal').modal('hide');
                
                $.ajax({
                    url: 'php/assignmentRequest.php',
                    dataType: 'json',
                    type: 'post',
                    data: {
                        'date'              : sDate,
                        'teacher_id'        : sTeacherId,
                        'note'              : note
                    },
                    beforeSend: function() {
                        showLoader();
                    }
                })
                .done(function(response, textStatus, jqXHR) {
                    
                    if (response.error) {
                        showFlashMessage(response.message_type, response.message);
                        obj.getStudentAssignments();
                    } else {
                        var requestedAssingment = response;                    
                        obj.$fullCalenderObj.fullCalendar( 'renderEvent', requestedAssingment , 'stick');
                    }
                })
                .always(function() {
                    hideLoader();
                });

                return false;   
            });
        },
        this.nextMonthClick = function() {
            var obj = this;
            var $calenderObj = obj.$fullCalenderObj;

            $('#student-calendar').off('click', 'button.fc-next-button');
            $('#student-calendar').on('click', 'button.fc-next-button', function() {
                var date = $calenderObj.fullCalendar('getDate');
                var formatedDate = moment(date).format('YYYY-MM-DD');
                obj.getStudentAssignments(formatedDate);
            });
        },
        this.prevMonthClick = function() {
            var obj = this;
            var $calenderObj = obj.$fullCalenderObj;

            $('#student-calendar').off('click', 'button.fc-prev-button');
            $('#student-calendar').on('click', 'button.fc-prev-button', function() {
                var date = $calenderObj.fullCalendar('getDate');
                var formatedDate = moment(date).format('YYYY-MM-DD');
                obj.getStudentAssignments(formatedDate);
            });
        },
        this.getStudentAssignments = function(fromDate=null) {
        	var obj = this;
            setTimeout(function() {
                
                var startDate = fromDate;
                if (!fromDate) {
                    // currentDate = moment().format('YYYY-MM-DD');
                    var date = obj.$fullCalenderObj.fullCalendar('getDate');
                    var startDate = moment(date).format("YYYY-MM-01")
                }

                $.ajax({
                    url: 'php/getStudentAssignments.php',
                    dataType: 'json',
                    type: 'get',
                    data: {
                        'date'      : startDate,
                        'student_id': obj.studentId,
                        'isSaved': true
                    },
                     beforeSend: function() {
                        showLoader();
                    }
                })
                .done(function(response, textStatus, jqXHR) {
                    obj.$fullCalenderObj.fullCalendar('removeEvents');
                    obj.$fullCalenderObj.fullCalendar('addEventSource', response);
                    obj.$fullCalenderObj.fullCalendar('rerenderEvents' );

                    //both prev next used for day renders
                    obj.$fullCalenderObj.fullCalendar('prev');
                    obj.$fullCalenderObj.fullCalendar('next');
                })
                .always(function() {
                    hideLoader();
                });
            }, 100);
        },
        this.getblackoutDays = function() {
            console.log('student calender blackout');
            var obj = this;
            $.ajax({
                url: 'php/getBlackoutDays.php',
                dataType: 'json',
                type: 'get',
                beforeSend: function() {
                    showLoader();
                }
            })
            .done(function(response, textStatus, jqXHR) {
                obj.blackoutDaysData = response;
                obj.blackoutDays = [];
                for (var i = 0; i < response.length; i++) {
                    obj.blackoutDays.push(response[i].date);
                }

                obj.studentCalendar();
            })
            .always(function() {
                hideLoader();
            });
        },
        this.getDayEvents = function(date) {
            var dayEvents = $('#student-calendar').fullCalendar( 'clientEvents', function(event){
                return moment(event.start).isSame(date,'day');
            });

            return dayEvents;
        },
        this.isBlackoutDay = function(date, blackoutDays) {
            var obj = this;
            date = moment(date).format('YYYY-MM-DD');
            
            flag = false;
            for (var i = 0; i < obj.blackoutDaysData.length; i++) {
                if (date == obj.blackoutDaysData[i].date
                    && obj.blackoutDaysData[i].building_id == obj.selectedStudentSchoolId) {
                    flag = true;
                }
            }

            // return blackoutDays.includes(date);
            return flag;
        }
    };
}

//Hide Show loader when ajax call
$( function() {

    //on teacher modal close reset inputs
    $('#assignmentRequestModal').on('hidden.bs.modal', function (e) {
      $(this).find("input[type=text],textarea,select")
           .val('')
           .end();
    })

});

/*
 * Check is working day or not
 */
function isWorkingDays(date) {
    //If the event is on a Saturday and Sunday, don't render it.
    if(moment(date).isoWeekday() == 6 
        || moment(date).isoWeekday() == 7) {

        return false;
    }

    return true;
}