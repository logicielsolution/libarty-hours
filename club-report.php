<?php

session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_CLUB_REPORT);

// This is the previous version of session timeout--changed back to test

function entry_html($title, $dbh) {
?>

<script>
 $(document).ready(function(){
    //  select date for assignment list
    $('.from').datepicker({
        format: "mm-dd-yyyy",
        todayHighlight: true,
        beforeShowDay: function (date){
            if (date > new Date()) {
                return false;
            }
        }
    });

    $('.to').datepicker({
        format: "mm-dd-yyyy",
        todayHighlight: true,
        beforeShowDay: function (date){
            if (date > new Date()) {
                return false;
            }
        }
    }); 

    // end of date function 
 });

</script>

<?php
    
    $fromDate = isset( $_GET['fromDate'] ) ? $_GET['fromDate'] : null;  
    $toDate = isset( $_GET['toDate'] ) ? $_GET['toDate'] : null;  

    $linkData="";
    $num_page=10;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
    $start_from = ($page-1) * $num_page; 

    // create object and call  make query and assignment list 
    $objReports = new Reports();
    
    if((isset($_REQUEST['fromDate'] ) && isset($_REQUEST['toDate'])) 
        || isset($_REQUEST['club_id'] ) )
    {
        $clubId = isset($_REQUEST['club_id']) ? $_REQUEST['club_id'] : '';

        $fromDateArr = explode('-', $_REQUEST['fromDate']);
        $toDateArr = explode('-', $_REQUEST['toDate']);
        
        if ($_REQUEST['fromDate']) {
            $fromDate = $fromDateArr[1] . '-' . $fromDateArr[0] . '-' . $fromDateArr[2];
        }

        if ($_REQUEST['toDate']) {
            $toDate = $toDateArr[1] . '-' . $toDateArr[0] . '-' . $toDateArr[2];
        }

        $query= $objReports->makeClubReportQuery($fromDate ,$toDate ,$clubId);                    
        
        //create link for pagination
         $linkData="&fromDate=".$_REQUEST['fromDate'];
         $linkData.="&toDate=".$_REQUEST['toDate'];
         $linkData.="&clubId=".$clubId;

    }
    else
    {
        $query = $objReports->makeClubReportQuery();
    }
    
    $reportType = Common::REPORT_TYPE_CLUB;
    $reports = $objReports->getReportsListPagination($query, $num_page,$start_from, null, null, $reportType);
?>

<div style="margin-top: 20px;">
      
    <h4>
       <form action="createExcelFile.php" method="post">
              <b>Club Report</b>
             <input type="hidden" name="query" value="<?=$query; ?>" >
             <input type="hidden" name="fromDate" value="<?=$fromDate; ?>" >
             <input type="hidden" name="toDate" value="<?=$toDate; ?>" >
             <input type="hidden" name="reportType" value="<?=$reportType; ?>" >  
             <input type="submit" value="Export To CSV" class="btn btn-info pull-right mybtn"  />
        </form>
    </h4>
    
    <hr/>

    <div class="form-group ">
        <form>

            <div class="col-sm-3">
                From:  
                <input type="text"  
                    name="fromDate" 
                    class="form-control from" 
                    readonly
                    value="<?php echo isset($_GET['fromDate'])?$_GET['fromDate']:""; ?>" >
            </div>

            <div class="col-sm-3">                    
                To:     
                <input type="text" 
                    name="toDate" 
                    class="form-control to" 
                    readonly
                    value="<?php echo isset($_GET['toDate'])?$_GET['toDate']:""; ?>" >
            </div>

            <div class="col-sm-4">
                Club Name :    
                    <input type="text" 
                        class="form-control" 
                        id="searchClub" 
                        name="club_name"
                        value="<?php echo isset($_GET['club_name'])?$_GET['club_name']:""; ?>">
                    
                <input type="hidden" id="clubId" name="club_id" />
            </div>

            <div class="col-sm-2">
                <div class="pull-right">
                    <input  type="submit"  class="btn btn-primary" value="Search"
                      style="margin-top:20px"  >
                    
                    <a class="btn btn-default" href="club-report.php"  style="margin-top:20px"  >  Reset </a>
                </div>
            </div>

        </form> <!-- End of FIlter Form -->  
    </div>

    <br/>
    <br/>   
    <br/> 
    <table class="table table-bordered" style="margin-top: 20px;">
        <colgroup>
            <col style="width: 30%;" />
            <col style="width: 30%;" />
            <col style="width: 10%;" />
            <col style="width: 10%;" />
            <col style="width: 10%;" />
            <col style="width: 10%;" />
        </colgroup>
        <thead>
            <tr class="bg-primary">
                <th>Student Name</th>
                <th>Clubs</th>
                <th>Grade</th>
                <th>Assigned</th>
                <th>Missed</th>
                <th>Detentions</th>
             </tr>
        </thead>
        <tbody>
            <?php     
                if (count($reports)>0) {
                    foreach ((array)$reports as $key => $report) {
            ?>
                        <tr>
                            <td><?php echo  $report['student_name'] ;?></td>
                            <td>
                                <?php 
                                    $clubObj = new Club($dbh);
                                    $clubNames = $clubObj->getStudentClubsList($report['student_id']);
                                    echo "<ul type='disc'>";
                                    foreach ($clubNames as $key => $name) {
                                        echo "<li>" . $name . "</li>";
                                    }
                                    echo "<ul>";
                                ?>
                            </td>
                            <td><?php echo  $report['grade']  ;?></td>
                            <td><?php echo  $report['assigned'] ;?></td>
                            <td><?php echo  $report['absent'] ;?></td>
                            <td><?php echo  $report['detention'] ;?></td>
 
                        </tr>
            <?php   } // end of foreach
                } // end of if
                else
                {
                     echo "<tr><td colspan='6' class='text-center'>No Record Found</td></tr>";
                }
            ?>
        </tbody>
    </table>
</div>


<div class="text-center">
    <ul class="pagination" id="paginationID">
        <?php 

            $paginationquery = $query;
            $res = pg_query($dbh, $paginationquery);
            if (!$res) { echo "An error occurred.\n"; exit; }
            $total_pages = pg_num_rows($res); 

            if($total_pages>10) { 
                //$total_pages = count($assigns); 
                $totalPaginationNumber =ceil($total_pages/$num_page);
                $active="";

                $prev = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

                $disablePrev='';
                if( $prev==1) {
                    $disablePrev='class="btn disabled"';
                }

                $currentPageNumber = isset($_REQUEST['page'])?$_REQUEST['page']:1;

                $showPageNumber= array(1 ,$totalPaginationNumber);
                $showDots= array();

                if($totalPaginationNumber > 10){
                    $counter=1;
                    for($j=-2 ; $j<=2; $j++){
                        if($currentPageNumber <= 3)
                        {
                            array_push($showPageNumber, $counter++); // 1 2 3 4 5
                            array_push($showDots, $totalPaginationNumber-1);  // 12345....15             
                        }
                        else if($currentPageNumber>3 && $currentPageNumber <= $totalPaginationNumber-5)
                        {
                            array_push($showDots, 2 , $totalPaginationNumber-1);  // 1...5...15
                            array_push($showPageNumber, $currentPageNumber+$j); // 6 7 [8] 9 10                
                        }
                        else 
                        {
                            array_push($showDots, 2);  // 12345....15                             
                            array_push($showPageNumber, $totalPaginationNumber - ($counter++)); // 1 2 3 4 5                
                        }
                    }
                }
                else {
                    for($j=1 ; $j<=$totalPaginationNumber; $j++){  
                        array_push($showPageNumber, $j); // 1 2 3 4 5          
                    }      
                }

                echo "<li><a $disablePrev href='report-list.php?page=".($prev-1).$linkData."'> &lt; </a> </li>"; 


                for ($i=1; $i<= $totalPaginationNumber; $i++) { 
                   $active='';
                   $disable='';



                    if((isset($_REQUEST['page']) && $_REQUEST['page']==$i) || 
                        (! isset($_REQUEST['page'])&& $i==1))
                    { 
                      $active = "active";  
                      $disable= "class='btn disabled'";
                    }

                    if (in_array($i, $showPageNumber)){
                       echo "<li  class='{$active}' ><a {$disable} href='report-list.php?page=".$i.$linkData."'>".
                          $i."</a> </li>"; 
                      }
                    else if( in_array($i, $showDots)){
                        echo "<li><a  href='#'>.....</a> </li>"; 
                      }
                } // end of for loop 

                if(!isset($_REQUEST['page']) ){  $_REQUEST['page']=1; }

                $next = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

                $disableNext='';
                if( $prev==$totalPaginationNumber) {
                    $disableNext='class="btn disabled"';
                }
    
                echo "<li><a $disableNext href='report-list.php?page=".($prev+1).$linkData."'> &gt; </a> </li>"; 
            }
        ?>    
    </ul> 
</div>


<?php
}
?>

<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Club Report";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    
    include("footer.php");
?>