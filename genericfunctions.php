<?php

function gen_header($title, $css="", $printcss="") {

?><!DOCTYPE html>
<html lang="en-US">
<head>
  <title><?php echo $title; ?></title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <script type="text/javascript" src="https://web.liberty.k12.mo.us/jquery/jquery-current.min.js"></script>
  <link href="https://web.liberty.k12.mo.us/genericfunctions.css" rel="stylesheet" type="text/css">
<?php
    if(!empty($css))
    {
?>
  <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" />
<?php
    }
?>
<?php
    if(!empty($printcss))
    {
?>
  <link rel="stylesheet" media="print" href="<?php echo $css; ?>" type="text/css" />
<?php
    }
?>
  <script language="javascript" type="text/javascript" src="/datetimepicker.js"></script>
</head>
<body bgcolor="#FFFFFF" background="https://web.liberty.k12.mo.us/images/bk-Lines.gif">
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" id="maintablecontainer">
  <tr>
    <td align="left" valign="middle" class="text-large-black" bgcolor="#20438C">
      <div style="width: 87px; height: 59px;">
        <a href="http://web.liberty.k12.mo.us"><img src="https://web.liberty.k12.mo.us/images/lg-Main2.gif" border="0" alt="Liberty Public Schools" title="Liberty Public Schools" /></a>
      </div>
      <div style="position: absolute; left: 90px; top: 20px; right: auto; bottom: auto; vertical-align: middle; text-align: left; width: auto; height: 59px;color: #FFFFFF; font-family: Arial, sans-serif; font-weight: bold;">
        <?php echo $title; ?>
      </div>
    </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="middle" bgcolor="#6882BD" class="text-small-black" colspan="2" nowrap="nowrap">
      &nbsp;
    </td>
  </tr>
  <tr>
    <td>
      <div id="dropshadow">
      </div>
    </td>
  </tr>
    <tr>
    <td width="100%" class="text-large-black">
      <table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
        <tr>
          <td width="100%" align="left" valign="middle" class="text-large-black">
<?php

} // END gen_header

function gen_footer() {

?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<?php

} // END gen_footer

function gen_ext_footer($links, $directinq=0) {

   /* The gen_ext_footer() allows you to place a menu of links at the bottom of your page
      Since I don't want to code a bunch here, you'll have to create an array of links in html
      e.g., <a href="blah.php" class="blah">Blah</a> and pass it to this function.
   */
   if ((!$links) || ($links == "") || (! is_array($links))) {
      $links = array();
   }
/*
   elseif (count($links) == 1) {
      $links = array($links);
   }
*/
?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" align="center">
<!--
  <tr>
    <td class="text-small-black" align="center" valign="middle">
      <?php
   $newline = 6;
   $i = 0;
   foreach ($links as $linktext => $link) {
      if ($i == $newline) {
         echo "<br />";
         $i = 0;
      }
      echo "[<a href=\"" . $link[0] . "\" class=\"accentfeedback\">" . $linktext . "</a>] ";
      $i++;
   }
   ?>
    </td>
  </tr>
-->
  <tr>
   <td class="text-small-black" align="center" valign="middle">
    <select name="woooha" onChange="location.href = this[this.selectedIndex].value">
     <option>JUMP TO...</option>
      <?php
   $logOutLink = 0;
   foreach ($links as $linktext => $link) {
      echo "<option value=\"" . $link[0] . "\">" . $linktext . "</option>\n";
      if ($logOutLink == 0) {
         echo "<option value=\"index.php?logout=1\" style=\"background-color: #FF0000; color: #FFFFFF; text-align: center;\"> --- LOG OUT HERE --- </option>\n";
         $logOutLink = 1;
      }
   }
   ?>
     </select>
    </td>
  </tr>
<?php
    if($directinq == 0)
    {
?>
  <tr>
    <td class="text-small-black">
      <hr style="margin-top: 2em; width: 575px; text-align: center;" />
      <p style="font-size: smaller; text-align: center;">Direct inquiries to: <a href="mailto:webmaster@liberty.k12.mo.us">webmaster@liberty.k12.mo.us</a>
        <!-- You are at http://web.liberty.k12.mo.us/, last updated May 12, 2004 15:03.-->
      </p>
    </td>
  </tr>
<?php
    }
?>
</table>
<br/>
</body>
</html>