<?php

class BlackoutDay
{
	private $dbh;

	function __construct()
	{
		include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
		$this->dbCon = $dbh;
	}

	public function store($date, $holyday, $buildings)
	{
		$blankTime = '00:00:00';

		$query = "INSERT INTO blackout_days (date, holyday, building_id, time) VALUES ($1, $2, $3, $4)";
		
		$result = pg_prepare($this->dbCon, "insert_query", $query);

		foreach ($buildings as $key => $building) {
			$result = pg_execute($this->dbCon, "insert_query", [$date, $holyday, $building, $blankTime]);
			
			if(!$result) {
				$response['error'] = true;
				$response['message'] = "Blackout Day could not be saved. Please try again.";

				return $response;
			}
		}
		
		// $SQLresult = pg_query($this->dbCon, $query);

		$response = [
			'error' => false,
			'message' => "Blackout Day saved successfully."
		];

		return json_encode($response);
	}

	public function update($id, $date, $holyday)
	{
		$query = "UPDATE blackout_days SET date = $1, holyday = $2 WHERE id = $3";
		$result = pg_prepare($this->dbCon, "update_query", $query);
		$result = pg_execute($this->dbCon, "update_query", [$date, $holyday, $id]);

		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Blackout Day could not be updated. Please try again.";

			return $response;
		}

		$response = [
			'error' => false,
			'message' => "Blackout Day updated successfully."
		];

		return json_encode($response);
	}

	public function distroy($id)
	{
		$query = "DELETE FROM blackout_days WHERE id=$1";
		$result = pg_prepare($this->dbCon, "delete_query", $query);
		$result = pg_execute($this->dbCon, "delete_query", [$id]);
		// $SQLresult = pg_query($this->dbCon, $query);
		if(!$result) {
			$response['error'] = true;
			$response['message'] = "Blackout Day could not be deleted. Please try again.";

			return $response;
		}

		$response = [
			'error' => false,
			'message' => "Blackout Day deleted successfully."
		];

		return json_encode($response);
	}
}

