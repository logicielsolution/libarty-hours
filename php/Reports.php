<?php

if (!isset($_SESSION)) {
    session_start();
}

class Reports {

    private $dbh;
    protected $to;
    protected $from;
    protected $teacherID;
    protected $clubId;

    function __construct() {
        include($_SERVER['DOCUMENT_ROOT'] . '/dbConnection.php');
        include('Teacher.php');
        include('Student.php');

        $this->dbCon = $dbh;
        $this->teacher = new Teacher;
        $this->student = new Student;
    }

    /**
     * Make query for get overall reports lists
     * @param  date  $fromdate
     * @param  date  $todates
     * @param  string $tname  Teacher name
     * @return string $query
     */
    public function makeQuery($fromdate = null, $todate = null, $teacherID = null) {
        $this->to = $todate;
        $this->from = $fromdate;
        $this->teacherID = $teacherID;

        $commonObj = new Common;

        $schoolId = Common::getSchoolId();
        $schoolId = $commonObj->getBuildingCodeById($schoolId);

        $query = "  select
                    count(a.detention) as detention ,
                    count(a.teacher_id) as assigned ,
                    s.grade as grade,
                    a.student_id , s.full_name as student_name
                    from assignments as a , students as s
                    where cast(a.student_id as int) = s.id and a.group_id is null and a.unassigned is not true
        ";

        if ($schoolId) {
            $query .= " and s.building_id = $schoolId";
        }

        if (!empty($this->teacherID)) {
            $query .= " and a.teacher_id = " . $this->teacherID;
        }

        if (!( empty($this->from) && empty($this->to) )) {
            $fromDate = new DateTime($this->from);
            $fromDate = $fromDate->format('Y-m-d');

            $toDate = new DateTime($this->to);
            $toDate = $toDate->format('Y-m-d');

            $query = $query . " and  a.date>='$fromDate' and a.date<='$toDate'";
        } else {
            $toDate = new DateTime();
            $toDate = $toDate->format('Y-m-d');

            $query = $query . " and a.date<='$toDate'";
        }

        $query .= " group by student_name , a.student_id , s.grade order by student_name ";

        return $query;
    }

    public function makeClubReportQuery($fromdate = null, $todate = null, $clubId = null) {
        $this->to = $todate;
        $this->from = $fromdate;
        $this->clubId = $clubId;

        $commonObj = new Common;

        $schoolId = Common::getSchoolId();
        $schoolId = $commonObj->getBuildingCodeById($schoolId);
        

        $query = "  select
                    count(a.detention) as detention ,
                    count(a.teacher_id) as assigned ,
                    s.grade as grade,
                    a.student_id , s.full_name as student_name
                    from assignments as a , students as s
                    where cast(a.student_id as int) = s.id and a.group_id is not null
        ";

        if ($schoolId) {
            $query .= " and s.building_id = $schoolId";
        }

        if (Common::isTeacher()) {
            $teacherId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
            
            $studentIds = $this->getTeachersClubsStudentIds($teacherId);
            $studentIds = implode(',', array_unique($studentIds));
            if (!$studentIds) {
               $studentIds = '0';
            }
            $query .= " and cast(a.student_id as int) in ($studentIds)";
        }

        if (!empty($this->clubId)) {
            $query .= " and a.group_id = " . $this->clubId;
        }

        if (!( empty($this->from) && empty($this->to) )) {
            $fromDate = new DateTime($this->from);
            $fromDate = $fromDate->format('Y-m-d');

            $toDate = new DateTime($this->to);
            $toDate = $toDate->format('Y-m-d');

            $query = $query . " and  a.date>='$fromDate' and a.date<='$toDate'";
        } else {
            $toDate = new DateTime();
            $toDate = $toDate->format('Y-m-d');

            $query = $query . " and a.date<='$toDate'";
        }

        $query .= " group by student_name , a.student_id , s.grade order by student_name ";
        
        return $query;
    }

    /**
     * get assignments list pagination
     * @param  string  $query
     * @param  int  $nopages
     * @param  int $start
     * @return array $assignments
     */
    public function getReportsListPagination($query, $nopages = null, $start = null, $fromDate=null, $toDate=null, $reportType) {

        if (!empty($nopages) && !empty($start)) {
            $query.=" LIMIT $nopages OFFSET $start;";
        }

        $SQLresult = pg_query($this->dbCon, $query);

        $assignments = [];
        if (pg_numrows($SQLresult)) {
            $assignments = pg_fetch_all($SQLresult);
        }

        $studentIds = implode(',', array_column($assignments, 'student_id'));
        if (!$studentIds) {
           $studentIds = '0';
        }

        $assignments = $this->bindStudentAbsentWithRecord($assignments, $studentIds, $fromDate, $toDate, $reportType);
        
        return $assignments;
    }

    /**
     * Bind students total absent with assignment records
     * @param  array  $assignments  assignments list
     * @param  string  $studentIds  student ids
     * @param  date  $fromDate    start date
     * @param  date  $toDate      end date
     * @return array  $assignments
     */
    public function bindStudentAbsentWithRecord($assignments, $studentIds, $fromDate=null, $toDate=null, $reportType)
    {
        if (!$fromDate) {
            $fromDate = isset($_GET['fromDate']) ? $_GET['fromDate'] : null;
            
            if ($fromDate) {
                $fromDateArr = explode('-', $fromDate);
                $fromDate = $fromDateArr[1] . '-' . $fromDateArr[0] . '-' . $fromDateArr[2];
            }
        }
        if (!$toDate) {
            $toDate = isset($_GET['toDate']) ? $_GET['toDate'] : null;

            if ($toDate) {
                $toDateArr = explode('-', $toDate);
                $toDate = $toDateArr[1] . '-' . $toDateArr[0] . '-' . $toDateArr[2];
            }
        }

        $countQuery = "select student_id, count(absent) from assignments where cast(student_id as int) in ($studentIds) and absent=true";

        if ($fromDate && $toDate) {
            $fromDate = new DateTime($fromDate);
            $fromDate = $fromDate->format('Y-m-d');

            $toDate = new DateTime($toDate);
            $toDate = $toDate->format('Y-m-d');

            $countQuery .= " and date>='$fromDate' and date<='$toDate'";
        }

        if ($reportType == Common::REPORT_TYPE_ASSIGNMENT) {
            $countQuery .= " and group_id is null";   
        } else {
            $countQuery .= " and group_id is not null";   
        }

        $countQuery .= " group by student_id";

        $SQLresult = pg_query($this->dbCon, $countQuery);

        $absentCount = [];
        if(pg_numrows($SQLresult)) {
            $absentCount = pg_fetch_all($SQLresult);
        }

        foreach ($assignments as $key => $assignment) {
            $assignments[$key]['absent'] = '0';
            foreach ($absentCount as $ckey => $count) {
                if ($assignment['student_id'] == $count['student_id']) {
                    $assignments[$key]['absent'] = $absentCount[$ckey]['count'];
                }
            }
        }

        return $assignments;
    }

    public function getTeachersClubsStudentIds($teacherId)
    {
        $sIdsQuery = "SELECT stg.student_id from student_to_group as stg inner join admin_to_group as atg on atg.group_id=stg.group_id  where atg.admin_id=$1";
        $result = pg_prepare($this->dbCon, "get_student_ids_query", $sIdsQuery);
        $result = pg_execute($this->dbCon, "get_student_ids_query", [$teacherId]);

        $studentIds = pg_fetch_all($result);

        if (!$studentIds) {
            return [];
        }
        $studentIds = array_column($studentIds, 'student_id');

        return $studentIds;
    }

    /**
     * Get Grades data for show in assignments export file
     * @param  array  $reports  Exports report data
     * @return array  $gradeData  return grades data
     */
    public function getGradesData($reports)
    {
        $totalGrades = 12;

        $gradeData = [];
        for($i = $totalGrades; $i >= 1; $i--){         

            $array = [];
            $array['grade'] = $i;
            $array['assigned'] = 0;
            $array['absent'] = 0;
            $array['detention'] = 0;

            $gradeData[] = $array;
            
        }

        foreach ($gradeData as $key => $gd) {
            foreach ($reports as $rkey => $rp) {
                if ($gd['grade'] == $rp['grade']) {
                    $gradeData[$key]['assigned']    = $gradeData[$key]['assigned'] + $rp['assigned'];
                    $gradeData[$key]['absent']      = $gradeData[$key]['absent'] + $rp['absent'];
                    $gradeData[$key]['detention']   = $gradeData[$key]['detention'] + $rp['detention'];
                }
            }
        }

        $total = [
            'grade' => 'Total',
            'assigned' => 0,
            'absent' => 0,
            'detention' => 0
        ];
        foreach ($gradeData as $key => $gd) {
            $total['assigned']  = $total['assigned'] + $gd['assigned'];
            $total['absent']    = $total['absent'] + $gd['absent'];
            $total['detention'] = $total['detention'] + $gd['detention'];
        }

        $gradeData[] = $total; // add total

        return $gradeData;
    }

}
