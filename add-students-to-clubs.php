<?php
session_start();

include("header.php");

?>
<?php function entry_html($title, $dbh) {
?>

<div class="add-student-to-club-class">
    
    <?php include('php/clubs/club-header.php'); ?>

    <div class="row" style="margin-top: 40px;">
        <div class="col-sm-2">
            <label for="search-student">Club Name:</label>
        </div>
        <div class="col-sm-10">
            <span class="club-title" style="font-size: 20px;">
                <b>
                    <?php echo $_GET['group_name']; ?>
                </b>
            </span>
        </div>
    </div>
    <?php if(!isset($_GET['action']) || ($_GET['action'] != "view")): ?>
        <!-- Search teacher -->
        <div style="margin-top: 10px;">
            <div class="row">
                <div class="col-sm-2">
                    <label for="search-student">Add Student:</label>
                </div>
                <div class="col-sm-10">
                   <input type="hidden" id="selctedGrpId" name="group_id" value="<?php echo $_GET['group_id']; ?>">
                   <input type="hidden" id="selctedGrpSchoolId" name="school_id" value="<?php echo $_GET['school_id']; ?>">
                   <input type="text" 
                        id="search-student" 
                        name="search-student" 
                        size="40">
                    <br>
                    <span id="not-found-message" style="color: red;"></span>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!--Start Table -->
     <div style="margin-top: 25px;">
        <table class="table table-bordered" style="margin-top: 20px;">
            <colgroup>
                <col style="width: 10%;" />
                <col style="width: 80%;" />
                <col style="width: 10%;" />
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Student Name</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $groupId = isset($_GET['group_id']) ? $_GET['group_id'] : null;

                    $query="SELECT s.id, s.full_name, s.username, sg.id as student_to_group_id 
                            FROM students as s 
                            INNER JOIN student_to_group as sg 
                            ON s.id = cast(sg.student_id as int)
                            WHERE sg.group_id='$groupId'";
                    $result = pg_query($dbh, $query);
                    if (!$result) {
                        echo "An error occurred.\n";
                        exit;
                    }

                    $students = pg_fetch_all($result);
                    
                    if ($students) {
                        foreach ($students as $key => $student) {
                            
                ?>
                    <tr>
                        <td class="assigned-student-id"><?php echo $student['id']; ?></td>
                        <td><?php echo $student['full_name']; ?></td>
                        <td class="text-center">
                            <a class="delete-added-student" 
                                data-id="<?php echo $student['student_to_group_id']; ?>"
                                data-student-id="<?php echo $student['id']; ?>"
                                data-group-id="<?php echo $_GET['group_id']; ?>"
                                style="cursor: pointer;">
                              <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <?php 
                        }
                    } else {
                        echo "<tr><td colspan='3' class='text-center'>No Record Found</td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
    <!--End Table -->

</div>

<?php
}
?>


<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Club Students";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    
    include("footer.php");
?>