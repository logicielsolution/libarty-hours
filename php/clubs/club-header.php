<h2>Club Manager</h2>  

<hr>

<?php if (Common::isSuperAdmin() || Common::isAdmin()) { ?>
    <a class="btn btn-primary" href="/clubs.php?createClub=yes" 
        role="button">
        Create Club
    </a>
<?php } ?>
<?php if (!Common::isStudent()) { ?>
    <a class="btn btn-primary" 
        href="/clubs.php?viewManagedClubs=yes" 
        role="button" 
        style="margin-right: 5px;">
        Manage Clubs
    </a>
    <a class="btn btn-primary" 
        href="/clubs-request.php" 
        role="button" 
        style="margin-right: 5px;">
        Clubs Request 
    </a>
<?php } ?>
<?php if (Common::isStudent()) { ?>
    <a class="btn btn-primary" 
        href="/clubs.php?viewYourClubs=yes" 
        role="button">
        View your Clubs
    </a> 
<?php } ?>

<a class="btn btn-primary" href="clubs.php" role="button">View All Clubs</a>
