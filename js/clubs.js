$( function() {
    var clubObj = clubs();
    clubObj.bindObjects();
});

function clubs(){
	return new function(){
		this.$searchTeacherInput      = $( "#search-teacher" ),
		this.$deleteAssinedAdminBtn   = $( ".delete-added-teacher" ),
		this.$searchStudentInput      = $( "#search-student" ),
		this.$deleteAddedStudentBtn   = $( ".delete-added-student" ),
		this.$deleteClubBtn           = $( ".delete-club-btn" ),
        this.createClubForm           = $( "#createClubForm" ),    
		this.bindObjects = function(){
			var obj = this;

            //multiple select admins
            obj.selectAdmins();

			//search teachers
			obj.searchTeachers();

			//search students
            if ($('.add-student-to-club-class').length > 0) {
			 obj.searchStudents();
            }

			// delete assigned admin
			obj.$deleteAssinedAdminBtn.click(function(){
				var id = $(this).data('id');
				if (confirm("Do you want to delete?")) {
			  		obj.deleteAssignedAdmin(id);
                }
			});

			// delete assigned student
			obj.$deleteAddedStudentBtn.click(function(){
                var id = $(this).data('id');
                var studentId = $(this).data('studentId');
				var groupId = $(this).data('groupId');
				if (confirm("Do you want to delete?")) {
			  		obj.deleteAddedStudent(id, groupId, studentId);
                }
			});

			// delete club
			obj.$deleteClubBtn.click(function(){
				var id = $(this).data('id');
				if (confirm("Do you want to delete?")) {
			  		obj.deleteClub(id);
                }
			});

            //select multiple dates from datepicer
            obj.selectDatesForClub();
            
            //create club
            obj.createClub();

            //Ask to join button for club
            $('.ask-to-join-btn').click(function(){
                var url = "php/clubs/askToJoin.php";
                var groupid = $(this).data('groupId');
                var groupName = $(this).data('groupName');
                
                obj.askToJoin(url, groupid, groupName);
            });

            //ask to remove button for club
            $('.ask-to-remove-btn').click(function(){
                var url = "php/clubs/askToRemove.php";
                var groupid = $(this).data('groupId');
                var groupName = $(this).data('groupName');
                if (confirm("Do you really want to remove this club?")) {
                    obj.askToRemove(url, groupid, groupName);
                }
            });
			
            //allow studnent to join club
            $('.allow-request-btn, .deny-request-btn').click(function(){
                var groupid = $(this).data('groupId');
                var groupName = $(this).data('groupName');
                var studentId = $(this).data('studentId');
                var confirmationtype = $(this).data('confirmationType');

                var msg = "Do you really want to accept the request?";
                if (confirmationtype == 'deny') {
                    msg = "Do you really want to deny the request?";
                }
                if (confirm(msg)) {
                    obj.allowDenyRequest(groupid, groupName, studentId, confirmationtype);
                }
            });
            

		},
        this.selectAdmins = function(){

            var selectedBuilding = $('#clubBuilding option:selected').val();
            $("#clubBuilding").change(function(){
                selectedBuilding = $('#clubBuilding option:selected').val();
            });

            // if on club edit screen then get school id
            if ($('#schoolId').length > 0) {
                selectedBuilding = $('#schoolId').val();
            }

            //if not on add teacher page 
            if ($("#select-club-admins").length < 1) {
                return;
            }
            
            $("#select-club-admins").select2({
                ajax: {
                    url: 'php/getTeachers.php',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            name: params.term, // search term
                            building: selectedBuilding
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.full_name,
                                    id: item.id
                                }
                            })
                        };
                        /*return {
                            results: data
                        };*/
                    },
                    cache: true                    
                },
                minimumInputLength: 2,
            });

        }
		this.searchTeachers = function(){
			var obj = this;

			var alreadyAddedAdminsid = [];
			$('td.assigned-admin-id').each(function(){
                 alreadyAddedAdminsid.push($(this).html());
            });

			//if not on add teacher page 
			if (obj.$searchTeacherInput.length < 1) {
				return;
			}
			
			obj.$searchTeacherInput.autocomplete({
				source: function( request, response ) {
                    $.ajax({
                        url: "php/getTeachers.php",
                        dataType: "json",
                        data: {
                            name: request.term
                        },
                        success: function( data ) {
                        	var record = [];
                        	for (var i = 0; i < data.length; i++) {
                        		if ($.inArray(data[i].id, alreadyAddedAdminsid) == -1) {
                        			// data.splice(i, 1);
                        			record.push(data[i]);
                        		}
                        	}

                        	if (record.length == 0) {
				                $("#not-found-message").text("No results found");
				            } else {
				                $("#not-found-message").empty();
				            }
                        	
                            response( record );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) {
                    console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );

                    setTimeout(function() {
                        obj.$searchTeacherInput.val(ui.item.full_name);
                        obj.addAdminToClub(ui.item.id);
                    }, 1);

                },

                focus: function( event, ui) {
                    obj.$searchTeacherInput.val(ui.item.full_name);
                    event.preventDefault();
                }
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                ul.css('z-index', 1100);
                return $( "<li>" ).data("item.autocomplete", item)
                .append( item.full_name )
                .appendTo( ul );
            };
		},
		this.addAdminToClub = function(teacherId) {

			var gropuId = $('#selctedGrpId').val();

			var $promise = $.ajax({
                url: 'php/clubs/addAdminToClub.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    teacher_id: teacherId,
                    group_id: gropuId
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
		},
		this.deleteAssignedAdmin = function(id) {
			var $promise = $.ajax({
                url: 'php/clubs/deleteAssignedAdminToClub.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
		},
		this.searchStudents = function(){
			var obj = this;

			var alreadyAddedStudentsid = [];
			$('td.assigned-student-id').each(function(){
                 alreadyAddedStudentsid.push($(this).html());
            });

            var gropuId = $('#selctedGrpId').val();
            var schoolId = $('#selctedGrpSchoolId').val();

			//if not on add teacher page 
			if (obj.$searchStudentInput.length < 1) {
				return;
			}
			
			obj.$searchStudentInput.autocomplete({
				source: function( request, response ) {
                    $.ajax({
                        url: "php/getStudents.php",
                        dataType: "json",
                        data: {
                            name: request.term,
                            /*search_for : 'club',*/
                            group_id : gropuId,
                            school_id: schoolId

                        },
                        success: function( data ) {
                        	var record = [];
                        	for (var i = 0; i < data.length; i++) {
                        		if ($.inArray(data[i].id, alreadyAddedStudentsid) == -1) {
                        			// data.splice(i, 1);
                        			record.push(data[i]);
                        		}
                        	}

                        	if (record.length == 0) {
				                $("#not-found-message").text("No results found");
				            } else {
				                $("#not-found-message").empty();
				            }
                        	
                            response( record );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) {
                    console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );

                    setTimeout(function() {
                        obj.$searchStudentInput.val(ui.item.full_name);
                        obj.addStudentToClub(ui.item.id);
                    }, 1);

                },

                focus: function( event, ui) {
                    obj.$searchStudentInput.val(ui.item.full_name);
                    event.preventDefault();
                }
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                ul.css('z-index', 1100);
                return $( "<li>" ).data("item.autocomplete", item)
                .append( item.full_name )
                .appendTo( ul );
            };
		},
		this.addStudentToClub = function(studentId){
			var gropuId = $('#selctedGrpId').val();

			var $promise = $.ajax({
                url: 'php/clubs/addStudentToClub.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    student_id: studentId,
                    group_id: gropuId
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
		},
		this.deleteAddedStudent = function(id, groupId, studentId) {
			var $promise = $.ajax({
                url: 'php/clubs/deleteAddedStudent.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id,
                    group_id: groupId,
                    student_id: studentId
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
		},
		this.deleteClub = function(id) {
			var $promise = $.ajax({
                url: 'php/clubs/deleteClub.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {
                	location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
		},
        this.selectDatesForClub = function() {

            //if not on add teacher page 
            if ($('#club-dates').length < 1) {
                return;
            }

            $('#club-dates, .input-group.date').datepicker({
                format: "dd/mm/yyyy",
                multidate: true,
                daysOfWeekDisabled: "0,6",
                todayHighlight: true
            });
            // $('#club-dates, .input-group.date').datepicker('setDate', [new Date(2017, 3, 20)]);
        },
        this.createClub = function(){
            var obj = this;

            obj.createClubForm.off('submit');
            obj.createClubForm.on('submit', function() {
                var $this = $(this);
                var url = $this.attr('action');
                var params = $this.serialize();
           
                var promise = $.ajax({
                    url: url,
                    dataType: "json",
                    type: 'POST',
                    data: params,
                    beforeSend: function() {
                        showLoader();
                    }
                });

                promise.done(function(response, textStatus, jqXHR) {
                    
                    if(response.error == false) {
                         window.location.replace("/clubs.php");
                    } else {
                        alert(response.message);
                    }
                }).always(function() {
                    hideLoader();
                });

                return false;   
            });
        },
        this.askToJoin = function(url, groupId, groupName) {
            var $promise = $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: {
                    group_id: groupId,
                    group_name: groupName
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                console.log(response);
                if(response.error == true) {
                    alert(response.message);
                } else {
                    alert(response.message);
                    location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
        },
        this.askToRemove = function(url, groupId, groupName) {
            var $promise = $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: {
                    group_id: groupId,
                    group_name: groupName
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                console.log(response);
                if(response.error == true) {
                    alert(response.message);
                } else {
                    location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
        },
        this.allowDenyRequest = function(groupId, groupName, studentId, confirmationtype) {
            var url = "php/clubs/allowDenyRequest.php";

            var $promise = $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: {
                    group_id: groupId,
                    group_name: groupName,
                    student_id: studentId,
                    confirmation_type: confirmationtype
                },
                beforeSend: function() {
                    showLoader();
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                console.log(response);
                if(response.error == true) {
                    alert(response.message);
                } else {
                    location.reload();
                }
            }).always(function() {
                hideLoader();
            });

            return false;
        }
	};
}