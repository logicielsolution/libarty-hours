<!-- Modal -->
<div class="modal fade" id="selectTeacherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="teacherSelectPopForm" 
				class="form-horizontal"
				action="/php/getStudentAssignments.php">
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h3 class="modal-title" id="myModalLabel">Add an Assignment</h3>
				</div>
				<div class="modal-body">
					<!-- Student Name -->
					<div class="form-group">
				      	<label class="control-label col-sm-3" for="student-name">Student Name:</label>
					    <div class="col-sm-9">
				      		<label class="control-label" id="student-name"></label>
					    </div>
				    </div>

				    <!-- Date -->
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="selected-date">Date:</label>
					    <div class="col-sm-9">
				      		<label class="control-label" id="selected-date"></label>
					    </div>
				    </div>

				    <!-- Teacher Name -->
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="search-teacher">Teacher Name:</label>
					    <div class="col-sm-6">
					    	<?php $teacherName = isset($_SESSION['full_user_name']) ? $_SESSION['full_user_name'] : ''; ?>
					      	<?php if (Common::isTeacher()) { ?>
					      		<label class="control-label" for="teacher-name">
					      			<?php echo ucfirst($teacherName); ?>
					      		</label>
					      	<?php } else { ?>
								<input type="text" 
									class="form-control" 
							        id="search-teacher" 
							        name="search-teacher">
						    <?php } ?>
					    </div>
				    </div>

				    <!-- Note -->
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="assignment-note">Note:</label>
					    <div class="col-sm-6">
							<textarea class="form-control"
								name="note" 
								id="assignment-note" 
								rows="3"></textarea>
					    </div>
				    </div>
					
					<!-- Assignment type -->
					<div class="form-group">
						<div class="col-sm-3"></div>
						<div class="radio col-sm-9" style="padding-top: 0px;">
						  	<label class="radio-inline">
						  		<input type="radio"
						  			class="assignment-type"
						  			name="assignment-type"
						  			value="<?php echo Common::FULL_DAY_ASSIGNMENT; ?>"
						  			checked>Full Day
						  	</label>
							<label class="radio-inline">
								<input type="radio"
									class="assignment-type" 
									name="assignment-type"
									value="<?php echo Common::FIRST_HALF_ASSIGNMENT; ?>">First Half
							</label>
							<label class="radio-inline">
								<input type="radio"
									class="assignment-type" 
									name="assignment-type"
									value="<?php echo Common::SECOND_HALF_ASSIGNMENT; ?>">Second Half
							</label> 
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" disabled="disabled">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>