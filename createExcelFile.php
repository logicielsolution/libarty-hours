<?php

include("php/Constants.php");
include("php/Authentication.php");
include("php/Reports.php");
include("php/clubs/Club.php");
include("dbConnection.php");

// check page permission
Authentication::hasPermission(Common::PAGE_ASSIGNMENT_REPORTLIST);


$objReports = new Reports();
$clubObj = new Club($dbh);
$query = $_POST['query'];
$fromDate = isset($_POST['fromDate']) ? $_POST['fromDate'] : null;
$toDate = isset($_POST['toDate']) ? $_POST['toDate'] : null;
$reportType = isset($_POST['reportType']) ? $_POST['reportType'] : null;
$teacherName = isset($_POST['teacherName']) ? $_POST['teacherName'] : null;

$reports = $objReports->getReportsListPagination($query, null, null, $fromDate, $toDate, $reportType);
$gradesData = $objReports->getGradesData($reports);

$assignmentFilename = "Assignment-Report.xls";
if ($teacherName) {
	$assignmentFilename = str_replace(' ', '-', $teacherName) . "-" . $assignmentFilename;
}

//create content in excel file
if (count($reports) > 0) {

	if ($reportType == Common::REPORT_TYPE_ASSIGNMENT) {
		header("Content-disposition: attachment; filename=" . $assignmentFilename);
	    echo "Student Name \t Grade \t Assigned  \t Absent  \t Detention \n";
	    foreach ((array) $reports as $key => $report) {
	        echo $report['student_name'] . "\t" .
	        $report['grade'] . "\t" .
	        $report['assigned'] . "\t" .
	        $report['absent'] . "\t" .
	        $report['detention'] . "\n";
	    }

	    // create grades table
	    echo "\n\n Grade \t Assigned  \t Absent  \t Detention \n";
	    foreach ($gradesData as $key => $gData) {
	        echo $gData['grade'] . "\t" .
	        $gData['assigned'] . "\t" .
	        $gData['absent'] . "\t" .
	        $gData['detention'] . "\n";
	    }

	} else {
		header("Content-disposition: attachment; filename=Club-Report.xls");
	    echo "Student Name \t Clubs \t Grade \t Assigned  \t Absent  \t Detention \n";
	    foreach ((array) $reports as $key => $report) {
        	$clubNames = $clubObj->getStudentClubsList($report['student_id']);
			$clubNames = implode(' | ', $clubNames);                     

	        echo $report['student_name'] . "\t" .
	        $clubNames . "\t" .
	        $report['grade'] . "\t" .
	        $report['assigned'] . "\t" .
	        $report['absent'] . "\t" .
	        $report['detention'] . "\n";
	    }
	}
}
?>