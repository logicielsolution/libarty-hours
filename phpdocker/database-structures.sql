--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: postgres
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: dblink_pkey_results; Type: TYPE; Schema: public; Owner: jcarter
--

CREATE TYPE dblink_pkey_results AS (
	"position" integer,
	colname text
);


ALTER TYPE public.dblink_pkey_results OWNER TO jcarter;

--
-- Name: dblink(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink(text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text) OWNER TO jlevora;

--
-- Name: dblink(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink(text, text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, text) OWNER TO jlevora;

--
-- Name: dblink(text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink(text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, boolean) OWNER TO jlevora;

--
-- Name: dblink(text, text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink(text, text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, text, boolean) OWNER TO jlevora;

--
-- Name: dblink_build_sql_delete(text, int2vector, integer, text[]); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_build_sql_delete(text, int2vector, integer, text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_delete';


ALTER FUNCTION public.dblink_build_sql_delete(text, int2vector, integer, text[]) OWNER TO jlevora;

--
-- Name: dblink_build_sql_insert(text, int2vector, integer, text[], text[]); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_build_sql_insert(text, int2vector, integer, text[], text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_insert';


ALTER FUNCTION public.dblink_build_sql_insert(text, int2vector, integer, text[], text[]) OWNER TO jlevora;

--
-- Name: dblink_build_sql_update(text, int2vector, integer, text[], text[]); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_build_sql_update(text, int2vector, integer, text[], text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_update';


ALTER FUNCTION public.dblink_build_sql_update(text, int2vector, integer, text[], text[]) OWNER TO jlevora;

--
-- Name: dblink_cancel_query(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_cancel_query(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_cancel_query';


ALTER FUNCTION public.dblink_cancel_query(text) OWNER TO jlevora;

--
-- Name: dblink_close(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_close(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text) OWNER TO jlevora;

--
-- Name: dblink_close(text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_close(text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, boolean) OWNER TO jlevora;

--
-- Name: dblink_close(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_close(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, text) OWNER TO jlevora;

--
-- Name: dblink_close(text, text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_close(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, text, boolean) OWNER TO jlevora;

--
-- Name: dblink_connect(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_connect(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect(text) OWNER TO jlevora;

--
-- Name: dblink_connect(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_connect(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect(text, text) OWNER TO jlevora;

--
-- Name: dblink_connect_u(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_connect_u(text) RETURNS text
    LANGUAGE c STRICT SECURITY DEFINER
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect_u(text) OWNER TO jlevora;

--
-- Name: dblink_connect_u(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_connect_u(text, text) RETURNS text
    LANGUAGE c STRICT SECURITY DEFINER
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect_u(text, text) OWNER TO jlevora;

--
-- Name: dblink_current_query(); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_current_query() RETURNS text
    LANGUAGE c
    AS '$libdir/dblink', 'dblink_current_query';


ALTER FUNCTION public.dblink_current_query() OWNER TO jlevora;

--
-- Name: dblink_disconnect(); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_disconnect() RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_disconnect';


ALTER FUNCTION public.dblink_disconnect() OWNER TO jlevora;

--
-- Name: dblink_disconnect(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_disconnect(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_disconnect';


ALTER FUNCTION public.dblink_disconnect(text) OWNER TO jlevora;

--
-- Name: dblink_error_message(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_error_message(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_error_message';


ALTER FUNCTION public.dblink_error_message(text) OWNER TO jlevora;

--
-- Name: dblink_exec(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_exec(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text) OWNER TO jlevora;

--
-- Name: dblink_exec(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_exec(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, text) OWNER TO jlevora;

--
-- Name: dblink_exec(text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_exec(text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, boolean) OWNER TO jlevora;

--
-- Name: dblink_exec(text, text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_exec(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, text, boolean) OWNER TO jlevora;

--
-- Name: dblink_fetch(text, integer); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_fetch(text, integer) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, integer) OWNER TO jlevora;

--
-- Name: dblink_fetch(text, integer, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_fetch(text, integer, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, integer, boolean) OWNER TO jlevora;

--
-- Name: dblink_fetch(text, text, integer); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_fetch(text, text, integer) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, text, integer) OWNER TO jlevora;

--
-- Name: dblink_fetch(text, text, integer, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_fetch(text, text, integer, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, text, integer, boolean) OWNER TO jlevora;

--
-- Name: dblink_get_connections(); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_connections() RETURNS text[]
    LANGUAGE c
    AS '$libdir/dblink', 'dblink_get_connections';


ALTER FUNCTION public.dblink_get_connections() OWNER TO jlevora;

--
-- Name: dblink_get_notify(); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_notify(OUT notify_name text, OUT be_pid integer, OUT extra text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_notify';


ALTER FUNCTION public.dblink_get_notify(OUT notify_name text, OUT be_pid integer, OUT extra text) OWNER TO jlevora;

--
-- Name: dblink_get_notify(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_notify(conname text, OUT notify_name text, OUT be_pid integer, OUT extra text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_notify';


ALTER FUNCTION public.dblink_get_notify(conname text, OUT notify_name text, OUT be_pid integer, OUT extra text) OWNER TO jlevora;

--
-- Name: dblink_get_pkey(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_pkey(text) RETURNS SETOF dblink_pkey_results
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_pkey';


ALTER FUNCTION public.dblink_get_pkey(text) OWNER TO jlevora;

--
-- Name: dblink_get_result(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_result(text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_result';


ALTER FUNCTION public.dblink_get_result(text) OWNER TO jlevora;

--
-- Name: dblink_get_result(text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_get_result(text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_result';


ALTER FUNCTION public.dblink_get_result(text, boolean) OWNER TO jlevora;

--
-- Name: dblink_is_busy(text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_is_busy(text) RETURNS integer
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_is_busy';


ALTER FUNCTION public.dblink_is_busy(text) OWNER TO jlevora;

--
-- Name: dblink_open(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_open(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text) OWNER TO jlevora;

--
-- Name: dblink_open(text, text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_open(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, boolean) OWNER TO jlevora;

--
-- Name: dblink_open(text, text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_open(text, text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, text) OWNER TO jlevora;

--
-- Name: dblink_open(text, text, text, boolean); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_open(text, text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, text, boolean) OWNER TO jlevora;

--
-- Name: dblink_send_query(text, text); Type: FUNCTION; Schema: public; Owner: jlevora
--

CREATE FUNCTION dblink_send_query(text, text) RETURNS integer
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_send_query';


ALTER FUNCTION public.dblink_send_query(text, text) OWNER TO jlevora;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_to_group; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE admin_to_group (
    id integer DEFAULT nextval(('"admin_to_group_seq"'::text)::regclass) NOT NULL,
    admin_id integer,
    group_id integer
);


ALTER TABLE public.admin_to_group OWNER TO jlevora;

--
-- Name: admin_to_group_seq; Type: SEQUENCE; Schema: public; Owner: cbier
--

CREATE SEQUENCE admin_to_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_to_group_seq OWNER TO cbier;

--
-- Name: allstudents; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE allstudents (
    id integer,
    username text,
    first text,
    last text,
    middle text,
    building_id integer,
    home_room text,
    home_room_teacher text,
    full_name text,
    last_first text,
    sasiid text,
    grade text,
    active boolean
);


ALTER TABLE public.allstudents OWNER TO jlevora;

--
-- Name: allstudents_prod; Type: VIEW; Schema: public; Owner: jlevora
--


--
-- Name: allusers; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE allusers (
    id integer,
    username text,
    first text,
    last text,
    full_name text,
    password text,
    student boolean,
    gradyear text,
    unique_id text,
    building_id integer
);


ALTER TABLE public.allusers OWNER TO jlevora;

--
-- Name: schools_prod; Type: VIEW; Schema: public; Owner: jlevora
--


--
-- Name: users_prod; Type: VIEW; Schema: public; Owner: jlevora
--


--
-- Name: allusers_prod; Type: VIEW; Schema: public; Owner: jlevora
--

--
-- Name: assignments; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE assignments (
    id integer DEFAULT nextval(('"assignments_id_seq"'::text)::regclass) NOT NULL,
    student_id text,
    teacher_id integer,
    group_id integer,
    absent boolean,
    date date,
    assigner_id integer,
    note text,
    created date,
    detention boolean,
    unassigned boolean
);


ALTER TABLE public.assignments OWNER TO jlevora;

--
-- Name: assignments_id_seq; Type: SEQUENCE; Schema: public; Owner: jlevora
--

CREATE SEQUENCE assignments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assignments_id_seq OWNER TO jlevora;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE groups (
    id integer DEFAULT nextval(('"groups_id_seq"'::text)::regclass) NOT NULL,
    name text,
    description text,
    admin_id integer,
    building integer,
    created date
);


ALTER TABLE public.groups OWNER TO jlevora;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: jlevora
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO jlevora;

--
-- Name: jobs; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE jobs (
    id integer,
    user_id integer,
    job_title_id integer
);


ALTER TABLE public.jobs OWNER TO jlevora;

--
-- Name: jobs_prod; Type: VIEW; Schema: public; Owner: jlevora
--

--
-- Name: schools; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE schools (
    id integer,
    school_name text,
    sasi_building_code integer
);


ALTER TABLE public.schools OWNER TO jlevora;

--
-- Name: student_to_group; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE student_to_group (
    id integer DEFAULT nextval(('"studentgroup_id_seq"'::text)::regclass) NOT NULL,
    student_id text,
    group_id integer
);


ALTER TABLE public.student_to_group OWNER TO jlevora;

--
-- Name: studentgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: jlevora
--

CREATE SEQUENCE studentgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studentgroup_id_seq OWNER TO jlevora;

--
-- Name: students; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE students (
    id integer,
    username text,
    first text,
    last text,
    middle text,
    building_id integer,
    home_room text,
    home_room_teacher text,
    full_name text,
    last_first text,
    sasiid text,
    password text,
    passwordhash text,
    grade text,
    telephone text
);


ALTER TABLE public.students OWNER TO jlevora;

--
-- Name: students_prod; Type: VIEW; Schema: public; Owner: jlevora
--

--
-- Name: teachers; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE teachers (
    id integer,
    username text,
    last text,
    first text,
    full_name text,
    last_first text,
    building_id integer,
    building_name text,
    job text,
    home_room text
);


ALTER TABLE public.teachers OWNER TO jlevora;

--
-- Name: teachers_prod; Type: VIEW; Schema: public; Owner: jlevora
--

-- Name: users; Type: TABLE; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE TABLE users (
    id integer,
    username text,
    first text,
    last text,
    full_name text,
    building_id integer,
    password text,
    student boolean,
    gradyear text,
    passwordhash text,
    school_name text,
    unique_id text
);


ALTER TABLE public.users OWNER TO jlevora;

--
-- Name: assignmentsid; Type: CONSTRAINT; Schema: public; Owner: jlevora; Tablespace: 
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignmentsid PRIMARY KEY (id);


--
-- Name: groupsid; Type: CONSTRAINT; Schema: public; Owner: jlevora; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groupsid PRIMARY KEY (id);


--
-- Name: studentgroupid; Type: CONSTRAINT; Schema: public; Owner: jlevora; Tablespace: 
--

ALTER TABLE ONLY student_to_group
    ADD CONSTRAINT studentgroupid PRIMARY KEY (id);


--
-- Name: group_id; Type: INDEX; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE INDEX group_id ON student_to_group USING btree (group_id);


--
-- Name: student_id; Type: INDEX; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE INDEX student_id ON assignments USING btree (student_id);


--
-- Name: studentid; Type: INDEX; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE INDEX studentid ON student_to_group USING btree (student_id);


--
-- Name: teacher_id; Type: INDEX; Schema: public; Owner: jlevora; Tablespace: 
--

CREATE INDEX teacher_id ON assignments USING btree (teacher_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: dblink_connect_u(text); Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON FUNCTION dblink_connect_u(text) FROM PUBLIC;
REVOKE ALL ON FUNCTION dblink_connect_u(text) FROM jlevora;
GRANT ALL ON FUNCTION dblink_connect_u(text) TO jlevora;


--
-- Name: dblink_connect_u(text, text); Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON FUNCTION dblink_connect_u(text, text) FROM PUBLIC;
REVOKE ALL ON FUNCTION dblink_connect_u(text, text) FROM jlevora;
GRANT ALL ON FUNCTION dblink_connect_u(text, text) TO jlevora;


--
-- Name: admin_to_group; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE admin_to_group FROM PUBLIC;
REVOKE ALL ON TABLE admin_to_group FROM jlevora;
GRANT ALL ON TABLE admin_to_group TO jlevora;
GRANT ALL ON TABLE admin_to_group TO rthaler;
GRANT ALL ON TABLE admin_to_group TO libertyeaglehour;
GRANT ALL ON TABLE admin_to_group TO contractor;


--
-- Name: admin_to_group_seq; Type: ACL; Schema: public; Owner: cbier
--

REVOKE ALL ON SEQUENCE admin_to_group_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE admin_to_group_seq FROM cbier;
GRANT ALL ON SEQUENCE admin_to_group_seq TO cbier;
GRANT SELECT,UPDATE ON SEQUENCE admin_to_group_seq TO libertyeaglehour;


--
-- Name: allstudents; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE allstudents FROM PUBLIC;
REVOKE ALL ON TABLE allstudents FROM jlevora;
GRANT ALL ON TABLE allstudents TO jlevora;
GRANT ALL ON TABLE allstudents TO rthaler;
GRANT ALL ON TABLE allstudents TO contractor;


--
-- Name: allstudents_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE allstudents_prod FROM PUBLIC;
REVOKE ALL ON TABLE allstudents_prod FROM jlevora;
GRANT ALL ON TABLE allstudents_prod TO jlevora;
GRANT ALL ON TABLE allstudents_prod TO rthaler;
GRANT ALL ON TABLE allstudents_prod TO libertyeaglehour;


--
-- Name: allusers; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE allusers FROM PUBLIC;
REVOKE ALL ON TABLE allusers FROM jlevora;
GRANT ALL ON TABLE allusers TO jlevora;
GRANT ALL ON TABLE allusers TO rthaler;
GRANT ALL ON TABLE allusers TO contractor;


--
-- Name: schools_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE schools_prod FROM PUBLIC;
REVOKE ALL ON TABLE schools_prod FROM jlevora;
GRANT ALL ON TABLE schools_prod TO jlevora;
GRANT ALL ON TABLE schools_prod TO rthaler;
GRANT SELECT ON TABLE schools_prod TO libertyeaglehour;


--
-- Name: users_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE users_prod FROM PUBLIC;
REVOKE ALL ON TABLE users_prod FROM jlevora;
GRANT ALL ON TABLE users_prod TO jlevora;
GRANT ALL ON TABLE users_prod TO rthaler;
GRANT SELECT ON TABLE users_prod TO libertyeaglehour;


--
-- Name: allusers_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE allusers_prod FROM PUBLIC;
REVOKE ALL ON TABLE allusers_prod FROM jlevora;
GRANT ALL ON TABLE allusers_prod TO jlevora;
GRANT ALL ON TABLE allusers_prod TO rthaler;
GRANT SELECT ON TABLE allusers_prod TO libertyeaglehour;


--
-- Name: assignments; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE assignments FROM PUBLIC;
REVOKE ALL ON TABLE assignments FROM jlevora;
GRANT ALL ON TABLE assignments TO jlevora;
GRANT ALL ON TABLE assignments TO rthaler;
GRANT ALL ON TABLE assignments TO libertyeaglehour;
GRANT ALL ON TABLE assignments TO contractor;


--
-- Name: assignments_id_seq; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON SEQUENCE assignments_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE assignments_id_seq FROM jlevora;
GRANT ALL ON SEQUENCE assignments_id_seq TO jlevora;
GRANT ALL ON SEQUENCE assignments_id_seq TO rthaler;
GRANT ALL ON SEQUENCE assignments_id_seq TO libertyeaglehour;


--
-- Name: groups; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE groups FROM PUBLIC;
REVOKE ALL ON TABLE groups FROM jlevora;
GRANT ALL ON TABLE groups TO jlevora;
GRANT ALL ON TABLE groups TO rthaler;
GRANT ALL ON TABLE groups TO libertyeaglehour;
GRANT ALL ON TABLE groups TO contractor;


--
-- Name: groups_id_seq; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON SEQUENCE groups_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE groups_id_seq FROM jlevora;
GRANT ALL ON SEQUENCE groups_id_seq TO jlevora;
GRANT ALL ON SEQUENCE groups_id_seq TO rthaler;
GRANT ALL ON SEQUENCE groups_id_seq TO libertyeaglehour;


--
-- Name: jobs; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE jobs FROM PUBLIC;
REVOKE ALL ON TABLE jobs FROM jlevora;
GRANT ALL ON TABLE jobs TO jlevora;
GRANT ALL ON TABLE jobs TO rthaler;
GRANT ALL ON TABLE jobs TO contractor;


--
-- Name: jobs_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE jobs_prod FROM PUBLIC;
REVOKE ALL ON TABLE jobs_prod FROM jlevora;
GRANT ALL ON TABLE jobs_prod TO jlevora;
GRANT ALL ON TABLE jobs_prod TO rthaler;
GRANT SELECT ON TABLE jobs_prod TO libertyeaglehour;


--
-- Name: schools; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE schools FROM PUBLIC;
REVOKE ALL ON TABLE schools FROM jlevora;
GRANT ALL ON TABLE schools TO jlevora;
GRANT ALL ON TABLE schools TO rthaler;
GRANT ALL ON TABLE schools TO contractor;


--
-- Name: student_to_group; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE student_to_group FROM PUBLIC;
REVOKE ALL ON TABLE student_to_group FROM jlevora;
GRANT ALL ON TABLE student_to_group TO jlevora;
GRANT ALL ON TABLE student_to_group TO rthaler;
GRANT ALL ON TABLE student_to_group TO libertyeaglehour;
GRANT ALL ON TABLE student_to_group TO contractor;


--
-- Name: studentgroup_id_seq; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON SEQUENCE studentgroup_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE studentgroup_id_seq FROM jlevora;
GRANT ALL ON SEQUENCE studentgroup_id_seq TO jlevora;
GRANT ALL ON SEQUENCE studentgroup_id_seq TO rthaler;
GRANT ALL ON SEQUENCE studentgroup_id_seq TO libertyeaglehour;


--
-- Name: students; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE students FROM PUBLIC;
REVOKE ALL ON TABLE students FROM jlevora;
GRANT ALL ON TABLE students TO jlevora;
GRANT ALL ON TABLE students TO rthaler;
GRANT ALL ON TABLE students TO contractor;


--
-- Name: students_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE students_prod FROM PUBLIC;
REVOKE ALL ON TABLE students_prod FROM jlevora;
GRANT ALL ON TABLE students_prod TO jlevora;
GRANT ALL ON TABLE students_prod TO rthaler;
GRANT ALL ON TABLE students_prod TO libertyeaglehour;


--
-- Name: teachers; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE teachers FROM PUBLIC;
REVOKE ALL ON TABLE teachers FROM jlevora;
GRANT ALL ON TABLE teachers TO jlevora;
GRANT ALL ON TABLE teachers TO rthaler;
GRANT ALL ON TABLE teachers TO contractor;


--
-- Name: teachers_prod; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE teachers_prod FROM PUBLIC;
REVOKE ALL ON TABLE teachers_prod FROM jlevora;
GRANT ALL ON TABLE teachers_prod TO jlevora;
GRANT ALL ON TABLE teachers_prod TO rthaler;
GRANT ALL ON TABLE teachers_prod TO libertyeaglehour;


--
-- Name: users; Type: ACL; Schema: public; Owner: jlevora
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM jlevora;
GRANT ALL ON TABLE users TO jlevora;
GRANT ALL ON TABLE users TO rthaler;
GRANT ALL ON TABLE users TO contractor;


--
-- PostgreSQL database dump complete
--

