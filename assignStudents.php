<?php

session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_ASSIGN_STUDENT);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//BEGIN entry_html();
?>
<?php function entry_html() { ?>

<!-- begin content -->

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->
<script>
    var USER = {
        id : "<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;?>",
        type : "<?php echo isset($_SESSION['user_type']) ? $_SESSION['user_type'] : null;?>",
    };
</script>

<div class="assign-student-page">
   <label for="search-student">Search Student(s)</label>
   <input type="text" 
        id="search-student" 
        name="search-student" 
        size="40">

   <a href="#" class="pull-right">view old assign students</a>
</div>

<br>
<div class="alert fade in flash-message">
  <p></p>
</div>

<!-- implement full calendar for assign students -->
<div class="student-calendar">
    <h2 class="calendar-title">
        <span class="selected-student-name"></span>'s calendar
    </h2>

    <form id="saveAssignmentForm" action="php/saveAssignments.php" style="display: inline;">
        <input type="hidden" name="selected-student-id" class="selected-student-id">
        <input type="hidden" name="selected-teacher-id" class="selected-teacher-id">
        <button type="submit" class="btn btn-primary btn-sm pull-right submit-assignment-btn" disabled="disabled">Submit</button>
    </form>
    
    <div id='full-calendar' style="margin-top: 15px;"></div>
</div>

<?php
}
//END entry_html();

if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") {
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Assign Assignment";

        gen_header($title);
       
        entry_html();

    } else {
        header('Location: login.php');
        exit;
    }
    include('footer.php');
?>