<?php

ini_set('display_errors',"1");
include ("Assignment.php");
include("Constants.php");
include ("clubs/Club.php");

$sDate 			= isset($_GET['date']) ? $_GET['date'] : null;
$studentId 		= isset($_GET['student_id']) ? $_GET['student_id'] : null;
$teacherId 		= isset($_GET['teacher_id']) ? $_GET['teacher_id'] : null;
$isSaved 		= isset($_GET['isSaved']) ? $_GET['isSaved'] : false;
$note 			= isset($_GET['note']) ? $_GET['note'] : '';
$assignmentType = isset($_GET['assignment_type']) ? $_GET['assignment_type'] : '';

$lastDateOfMonth 	= date("Y/m/t", strtotime(str_replace('/', '-', $sDate)));
$date 				= new DateTime(str_replace('/', '-', $lastDateOfMonth));
$lastDateOfMonth 	= $date->format('d/m/Y');

$assignmentObj = new Assignment;
$clubObj = new Club($assignmentObj->dbCon);

// $clubAssignments = $clubObj->getStudentClubAssignments($studentId, $sDate, $lastDateOfMonth);
$datesList = $assignmentObj->getAssignmentList($sDate, $lastDateOfMonth, $note, $assignmentType);
$assignedAssignments = $assignmentObj->getStudentAssignments($sDate, $studentId, $teacherId, $assignmentType);

$assignedAssignments = $clubObj->addClubDetailsToClubAssignment($assignedAssignments, $lastDateOfMonth);

/*if($clubAssignments) {
	$assignedAssignments = array_merge($clubAssignments, $assignedAssignments);
}*/

$datesList = $assignmentObj->setAssignmentStatus($datesList, $assignedAssignments, $isSaved, $teacherId);

$datesListCopy = $datesList;

// unset club assignment if day have normal assignment
if ($isSaved) {
	foreach ($datesListCopy as $key => $dlCopy) {
		foreach ($datesList as $key2 => $dl) {
			if (($dlCopy['event_date'] == $dl['event_date']) && $dlCopy['group_id'] == null && $dl['group_id'] != null ) {
				unset($datesList[$key2]);
			}
		}
	}
}

$datesList = array_values($datesList);

echo json_encode($datesList);
exit;