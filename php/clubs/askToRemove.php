<?php 

if(!isset($_SESSION)) {
     session_start();
}
ini_set('display_errors',"1");

include($_SERVER['DOCUMENT_ROOT'].'/php/Constants.php');
include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
include($_SERVER['DOCUMENT_ROOT'].'/php/Mailer.php');

$commonObj = new Common($dbh);

$groupId = isset($_POST['group_id']) ? $_POST['group_id'] : null;
$groupName = isset($_POST['group_name']) ? $_POST['group_name'] : null;

// send email process
$query = "SELECT admin_id FROM admin_to_group WHERE group_id=$groupId";
$SQLresult = pg_query($dbh, $query);

$adminIds = [];
if(pg_numrows($SQLresult)) {
	$adminIds = pg_fetch_all($SQLresult);
}

foreach ($adminIds as $key => $id) {
	$admin = $commonObj->getUserbyId($id['admin_id']);

	$email = $admin->username . Common::LIBERTY_EMAIL_DOMAIN;
	$enviroment = Common::APP_ENV;
	if ($enviroment == 'local') {
		$email = Common::TEACHER_TEST_EMAIL;
	}

	$mail = new Mailer;

	$mail->addAddress($email);

	$mail->sendClubRemoveMailToAdmin($admin, $groupName);
}

$respose = [
			'error' => false,
			'message' => 'Remove request send successfully.'
		];
		
echo json_encode($respose);
exit;