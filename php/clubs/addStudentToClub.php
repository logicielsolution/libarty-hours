<?php

session_start();

ini_set('display_errors',"1");
include("Club.php");

$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$groupId = isset($_POST['group_id']) ? $_POST['group_id'] : null;

$clubObj = new Club;
$response = $clubObj->addStudentToClub($studentId, $groupId);

echo $response;
exit;