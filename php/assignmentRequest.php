<?php 
ini_set('display_errors',"1");

if(!isset($_SESSION)) {
     session_start();
}

include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
include('Mailer.php');
include("Teacher.php");
include("Student.php");

$selectedDate = isset($_POST['date']) ? $_POST['date'] : null;
$teacherId = isset($_POST['teacher_id']) ? $_POST['teacher_id'] : null;
$note = isset($_POST['note']) ? $_POST['note'] : null;
$studentId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

$teacherObj = new Teacher;
$studentObj = new Student;

$teacher = $teacherObj->getTeacherById($teacherId);
$student = $studentObj->getStudentById($studentId);

$email = $teacher->username . Common::LIBERTY_EMAIL_DOMAIN;
$enviroment = Common::APP_ENV;
if ($enviroment == 'local') {
	$email = Common::TEACHER_TEST_EMAIL;
}

if (isAnyAssignmentExist($dbh, $studentId, $selectedDate)) {
	$response['error'] = true;
	$response['message_type'] = 'error';
	$response['message'] = "You have already a assignment on this date.";

	echo json_encode($response);
	exit();
}

$query = "INSERT INTO assignments (student_id, teacher_id, date, unassigned, note) VALUES ($1, $2, $3, $4, $5)";
		
$result = pg_prepare($dbh, "insert_requested_query", $query);
$result = pg_execute($dbh, "insert_requested_query", [$studentId, $teacherId, $selectedDate, 1, $note]);
	
if(!$result) {
	$response['error'] = true;
	$response['message_type'] = 'error';
	$response['message'] = "Requested assignment could not saved. Please try again.";

	echo json_encode($response);
	exit();
}

addEntryToStudentRequests($dbh, $studentId, $selectedDate);

$mail = new Mailer;
$mail->addAddress($email);
$mail->sendAssignmentRequestMailToAdmin($teacher, $student, $selectedDate);

$event = [
	'title' => 'Liberty School',
	'start' => $selectedDate,
	'color'	=> 'grey',
	'teacher' => ucfirst($teacher->full_name),
	'note' 	=> $note
];

echo json_encode($event);


/**
 * when student request for assignment then add assignment request entry in student requests
 * table 
 * @param int  $studentId
 * @param date  $selectedDate
 * @return bool
 */
function addEntryToStudentRequests($dbh, $studentId, $selectedDate)
{
	$query = "INSERT INTO student_requests (student_id, status, date, request_type) VALUES ($1, $2, $3, $4)";
	
	$result = pg_prepare($dbh, "insert_query", $query);
	$result = pg_execute($dbh, "insert_query", [$studentId, Common::STATUS_PENDING, $selectedDate, Common::ASSIGNMENT_REQUETS]);
	
	if(!$result) {
		return false;
	}

	return true;
}

function isAnyAssignmentExist($dbh, $studentId, $date)
{
	$assignmentQuery = "SELECT * FROM assignments WHERE 1=1";
	$assignmentQuery .= " AND student_id = '$studentId' AND date = '$date'";

	$SQLresult = pg_query($dbh, $assignmentQuery);
   	if(!pg_numrows($SQLresult)) {
		return false;
	}

	return true;
}