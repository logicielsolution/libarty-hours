<?php
session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_BLACKOUT_DAYS);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//BEGIN entry_html();
?>
<?php function entry_html($dbh) { ?>

<!-- begin content -->

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->

<div style="margin-top: 20px;">
    <h4 style="display: inline;"><b>Blackout days</b></h4>
    <button class="btn btn-primary btn-sm pull-right" id="add-blackout-day">ADD</button>

    <table class="table table-bordered" style="margin-top: 20px;">
        <colgroup>
            <col style="width: 20%;" />
            <col style="width: 40%;" />
            <col style="width: 30%;" />
            <col style="width: 10%;" />
        </colgroup>
        <thead>
            <tr>
                <th>Date</th>
                <th>Holiday</th>
                <th>Building</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $commonObj = new Common;
                
                $query="SELECT * FROM blackout_days";
                $buildingId = Common::getSchoolId();

                if (!Common::isStudent()) {
                    $buildingId = $commonObj->getBuildingCodeById($buildingId);
                }
                if ($buildingId) {
                    $query .= " WHERE building_id='$buildingId'"; 
                }
                
                $query .= ' ORDER BY date';

                $result = pg_query($dbh, $query);
                if (!$result) {
                    echo "An error occurred.\n";
                    exit;
                }

                $days = pg_fetch_all($result);
                if ($days) {
                    foreach ($days as $key => $day) {
            ?>
                        <tr>
                            <td><?php echo date("d-m-Y", strtotime($day['date'])); ?></td>
                            <td><?php echo $day['holyday']; ?></td>
                            <td>
                                <?php
                                    echo $commonObj->getBuildingNames($day['building_id']); 
                                ?>
                            </td>
                            <td class="text-center">
                                <a class="edit-blackout-day" 
                                    data-id="<?php echo $day['id']; ?>"
                                    style="cursor: pointer;">
                                  <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                |
                                <a class="delete-blackout-day" 
                                    data-id="<?php echo $day['id']; ?>"
                                    style="cursor: pointer;">
                                  <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
            <?php 
                    }
                } else {
                    echo "<tr><td colspan='4' class='text-center'>No Record Found</td></tr>";
                }
            ?>
        </tbody>
    </table>
</div>

<?php
}

if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>
<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") {
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Blackout Days";

        gen_header($title);
       
        entry_html($dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include('footer.php');
?>