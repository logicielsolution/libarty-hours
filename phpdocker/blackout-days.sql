--
-- Create blackout_days table if not exist
--
CREATE TABLE IF NOT EXISTS public.blackout_days (id SERIAL PRIMARY KEY, date date, holyday text);

INSERT INTO blackout_days (date, holyday) VALUES ('2017-04-24');
INSERT INTO blackout_days (date, holyday) VALUES ('2017-04-26');
INSERT INTO blackout_days (date, holyday) VALUES ('2017-04-28');