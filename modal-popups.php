<?php if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') { ?>
<!-- Add Blackout Day Modal -->
<div class="modal fade" id="addBlackoutDayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="blackoutDaysForm"
				class="form-horizontal" 
				action="/php/blackout-days/saveBlackoutDay.php">
				
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h3 class="modal-title" id="myModalLabel">Add Blackout Day</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
				      	<label class="control-label col-sm-2" for="date">Date:</label>
					    <div class="col-sm-5">
							<input type="text" class="form-control" name="date" id="blackout-day-date" readonly>
					    </div>
				    </div>
				    <div class="form-group">
				      	<label class="control-label col-sm-2" for="holyday">Holiday:</label>
					    <div class="col-sm-5">
							<input type="text" class="form-control add-holyday" name="holyday">
					    </div>
				    </div>
						
					<?php 
						$commonObj = new Common; 
						
						$buildingId = Common::getSchoolId();
						if (!Common::isStudent()) {
		                    $buildingId = $commonObj->getBuildingCodeById($buildingId);
		                }
						if ($buildingId ) {
					?>
						<input type="hidden" name="building" value="<?php echo $buildingId; ?>">
					<?php 
						} else {
					?>
						<div class="form-group">        
				      		<label class="control-label col-sm-2" for="building">Building: </label>
					      	<div class="col-sm-10">
					        	<div class="checkbox">
									<?php
									 	$schools = $commonObj->getSchools(); 
									 	foreach ($schools as $key => $school) {
									?>
				          				<label>
											<input type="checkbox"
												class="blackout-days-checkbox"
												name="building[<?php echo $key; ?>]" 
												value="<?php echo $school['sasi_building_code']; ?>">
												<?php echo $school['school_name']; ?>
				          				</label>
										<br>
	  								<?php } ?>
	  								<span style="color: red" class="building-selection-error"></span>
					        	</div>
					      	</div>
					    </div>
					<?php 
						} 
					?>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary add-submit-btn">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- edit blackout day modal -->
<div class="modal fade" id="editBlackoutDayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="editblackoutDaysForm"
				class="form-horizontal"
				action="/php/blackout-days/updateBlackoutDay.php">
				
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h3 class="modal-title" id="myModalLabel">Update Blackout Day</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
				      	<label class="control-label col-sm-2" for="date">Date:</label>
					    <div class="col-sm-5">
							<input type="text" name="date" class="form-control edit-date" readonly>
					    </div>
				    </div>
				    <div class="form-group">
				      	<label class="control-label col-sm-2" for="holyday">Holiday:</label>
					    <div class="col-sm-5">
							<input type="text" name="holyday" class="form-control edit-holyday">
					    </div>
				    </div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary update-submit-btn">Update</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Update note modal -->
<div class="modal fade" id="updateNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="updateNoteForm" action="/php/updateNote.php" style="margin: 0;">
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h3 class="modal-title" id="myModalLabel">Update Note</h3>
				</div>
				<div class="modal-body">
					<table>
					<tr>
						<th>
							<label for="">Note</label>
						</th>
						<td style="padding-left:10px; ">
							<textarea name="note" id="update-note" cols="25" rows="3"></textarea>
						</td>
					</tr>
				</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary update-note-submit-btn">Update</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Assignment request popup -->
<div class="modal fade" id="assignmentRequestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="assignmentRequestPopupForm" 
				class="form-horizontal"
				action="/php/assignmentRequest.php">
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h3 class="modal-title" id="myModalLabel">Assignment Request</h3>
				</div>
				<div class="modal-body">

				    <!-- Teacher Name -->
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="search-teacher">Teacher Name:</label>
					    <div class="col-sm-6">
							<input type="text" 
								class="form-control" 
						        id="search-teacher-for-assignment_request" 
						        name="search-teacher">
					    </div>
				    </div>

				    <!-- Note -->
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="assignment-note">Note:</label>
					    <div class="col-sm-6">
							<textarea class="form-control"
								name="note" 
								id="assignment-request-note" 
								rows="3"></textarea>
					    </div>
				    </div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" disabled="disabled">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Accept Deny assignment request -->
<div class="modal fade" id="allowDenyAssignmentRequestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="allowDenyAssignmentRequestPopupForm" 
				class="form-horizontal"
				action="/php/allowDenyAssignmentRequest.php">
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h4 class="modal-title" id="myModalLabel">Accept Assignment Request</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="assignment_id" id="requested-assignment-id">
				    <div class="form-group">
				      	<label class="control-label col-sm-3">Student Name:</label>
					    <div class="col-sm-6">
					    	<h5 class="requestl-assignment-student-name" style="font-weight: bold;">
					    	</h5>
					    </div>
				    </div>
				    
				    <div class="form-group">
				      	<label class="control-label col-sm-3" for="assignment-note">Note:</label>
					    <div class="col-sm-6" style="margin-top:7px; ">
							<p class="requestl-assignment-note"></p>
					    </div>
				    </div>
				</div>
				<div class="modal-footer">
					<button type="submit" 
						name="submit" 
						class="btn btn-success assignment-request-submit-button" 
						value="allow">Allow</button>
					<button type="submit" 
						name="submit" 
						class="btn btn-danger assignment-request-submit-button" 
						value="deny">Deny</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Mark assignment attendance -->
<div class="modal fade" id="assignmentAttendanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="assignmentAttendanceRequestForm" 
				class="form-horizontal"
				action="/php/assignmentAttendanceRequest.php">
				<div class="modal-header" style="background-color: #2e6da4; color: #fff; ">
					<button type="button" 
						class="close" 
						data-dismiss="modal" 
						aria-label="Close"
						style="color: #fff; margin: 0;">
							<span aria-hidden="true">&times;</span>
						</button>
					<h4 class="modal-title" id="myModalLabel">Mark Attendance</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="assignment_id" id="attendance-assignment-id">
					<div class="row">
						<div class="col-sm-6">
				    		<label class="radio-inline">
				    			<input type="radio" 
				    				name="attendance"
				    				class="present-radio" 
				    				value="present"
				    				style="margin-top:0px; "
				    				checked="checked">
				    			Present
				    		</label>
						</div>
						<div class="col-sm-6">
							<label class="radio-inline">
								<input type="radio" 
									name="attendance"
									value="absent"
									style="margin-top:0px; ">
								Absent
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php } ?>