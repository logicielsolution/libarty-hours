<?php

/*
   This file is required by include, for our pages that need to verify if a user is allowed access
   or not.  It should be owned by root:www-data and chmod 640 to prevent others from viewing
   how it works.
*/

//print_r($_SERVER); exit;

if (isset($_SESSION['tech_browser_setting']) && $_SESSION['tech_browser_setting'] != "") {
   $_SESSION['browser_setting'] = $_SESSION['tech_browser_setting'];
}

if ($_SESSION['browser_setting'] != sha1(md5("yes" . $_SESSION['user_id'] . $_SESSION['username'] . session_id()))) {
//echo "STOPPED >> " . $_SESSION['browser_setting'] . " << != (( " . sha1(md5("yes" . $_SESSION['user_id'] . $_SESSION['username'] . session_id())) . " )) "; exit;
   session_unset();
   header("Location: http://web.liberty.k12.mo.us" . $_SERVER['REQUEST_URI']);
   exit;

}

?>