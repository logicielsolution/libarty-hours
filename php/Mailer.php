<?php

require_once $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
 
class Mailer extends PHPMailer
{
	

	//Set SMTP host name                          
	public $Host = "smtp.gmail.com";

	//Set this to true if SMTP host requires authentication to send email
	public $SMTPAuth = true;                          

	//Provide username and password     
	public $Username = "testdata777@gmail.com";                 
	public $Password = "logiciel";                           

	//If SMTP requires TLS encryption then set it
	public $SMTPSecure = "tls";                           

	//Set TCP port to connect to 
	public $Port = 587;

	//From email address and name
	public $From = "testdata777@gmail.com";
	public $FromName = "Test Data";

	public function __construct()
	{

		parent::__construct();

		//Set PHPMailer to use SMTP.
		$this->isSMTP();

		//Send HTML or Plain Text email
		$this->isHTML(true);
	}

	/**
	 * Send assignment email to student
	 * @param  array  $student  Student detail
	 * @return bool
	 */
	public function sendAssignmentMailToStudent($student, $records)
	{

		$this->Subject = "Subject Text";

		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/studentEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##RECORDS##'];
		$replaceTo 		= [$student->full_name, $records];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    // echo "Mailer Error: " . $this->ErrorInfo;
		    return false;
		} 
		else 
		{
		    // echo "Message has been sent successfully";
		    return true;
		}
	}

	/**
	 * Send assignment email to teacher
	 * @param  array  $teacher  Teacher detail
	 * @return bool
	 */
	public function sendAssignmentMailToTeacher($teacher, $records)
	{

		$this->Subject = "Subject Text Teacher Mail";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/teacherEmailTemplate.html");		
		$replaceWith 	= ['##TEACHER_NAME##', '##RECORDS##'];
		$replaceTo 		= [$teacher->full_name, $records];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    // echo "Mailer Error: " . $this->ErrorInfo;
		    return false;
		} 
		else 
		{
		    // echo "Message has been sent successfully";
		    return true;
		}
	}

	/**
	 * Email send to admin when student request to join the club
	 * @param  object  $admin  admin detail
	 * @param  string  $groupName
	 * @return bool
	 */
	public function sendClubJoiningMailToAdmin($admin, $groupName)
	{
		$this->Subject = "I Want Join Club";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/askToJoinClubEmailTemplate.html");		
		$replaceWith 	= ['##ADMIN_NAME##', '##STUDENT_NAME##', '##GROUP_NAME##'];
		$replaceTo 		= [$admin->full_name, $_SESSION['full_user_name'], $groupName];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    // echo "Email has been sent successfully";
		    return true;
		}
	}

	/**
	 * Send email to admin when student wants to remove club
	 * @param  object  $admin  admin detail
	 * @param  string  $groupName
	 * @return bool
	 */
	public function sendClubRemoveMailToAdmin($admin, $groupName)
	{
		$this->Subject = "I Want to remove the club";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/askToRemoveClubEmailTemplate.html");		
		$replaceWith 	= ['##ADMIN_NAME##', '##STUDENT_NAME##', '##GROUP_NAME##'];
		$replaceTo 		= [$admin->full_name, $_SESSION['full_user_name'], $groupName];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    // echo "Email has been sent successfully";
		    return true;
		}	
	}

	/**
	 * send allow club request email to student
	 * @param  object  $student
	 * @param  string  $groupName
	 * @return bool
	 */
	public function sendAllowRequestEmail($student, $groupName)
	{
		$this->Subject = "Club Request Accepted";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/allowRequestEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##GROUP_NAME##'];
		$replaceTo 		= [$student->full_name, $groupName];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    // echo "Email has been sent successfully";
		    return true;
		}	
	}

	/**
	 * send deny club request email to student
	 * @param  object  $student
	 * @param  string  $groupName
	 * @return bool
	 */
	public function sendDenyRequestEmail($student, $groupName)
	{
		$this->Subject = "Club Request denied";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/denyRequestEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##GROUP_NAME##'];
		$replaceTo 		= [$student->full_name, $groupName];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    // echo "Email has been sent successfully";
		    return true;
		}
	}

	/**
	 * send email to teacher when send assignment request
	 * @param  object  $teacher
	 * @param  object  $student
	 * @param  date  $assignmentDate
	 * @return bool
	 */
	public function sendAssignmentRequestMailToAdmin($teacher, $student, $assignmentDate)
	{
		$assignmentDate = date("d-m-Y", strtotime($assignmentDate));

		$this->Subject = "Assignment Request";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/assignmentRequestEmailTemplate.html");		
		$replaceWith 	= ['##ADMIN_NAME##', '##STUDENT_NAME##', '##ASSIGNMENT_DATE##'];
		$replaceTo 		= [$teacher->full_name, $student->full_name, $assignmentDate];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    return true;
		}
	}

	/**
	 * Send email to student when teacher accept assignment request
	 * @param  object  $student
	 * @param  object  $teacher
	 * @param  date  $eventDate
	 * @return bool
	 */
	public function sendAllowAssignmentRequestMailToStudent($student, $teacher, $eventDate)
	{
		$this->Subject = "Assignment Request Accepted";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/acceptAssignmentRequestEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##ASSIGNMENT_DATE##', '##ADMIN_NAME##'];
		$replaceTo 		= [$student->full_name, $eventDate, $teacher->full_name];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    return true;
		}
	}

	/**
	 * Send email to student when teacher denied assignment request
	 * @param  object  $student
	 * @param  object  $teacher
	 * @param  date  $eventDate
	 * @return bool
	 */
	public function sendDenyAssignmentRequestMailToStudent($student, $teacher, $eventDate)
	{
		$this->Subject = "Assignment Request Denied";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/denyAssignmentRequestEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##ASSIGNMENT_DATE##', '##ADMIN_NAME##'];
		$replaceTo 		= [$student->full_name, $eventDate, $teacher->full_name];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} 
		else 
		{
		    return true;
		}
	}

	/**
	 * Send email to student when admin mark student in detention
	 * @param  object  $student
	 * @param  object  $teacher
	 * @return bool
	 */
	public function sendDetentionMailToStudent($student, $loggedInUser)
	{
		$loggedInUser = ucfirst($loggedInUser);
			
		$this->Subject = "Detention Email";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/detentionEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##ADMIN_NAME##'];
		$replaceTo 		= [$student->full_name, $loggedInUser];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} else {
		    return true;
		}	
	}

	/**
	 * send email to student when teacher mark student as absent
	 * @param  object  $student
	 * @param  string  $teacherName
	 * @param  date  $assignmentDate
	 * @return bool
	 */
	public function sendAbsentAttendanceMailToStudent($student, $teacherName, $assignmentDate)
	{
		$this->Subject = "Attendance Mail";
		
		$htmlContent 	= file_get_contents($_SERVER['DOCUMENT_ROOT']."/html/email/attendanceEmailTemplate.html");		
		$replaceWith 	= ['##STUDENT_NAME##', '##ASSIGNMENT_DATE##', '##TEACHER_NAME##'];
		$replaceTo 		= [$student->full_name, $assignmentDate, $teacherName];
		$htmlContent 	= str_replace($replaceWith, $replaceTo, $htmlContent);
		$this->Body 	= $htmlContent;

		if(!$this->send()) 
		{
		    return false;
		} else {
		    return true;
		}
	}
}