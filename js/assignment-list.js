
$(document).ready(function(){


    
    
    function sendData(id , teacherName , date , attend , stdID)
    {
            $.ajax({
                url: 'php/assignmentAttendanceRequest.php',
                dataType: 'json',
                data:{'teacher':teacherName , 'assignment_date':date , 'assignment_id':id , 
                      'attendance': attend , 'student_id':stdID },
                type: 'POST',
                beforeSend: function() {
                    showLoader();
                }
            })
            .success(function(response) {
            //    console.log(response);
                location.reload();
            })
            .always(function() {
                hideLoader();
            });
    }

    function acceptDenyAssignmentRequest(assignmentId , assignmentDate , submitType , stdID) {
        $.ajax({
            url: '/php/allowDenyAssignmentRequest.php',
            dataType: 'json',
            data : {
                'date':assignmentDate ,
                'assignment_id':assignmentId , 
                'submit_type': submitType ,
                'student_id':stdID 
            },
            type: 'POST',
            beforeSend: function() {
                showLoader();
            }
        })
        .success(function(response) {
        //    console.log(response);
            location.reload();
        })
        .always(function() {
            hideLoader();
        });
    }

    $('.absentButton').click(function(){ 
        var assignmentListId = $(this).attr('data-aid');
        var teacherName = $(this).attr('data-tname');
        var assignmentDate = $(this).attr('data-adate');
        var sid = $(this).attr('data-sid');
        var attendence = 'absent';  //Absent
        if (confirm("Do you really want to absent this student?")) {
            sendData( assignmentListId , teacherName , assignmentDate , attendence , sid);
        }
    }); 

    $('.presentButton').click(function(){ 
        var assignmentListId = $(this).attr('data-aid');
        var teacherName = $(this).attr('data-tname');
        var assignmentDate = $(this).attr('data-adate');
        var sid = $(this).attr('data-sid');
        var attendence = 'present';  // Present
        if (confirm("Do you really want to present this student?")) {
            sendData(assignmentListId , teacherName , assignmentDate , attendence , sid);
        }
    });

    $('.allowAssignmentRequestButton').click(function(){ 
        var assignmentId = $(this).attr('data-aid');
        var assignmentDate = $(this).attr('data-adate');
        var sid = $(this).attr('data-sid');
        var submitType = 'allow'; // Allow

        if (confirm("Do you really want to accept assignment request?")) {
            acceptDenyAssignmentRequest( assignmentId , assignmentDate , submitType , sid);
        }
    }); 

    $('.denyAssignmentRequestButton').click(function(){ 
        var assignmentId = $(this).attr('data-aid');
        var assignmentDate = $(this).attr('data-adate');
        var sid = $(this).attr('data-sid');
        var submitType = 'deny';  // Deny
        
        if (confirm("Do you really want to deny assignment request?")) {
            acceptDenyAssignmentRequest(assignmentId , assignmentDate , submitType , sid);
        }
    });




}); // end of docmuent


