<?php

session_start();

ini_set('display_errors',"1");
include("Club.php");
/*print_r($_POST);
exit();*/
$clubAdminID = isset($_POST['clubAdminID']) ? $_POST['clubAdminID'] : null;
$clubBuilding = isset($_POST['clubBuilding']) ? $_POST['clubBuilding'] : null;
$clubName = isset($_POST['clubName']) ? $_POST['clubName'] : null;
$clubDescription = isset($_POST['clubDescription']) ? $_POST['clubDescription'] : null;
$clubDates = isset($_POST['clubDates']) ? $_POST['clubDates'] : null;
$clubAdmins = isset($_POST['admins']) ? $_POST['admins'] : null;

if (!$clubName || !$clubDescription || !$clubDates || !$clubAdmins) {
	$response['validation_error'] = true;
	$response['message'] = "Please fill all the input fields";

	echo json_encode($response);
	exit;
}

$clubObj = new Club;

$clubDetail = [
	'clubAdminID' 		=> $clubAdminID,
	'clubBuilding' 		=> $clubBuilding,
	'clubName' 			=> $clubName,
	'clubDescription' 	=> $clubDescription,
	'clubDates' 		=> $clubDates,
	'clubAdmins' 		=> $clubAdmins
];

$response = $clubObj->saveClub($clubDetail);

echo $response;
exit;