<?php

session_start();

ini_set('display_errors',"1");
include("Club.php");

$teacherId = isset($_POST['teacher_id']) ? $_POST['teacher_id'] : null;
$groupId = isset($_POST['group_id']) ? $_POST['group_id'] : null;

$clubObj = new Club;
$response = $clubObj->addAdminToClub($teacherId, $groupId);

echo $response;
exit;