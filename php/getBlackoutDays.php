<?php

if(!isset($_SESSION)) {
     session_start();
}

include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');

$commonObj = new Common($dbh);

$id = isset($_GET['id']) ? $_GET['id'] : null;
$building_id = Common::getSchoolId();

if (!Common::isStudent()) {
	$building_id = $commonObj->getBuildingCodeById($building_id);
}

$query = "SELECT * FROM blackout_days";

if ($id && $building_id) {
	$query = $query . " WHERE id='$id' AND building_id='$building_id'"; 	
} elseif ($id) {
	$query = $query . " WHERE id='$id'"; 	
} elseif ($building_id) {
	$query .= " WHERE building_id='$building_id'";
}

$SQLresult = pg_query($dbh, $query);

$days = [];
if(pg_numrows($SQLresult)) {
  	$days = pg_fetch_all($SQLresult);
}

echo json_encode($days);