$(function() {

	// set default time zone
 	// moment.tz.setDefault("Asia/Kolkata");
 	moment.tz.setDefault("Africa/Abidjan"); //utc 00
 	console.log(moment().format());

 	$('.flash-message').hide();
});

function showLoader() {
    $('#loader').show();
    $('#blur-backgroung').show();
}

function hideLoader() {
    $('#loader').hide();
    $('#blur-backgroung').hide();
}

function showFlashMessage(type, message) {
	var cls;
	switch(type) {
	    case 'success':
	        cls = 'alert-success';
	        break;
	    case 'info':
	        cls = 'alert-info';
	        break;
	    default:
	        cls = 'alert-danger'
	} 

	$('.flash-message').addClass(cls);
	$('.flash-message p').text(message);
	$('.flash-message').show();
	// $('body').scrollTop(0); //scroll window

	setTimeout(function() {
	    $('.flash-message').fadeOut('slow');
	}, 10000);
	setTimeout(function() {
		$('.flash-message').removeClass(cls);
	}, 12000);
}