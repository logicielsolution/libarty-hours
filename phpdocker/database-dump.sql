--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: admin_to_group_seq; Type: SEQUENCE SET; Schema: public; Owner: cbier
--

SELECT pg_catalog.setval('admin_to_group_seq', 6, true);


--
-- Name: assignments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jlevora
--

SELECT pg_catalog.setval('assignments_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jlevora
--

SELECT pg_catalog.setval('groups_id_seq', 14, true);


--
-- Name: studentgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jlevora
--

SELECT pg_catalog.setval('studentgroup_id_seq', 2, true);


--
-- Data for Name: admin_to_group; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO admin_to_group VALUES (4, 19095, 12);
INSERT INTO admin_to_group VALUES (5, 2846, 13);
INSERT INTO admin_to_group VALUES (6, 19095, 14);


--
-- Data for Name: allstudents; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO allstudents VALUES (1, 'astudent', 'alpha', 'student', 'test', 510, '', '', 'Alpha Test Student', 'student, alpha', '00000000', '12', true);
INSERT INTO allstudents VALUES (2, 'bstudent', 'beta', 'student', 'test', 510, '', '', 'Beta Test Student', 'student, beta', '00000000', '11', true);
INSERT INTO allstudents VALUES (3, 'cstudent', 'charlie', 'student', 'test', 510, '', '', 'Charlie Test Student', 'student, charlie', '0000000', '10', true);
INSERT INTO allstudents VALUES (4, 'dstudent', 'delta', 'student', 'test', 510, '', '', 'Delta Test Student', 'student, delta', '0000000', '09', true);
INSERT INTO allstudents VALUES (5, 'estudent', 'echo', 'student', 'test', 1070, '', '', 'Echo Test Student', 'student, echo', '0000000', '12', true);
INSERT INTO allstudents VALUES (6, 'fstudent', 'foxtrot', 'student', 'test', 1070, '', '', 'Foxtrot Test Student', 'student, foxtrot', '0000000', '11', true);
INSERT INTO allstudents VALUES (7, 'gstudent', 'Golf', 'Student', 'Test', 1070, '', '', 'Golf Test Student', 'student, golf', '0000000', '10', true);
INSERT INTO allstudents VALUES (8, 'hstudent', 'Hotel', 'Student', 'Test', 1070, '', '', 'Hotel Test Student', 'Student, Hotel', '0000000', '09', true);
INSERT INTO allstudents VALUES (9, 'lstudent', 'LS', 'Student', 'Test', 1010, '', '', 'LS Test Student', 'Student, LS', '0000000', '09', true);


--
-- Data for Name: allusers; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO allusers VALUES (20615, 'ateacher', 'alpha', 'teacher', 'alpha teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 3);
INSERT INTO allusers VALUES (20616, 'bteacher', 'Beta', 'teacher', 'beta teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 3);
INSERT INTO allusers VALUES (20617, 'cteacher', 'charlie', 'teacher', 'charlie teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 3);
INSERT INTO allusers VALUES (20618, 'dteacher', 'Delta', 'Teacher', 'delta teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 3);
INSERT INTO allusers VALUES (20619, 'etacher', 'echo', 'teacher', 'echo teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 22);
INSERT INTO allusers VALUES (20620, 'fteacher', 'foxtrot', 'teacher', 'foxtrot teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 22);
INSERT INTO allusers VALUES (20621, 'gteacher', 'Golf', 'Teacher', 'Golf Teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 22);
INSERT INTO allusers VALUES (20622, 'hteacher', 'Hotel', 'Teacher', 'Hotel Teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 22);
INSERT INTO allusers VALUES (20623, 'lteacher', 'LS', 'Teacher', 'LS Teacher', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', false, NULL, NULL, 23);


--
-- Data for Name: assignments; Type: TABLE DATA; Schema: public; Owner: jlevora
--



--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO groups VALUES (12, 'Test', 'Test', 19095, 1070, '2017-02-15');
INSERT INTO groups VALUES (13, 'Test', 'Test', 2846, 0, '2017-02-16');
INSERT INTO groups VALUES (14, 'Woodshop', 'This is an advanced woodshop class', 19095, 1070, '2017-02-17');


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO jobs VALUES (1, 20615, 1);
INSERT INTO jobs VALUES (2, 20616, 1);
INSERT INTO jobs VALUES (3, 20617, 1);
INSERT INTO jobs VALUES (4, 20618, 1);
INSERT INTO jobs VALUES (5, 20618, 1);
INSERT INTO jobs VALUES (6, 20619, 1);
INSERT INTO jobs VALUES (7, 20620, 1);
INSERT INTO jobs VALUES (8, 20621, 1);
INSERT INTO jobs VALUES (9, 20622, 1);
INSERT INTO jobs VALUES (10, 20615, 41);
INSERT INTO jobs VALUES (11, 1, 41);
INSERT INTO jobs VALUES (12, 2, 41);
INSERT INTO jobs VALUES (13, 3, 41);


--
-- Data for Name: schools; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO schools VALUES (3, 'Liberty High School', 510);
INSERT INTO schools VALUES (22, 'Liberty North High School', 1070);
INSERT INTO schools VALUES (23, 'LS School', 1010);


--
-- Data for Name: student_to_group; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO student_to_group VALUES (2, '49222', 3);


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO students VALUES (81870, '81870astudent', 'Alpha', 'Student', '', 510, '', '', 'Alpha Student', 'Student, Alpha', '9900000001', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '12', NULL);
INSERT INTO students VALUES (81871, '81871bstudent', 'Beta', 'Student', '', 510, '', '', 'BETA Student', 'Student, Beta', '9900000002', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '11', NULL);
INSERT INTO students VALUES (81872, '81872cstudent', 'Charlie', 'Student', '', 510, '', '', 'Charlie Student', 'Student, Charlie', '9900000003', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '10', NULL);
INSERT INTO students VALUES (81873, '81873dstudent', 'Delta', 'Student', '', 510, '', '', 'Delta Student', 'Student, Delta', '9900000004', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '9', NULL);
INSERT INTO students VALUES (81874, '81874estudent', 'Echo', 'Student', '', 1070, '', '', 'Echo Student', 'Student, Echo', '9900000005', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '12', NULL);
INSERT INTO students VALUES (81875, '81875fstudent', 'Foxtrot', 'Student', '', 1070, '', '', 'Foxtrot Student', 'Student, Foxtrot', '9900000006', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '11', NULL);
INSERT INTO students VALUES (81876, '81876gstudent', 'Golf', 'Student', '', 1070, '', '', 'Golf Student', 'Student, Gold', '9900000007', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '10', NULL);
INSERT INTO students VALUES (81877, '81877hstudent', 'Hotel', 'Student', '', 1070, '', '', 'Hotel Student', 'Student, Hotal', '9900000008', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '9', NULL);
INSERT INTO students VALUES (81878, '81878lstudent', 'LS', 'Student', '', 1010, '', '', 'LS Student', 'Student, LS', '9900000008', 'test', '$2a$10$1bbdb208b9ce5b54bf885uT82P2mPaN2h1cSskxDNKyeVDa/Zcrze', '9', NULL);


--
-- Data for Name: teachers; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO teachers VALUES (20615, 'ateacher', 'teacher', 'alpha', 'Alpha Teacher', 'Teacher, Alpha', 3, 'Liberty High School', NULL, '');
INSERT INTO teachers VALUES (20616, 'bteacher', 'teacher', 'beta', 'Beta Teacher', 'Teacher, Beta', 3, 'Liberty High School', NULL, '');
INSERT INTO teachers VALUES (20617, 'cteacher', 'Teacher', 'Charlie', 'Charlie Teacher', 'Teacher, Charlie', 3, 'Liberty High School', NULL, '');
INSERT INTO teachers VALUES (20618, 'dteacher', 'Teacher', 'Delta', 'Delta Teacher', 'Teacher, Delta', 3, 'Liberty High School', NULL, '');
INSERT INTO teachers VALUES (20619, 'eteacher', 'Teacher', 'Echo', 'Echo Teacher', 'Teacher, Echo', 22, 'Liberty North High School', NULL, '');
INSERT INTO teachers VALUES (20620, 'fteacher', 'teacher', 'foxtrot', 'foxtrot teacher', 'teacher, foxtrot', 22, 'Liberty North High School', NULL, '');
INSERT INTO teachers VALUES (20621, 'gteacher', 'teacher', 'Golf', 'Golf Teacher', 'Teacher, Golf', 22, 'Liberty North High School', NULL, '');
INSERT INTO teachers VALUES (20622, 'hteacher', 'Teacher', 'Hotel', 'Hotel Teacher', 'Teacher, Hotel', 22, 'Liberty North High School', NULL, '');
INSERT INTO teachers VALUES (20623, 'lteacher', 'Teacher', 'LS', 'LS Teacher', 'Teacher, LS', 23, 'LS School', NULL, '');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: jlevora
--

INSERT INTO users VALUES (1, 'superadmin', 'Super', 'Admin', 'Super Admin', null, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (2, 'admin', 'Building', 'Admin', 'Building Admin', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (3, 'nsadmin', 'North', 'Admin', 'North Admin', 22, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty North High School', NULL);
INSERT INTO users VALUES (20616, 'bteacher', 'beta', 'teacher', 'beta teacher', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (20615, 'ateacher', 'alpha', 'teacher', 'alpha teacher', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (20617, 'cteacher', 'charlie', 'teacher', 'charlie teacher', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (20618, 'dteacher', 'delta', 'teacher', 'delta teacher', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty High School', NULL);
INSERT INTO users VALUES (20619, 'eteacher', 'echo', 'teacher', 'echo teacher', 22, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty North High School', NULL);
INSERT INTO users VALUES (20620, 'fteacher', 'foxtrot', 'teacher', 'foxtrot teacher', 22, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty North High School', NULL);
INSERT INTO users VALUES (20621, 'gteacher', 'Golf', 'teacher', 'Golf teacher', 22, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty North High School', NULL);
INSERT INTO users VALUES (20622, 'hteacher', 'Hotel', 'teacher', 'Hotel teacher', 22, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'Liberty North High School', NULL);
INSERT INTO users VALUES (20623, 'lteacher', 'LS', 'teacher', 'LS teacher', 23, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', true, NULL, '$2a$10$1bbdb208b9ce5b54bf885u76h3uiXP8p3Wdqr8ZOwMGMiCDJKak6q', 'LS School', NULL);


--
-- PostgreSQL database dump complete
--

