<?php 

ini_set('display_errors',"1");
include("BlackoutDay.php");

$id = isset($_POST['id']) ? $_POST['id'] : null;
$holyday = isset($_POST['holyday']) ? $_POST['holyday'] : null;
$date = isset($_POST['date']) ? $_POST['date'] : null;
if ($date) {
	$date = date("Y-m-d", strtotime($date));
}

$blackoutDayObj = new BlackoutDay;
$response = $blackoutDayObj->update($id, $date, $holyday);

echo $response;
exit;