$(function(){

    findTeacher();
    searchClub();
    // search teacher
    function findTeacher() {
        if ($("#findTeacher").length < 1) {
            return;
        }
        $( "#findTeacher" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "php/getTeachers.php",
                    dataType: "json",
                    data: {
                        name: request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                //console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );

                var selectedTeacher = {
                    id: ui.item.id,
                    name: ui.item.full_name
                };

                setTimeout(function() {
                    $( "#findTeacher" ).val(ui.item.full_name);
                    $("#teacherID").val(ui.item.id);
                }, 1);

            },

            focus: function( event, ui) {
                $( "#findTeacher" ).val(ui.item.full_name);
                event.preventDefault();
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            ul.css('z-index', 1100);
            return $( "<li>" ).data("item.autocomplete", item)
            .append( item.full_name )
            .appendTo( ul );
        };
    }
    //search club
    function searchClub() {
        if ($("#searchClub").length < 1) {
            return;
        }
        $( "#searchClub" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "php/clubs/getClubList.php",
                    dataType: "json",
                    data: {
                        name: request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
            //console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );       
            console.log(ui.item.id);
                var selectedClub = {
                    id: ui.item.id,
                    name: ui.item.name
                };

                setTimeout(function() {
                    $( "#searchClub" ).val(ui.item.name);
                    $("#clubId").val(ui.item.id);
                }, 1);

            },

            focus: function( event, ui) {
                $( "#searchClub" ).val(ui.item.name);
                event.preventDefault();
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            ul.css('z-index', 1100);
            return $( "<li>" ).data("item.autocomplete", item)
            .append( item.name )
            .appendTo( ul );
        };
    }
});








