<?php

/*
  Log Out
  Clear all $_SESSION data
  Written by Brian Harmon
  3/10/06
*/

session_start();
session_unset();
session_destroy();
header('Location: index.php');
exit;

?>
