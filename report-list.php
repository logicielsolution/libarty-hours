<?php

session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_ASSIGNMENT_REPORTLIST);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

?>
<!-- BEGIN entry_html() -->
<?php function entry_html($dbh) { ?>

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->
<script>
 $(document).ready(function(){
    //  select date for assignment list
    $('.from').datepicker({
        format: "mm-dd-yyyy",
        todayHighlight: true,
        beforeShowDay: function (date){
            if (date > new Date()) {
                return false;
            }
        }
    });

    $('.to').datepicker({
        format: "mm-dd-yyyy",
        todayHighlight: true,
        beforeShowDay: function (date){
            if (date > new Date()) {
                return false;
            }
        }
    }); 
    // end of date function 
 });

</script>

<?php
    $fromDate = isset( $_GET['fromDate'] ) ? $_GET['fromDate'] : null;  
    $toDate = isset( $_GET['toDate'] ) ? $_GET['toDate'] : null;  
    $teacherName = isset( $_GET['teacher'] ) ? $_GET['teacher'] : null;  

    $linkData="";
    $num_page=10;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
    $start_from = ($page-1) * $num_page; 

    // create object and call  make query and assignment list 
    $objReports = new Reports();
    
    if((isset($_REQUEST['fromDate'] ) && isset($_REQUEST['toDate'])) 
        || isset($_REQUEST['teacherID'] ) )
    {
        $teacherID = isset($_REQUEST['teacherID']) ? $_REQUEST['teacherID'] : '';

        $fromDateArr = explode('-', $_REQUEST['fromDate']);
        $toDateArr = explode('-', $_REQUEST['toDate']);
        
        if ($_REQUEST['fromDate']) {
            $fromDate = $fromDateArr[1] . '-' . $fromDateArr[0] . '-' . $fromDateArr[2];
        }
        if ($_REQUEST['toDate']) {
            $toDate = $toDateArr[1] . '-' . $toDateArr[0] . '-' . $toDateArr[2];
        }

        $query= $objReports->makeQuery($fromDate, $toDate, $teacherID);                    
        //create link for pagination
         $linkData="&fromDate=".$_REQUEST['fromDate'];
         $linkData.="&toDate=".$_REQUEST['toDate'];
         $linkData.="&teacherID=".$teacherID;

    }
    else
    {
        $query = $objReports->makeQuery();
    }
    
    $reportType = Common::REPORT_TYPE_ASSIGNMENT;
    $reports = $objReports->getReportsListPagination($query, $num_page,$start_from, null, null, $reportType);
?>

<div style="margin-top: 20px;">
      
    <h4>
       <form action="createExcelFile.php" method="post">
              <b>Assignment Report</b>
         <input type="hidden" name="query" value="<?=$query; ?>" >  
         <input type="hidden" name="fromDate" value="<?=$fromDate; ?>" >  
         <input type="hidden" name="toDate" value="<?=$toDate; ?>" >  
         <input type="hidden" name="reportType" value="<?=$reportType; ?>" >  
         <input type="hidden" name="teacherName" value="<?=$teacherName; ?>" >  
         <input type="submit" value="Export To CSV" class="btn btn-info pull-right mybtn"  />
     </form>
    </h4>
    
    <hr/>

    <div class="form-group ">
        <form>

            <div class="col-sm-3">
                From:  
                <input type="text"  name="fromDate" class="form-control from" readonly
                   value="<?php echo isset($_GET['fromDate'])?$_GET['fromDate']:""; ?>" >
            </div>

            <div class="col-sm-3">                    
                To:     
                <input type="text" name="toDate" class="form-control to" readonly
                     value="<?php echo isset($_GET['toDate'])?$_GET['toDate']:""; ?>" >
            </div>
            
            <div class="col-sm-4">
                <?php if(! Common::isTeacher()) { ?>
                    Teacher Name :    
                    <input type="text" 
                        class="form-control" 
                        id="findTeacher" 
                        name="teacher"
                        value="<?php echo isset($_GET['teacher'])?$_GET['teacher']:""; ?>">
                <?php } ?>
                <input type="hidden" id="teacherID" name="teacherID" />     
            </div>

            <div class="col-sm-2">
                <div class="pull-right">
                    <input  type="submit"  class="btn btn-primary" value="Search"
                      style="margin-top:20px"  >
                    
                    <a class="btn btn-default" href="report-list.php"  style="margin-top:20px"  >  Reset </a>
                </div>
            </div>

        </form> <!-- End of FIlter Form -->  
    </div>

    <br/><br/><br/> 
    
    <table class="table table-bordered" style="margin-top: 20px;">
        <colgroup>
            <col style="width: 30%;" />
            <col style="width: 14%;" />
            <col style="width: 14%;" />
            <col style="width: 14%;" />
            <col style="width: 14%;" />
        </colgroup>
        <thead>
            <tr class="bg-primary">
                <th>Student Name</th>
                <th>Grade</th>
                <th>Assigned</th>
                <th>Missed</th>
                <th>Detentions</th>
             </tr>
        </thead>
        <tbody>
            <?php     
                if (count($reports)>0) {
                    foreach ((array)$reports as $key => $report) {
            ?>
                        <tr>
                            <td><?php echo  $report['student_name'] ;?></td>
                            <td><?php echo  $report['grade']  ;?></td>
                            <td><?php echo  $report['assigned'] ;?></td>
                            <td><?php echo  $report['absent'] ;?></td>
                            <td><?php echo  $report['detention'] ;?></td>
 
                        </tr>
            <?php   } // end of foreach
                } // end of if
                else
                {
                     echo "<tr><td colspan='6' class='text-center'>No Record Found</td></tr>";
                }
            ?>
        </tbody>
    </table>
</div>


<div class="text-center">
<ul class="pagination" id="paginationID">
<?php 


 $paginationquery = $query;
   $res = pg_query($dbh, $paginationquery);
     if (!$res) { echo "An error occurred.\n"; exit; }
     $total_pages = pg_num_rows($res); 



    if($total_pages>10)
    { 
  //     $total_pages = count($assigns); 
     $totalPaginationNumber =ceil($total_pages/$num_page);
     $active="";

       $prev = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

       $disablePrev='';
       if( $prev==1)
       {
        $disablePrev='class="btn disabled"';
       }

     $currentPageNumber = isset($_REQUEST['page'])?$_REQUEST['page']:1;

     $showPageNumber= array(1 ,$totalPaginationNumber);
     $showDots= array();

     if($totalPaginationNumber > 10){
        $counter=1;
        for($j=-2 ; $j<=2; $j++){
            if($currentPageNumber <= 3)
            {
                array_push($showPageNumber, $counter++); // 1 2 3 4 5
                array_push($showDots, $totalPaginationNumber-1);  // 12345....15             
            }
            else if($currentPageNumber>3 && $currentPageNumber <= $totalPaginationNumber-5)
            {
                array_push($showDots, 2 , $totalPaginationNumber-1);  // 1...5...15
                array_push($showPageNumber, $currentPageNumber+$j); // 6 7 [8] 9 10                
            }
            else 
            {
                array_push($showDots, 2);  // 12345....15                             
                array_push($showPageNumber, $totalPaginationNumber - ($counter++)); // 1 2 3 4 5                
            }
        }
     }
     else{
        for($j=1 ; $j<=$totalPaginationNumber; $j++){  
                array_push($showPageNumber, $j); // 1 2 3 4 5          
        }      
     }

     echo "<li><a $disablePrev href='report-list.php?page=".($prev-1).$linkData."'> &lt; </a> </li>"; 

     for ($i=1; $i<= $totalPaginationNumber; $i++) { 
           $active='';
           $disable='';



            if((isset($_REQUEST['page']) && $_REQUEST['page']==$i) || 
                (! isset($_REQUEST['page'])&& $i==1))
            { 
              $active = "active";  
              $disable= "class='btn disabled'";
            }

            if (in_array($i, $showPageNumber)){
               echo "<li  class='{$active}' ><a {$disable} href='report-list.php?page=".$i.$linkData."'>".
                  $i."</a> </li>"; 
              }
            else if( in_array($i, $showDots)){
                echo "<li><a  href='#'>.....</a> </li>"; 
              }

     } // end of for loop 

     if(!isset($_REQUEST['page']) ){  $_REQUEST['page']=1; }

       $next = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

       $disableNext='';
       if( $prev==$totalPaginationNumber)
       {
        $disableNext='class="btn disabled"';
       }
    
     echo "<li><a $disableNext href='report-list.php?page=".($prev+1).$linkData."'> &gt; </a> </li>"; 
    }


?>    
</ul> 
</div>


<?php
}
?>

<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Assignment Report";

        gen_header($title);
       
        entry_html($dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include("footer.php");
?>