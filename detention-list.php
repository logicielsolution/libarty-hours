<?php
session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_DETENTION_LIST);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//echo nl2br(print_r($_SESSION, 1)); exit;


//BEGIN entry_html();
?>
<?php function entry_html($dbh) { ?>

<!-- begin content -->

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->
<div style="margin-top: 20px;">
    <h4><b> Detention List</b></h4>
    
    <hr>

    <!-- filter -->
     <form class="form-inline" style="margin-top: 20px;">
        <div class="form-group">
            <label for="sName">Student Name:</label>
            <input type="text" 
                class="form-control" 
                id="sName"
                name="sName"
                value="<?php echo isset($_GET['sName']) ? $_GET['sName'] : null; ?>">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
        <a class="btn btn-default" href="detention-list.php">  Reset </a>
    </form>
   
    <table class="table table-bordered" style="margin-top: 20px;">
        <colgroup>
            <col style="width: 10%;" />
            <col style="width: 40%;" />
            <col style="width: 30%;" />
            <col style="width: 20%;" />
        </colgroup>
        <thead>
            <tr>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Assigment Date</th>
                <th>Building Name</th>
            </tr>
        </thead>
        <tbody>
            <?php 

                $commonObj = new Common;

                $sName = isset($_GET['sName']) ? $_GET['sName'] : null;

                $buildingId = Common::getSchoolId();
                $buildingId = $commonObj->getBuildingCodeById($buildingId);
            
                $query = "SELECT s.id, s.full_name ,a.date,  b.school_name 
                            FROM  assignments AS a, students AS s , schools AS b  
                            WHERE s.id=cast(a.student_id as int) 
                            AND a.detention=true  
                            AND s.building_id=b.sasi_building_code";
                if ($buildingId) {
                    $query .= " AND s.building_id=$buildingId";
                }

                if ($sName) {
                    $query .= " AND s.full_name ILIKE '$sName%'";
                }

                $query .= " ORDER BY a.date DESC";

                $result = pg_query($dbh, $query);
                if (!$result) {
                    echo "An error occurred.\n";
                    exit;
                }

                $detents = pg_fetch_all($result);

                if ($detents) {
                    foreach ((array)$detents as $key => $detent) {
                        $id = $detent['id'];
                        $name = $detent['full_name'];
                        $buildingName = $detent['school_name'];
                        $assignDate = date("d-m-Y", strtotime($detent['date']));
            ?>
                    <tr>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $assignDate; ?></td>
                        <td><?php echo $buildingName; ?></td>
                    </tr>

                <?php
                    }
                    } 
                    else
                    {
                         echo "<tr><td colspan='4' class='text-center'>No Record Found</td></tr>";
                    }


            ?>
 
        </tbody>
    </table>
</div>



<?php } //end entry html function ?>

<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Detention";

        gen_header($title);
       
        entry_html($dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include("footer.php");
?>