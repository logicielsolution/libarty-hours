<?php

ini_set('display_errors',"1");
include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');

$queries = [];



//add blackout days table
$blackoutDayquery = "CREATE TABLE IF NOT EXISTS public.blackout_days (id SERIAL PRIMARY KEY, date date, holyday text)";
$queries['blackout_days'] = $blackoutDayquery;




//add building id field
$addBuildingIdFieldToblackoutDayTable = "ALTER TABLE public.blackout_days ADD column IF NOT EXISTS building_id integer";
$queries['add_building_id_column_to_blackout_day_table'] = $addBuildingIdFieldToblackoutDayTable;




//change field data type
$changeStudentIdDataTypeInStudentToGroupTable = "ALTER TABLE public.student_to_group ALTER column student_id type text using student_id::text";
$queries['change_student_id_data_type_in_student_to_group_table'] = $changeStudentIdDataTypeInStudentToGroupTable;




//add club_dates table
$clubDatesquery = "CREATE TABLE IF NOT EXISTS public.club_dates (id SERIAL PRIMARY KEY, group_id integer, date date)";
$queries['club_dates'] = $clubDatesquery;



//change field data type
$changeStudentIdDataTypeInAssignmentsTable = "ALTER TABLE public.assignments ALTER column student_id type text using student_id::text";
$queries['change_student_id_data_type_in_assignments_table'] = $changeStudentIdDataTypeInAssignmentsTable;


//add student_requests table
$studentRequestQuery = "CREATE TABLE IF NOT EXISTS public.student_requests (id SERIAL PRIMARY KEY, group_id integer, student_id integer, status varchar,  date date)";
$queries['student_requests'] = $studentRequestQuery;

// rename group_requests table to student_requests
/*$renameTableQuery = "ALTER TABLE IF EXISTS group_requests RENAME TO student_requests";
$queries['rename_group_request_table_to_student_requests'] = $renameTableQuery;*/

//add request_type field in student_requests table
$addRequestTypeFieldToStudentRequestsTable = "ALTER TABLE public.student_requests ADD column IF NOT EXISTS request_type varchar";
$queries['add_request_type_column_to_student_requests_table'] = $addRequestTypeFieldToStudentRequestsTable;

//add request_type field in student_requests table
$addAssignmentTypeFieldToAssignmentTable = "ALTER TABLE public.assignments ADD column IF NOT EXISTS assignment_type varchar";
$queries['add_assignment_type_column_to_assignments_table'] = $addAssignmentTypeFieldToAssignmentTable;


//change field data type
$addTimeFieldToblackoutDayTable = "ALTER TABLE public.blackout_days ADD column IF NOT EXISTS time time";
$queries['add_time_field_to_blackout_days_table'] = $addTimeFieldToblackoutDayTable;


foreach ($queries as $key => $query) {
	$result = pg_query($dbh, $query);

	if (!$result) {
		echo "An error occurred in " . $key . " query.\n";
  		exit;
	}

	echo $key . " query executed.<br><br>";
}

exit;
 