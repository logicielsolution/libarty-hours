<?php

ini_set('display_errors',"1");

if(!isset($_SESSION)) {
     session_start();
}

include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
include('Mailer.php');
include("Teacher.php");
include("Student.php");

$assignmentId = isset($_POST['assignment_id']) ? $_POST['assignment_id'] : null;
$studentId = isset($_POST['student_id']) ? $_POST['student_id'] : null;
$loggedInUserName = isset($_SESSION['full_user_name']) ? $_SESSION['full_user_name'] : null;
$studentObj = new Student;
$student = $studentObj->getStudentById($studentId);

//Update status
$query = "UPDATE assignments SET detention = $1 WHERE id = $2";
		
$result = pg_prepare($dbh, "update_query", $query);
$result = pg_execute($dbh, "update_query", [1, $assignmentId]);
	
if(!$result) {
	$response['error'] = true;
	$response['message'] = "Assignment request not accepted. Please try again.";

	echo json_encode($response);
}

//Send email
$email = $student->username . Common::LIBERTY_EMAIL_DOMAIN;
$enviroment = Common::APP_ENV;
if ($enviroment == 'local') {
	$email = Common::STUDENT_TEST_EMAIL;
}

$mail = new Mailer;
$mail->addAddress($email);
$mail->sendDetentionMailToStudent($student, $loggedInUserName);

$response['error'] = false;
$response['message'] = 'Detention status successfully updated.';

echo json_encode($response);