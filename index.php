<?php
ini_set('display_errors',"1");
session_start();

include("php/Constants.php");
include("genericfunctionstech.php");

  //  echo 'Session path "'.session_save_path().'" is not writable for PHP!'; 

// This is the previous version of session timeout--changed back to test

if (isset($_SESSION['LAST_ACTIVITY']) && time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//echo nl2br(print_r($_SESSION, 1)); exit;

function login_html() {
   ?>

<div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>Enter LEH Login</h1><br>
				  <form name="login" method="POST" action="login.php?REQUEST_URI=<?php echo urlencode($_SERVER['REQUEST_URI']); ?>">
					<input type="text" name="username" placeholder="Username">
					<input type="password" name="password" placeholder="Password">
					<input type="submit" name="submit" value="Log In" class="login loginmodal-submit">
				  </form>
					
				  <div class="login-help">
					Having issues? Contact LPS Help Desk x7078
				  </div>
				</div>
			</div>


<script language="JavaScript">
<!--

document.login.username.focus();

//-->
</script>
<?php
   exit;

} 
// END login_html();


//BEGIN entry_html();?>
<?php function entry_html() {

if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
    $libertyEagle = "Eagle" ;
}elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
    $libertyEagle = "Liberty";
}elseif ($_SESSION['school_id'] != ""){
    $libertyEagle  = "Liberty/Eagle";
}else{
    $libertyEagle = "No School";
}
   ?>

<!-- begin content -->

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<?php 
?>
Welcome, <span style="font-weight: bold;"><?php echo $_SESSION['full_user_name']; ?></span>.<br/>
<br/>

Would you like to:<br/>
<ul style="list-style: disc;">
 <li><a href="?createNewIncident=yes" class="accentfeedback">Create A New Report</a></li>
 <li><a href="" class="accentfeedback">View Previously Submitted</a></li>
 
 <?php 
if ((isset($_SESSION['tr_iswebmaster']) && ($_SESSION['tr_iswebmaster'] == "yes")) 
  || (isset($_SESSION['tr_isdirector']) && ($_SESSION['tr_isdirector'] == "yes")) 
  || (isset($_SESSION['username']) && ($_SESSION['username'] == "treyk")) 
  || (isset($_SESSION['username']) && ($_SESSION['username'] == "nralph"))) { ?>
<span>Supervisor Options</span><br />
 <li><a href="?viewCurrentYear=yes" class="accentfeedback">Search for Report</a></li>
 <li><a href="?viewPreviousYear=yes" class="accentfeedback">View All Reports</a></li>
<?php }
?>
 <li><a href="logout.php" class="accentfeedback">Log Out</a></li>
</ul>
<br/>
<br/>

<?php

}

// BEGIN assignments();

function assignments(){
    if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
        $libertyEagle = "Eagle" ;
    }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
        $libertyEagle = "Liberty";
    }elseif ($_SESSION['school_id'] != ""){
        $libertyEagle  = "Liberty/Eagle";
    }else{
        $libertyEagle = "No School";
    }
    
    if (empty($_SESSION['LAST_ACTIVITY']))
    {
    $_SESSION['LAST_ACTIVITY'] = time();
    }
?>

<link rel='stylesheet' href='css/fullcalendar.css' />
<style>
#calendar {width: 100%;margin: 0 auto;}
.fc-header tbody tr {border:none;}
.fc-header td {
    border: none;
    background: none repeat scroll 0 0 #3E5990;
    color: #FFFFFF;
}
.fc-header {margin-bottom:0px;}
.fc-header-title h2 {margin:8px 0;}
</style>
<script src='lib/jquery.min.js'></script>
<script src='lib/moment.min.js'></script>
<script src='lib/moment-timezone.min.js'></script>
<script src='lib/moment-timezone-with-data.min.js'></script>
<script src='js/fullcalendar.js'></script>
<script src='js/app.js'></script>
<script>
    
    $(document).ready(function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        // page is now ready, initialize the calendar...

        $('#calendar').fullCalendar({
            // put your options and callbacks here
            header: {
                left:   '',
                center: 'title',
                right:  'prev,next'
            },
            editable: false,
            eventRender: function(event, element) {
                var text = '<p>Start: '+event.start+'<br/>End: '+event.details+'</p>';

                element.qtip({
                    content: {
                        title: event.title,
                        text: text
                    },
                    style: {
                        classes: 'qtip-rounded qtip-shadow'
                    },
                    position: {
                        my: 'top left',
                        at: 'bottom right'
                    }
                });
            }
        })

    });
</script>

<div id='calendar'></div>
    
<?php    
}

if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>
<!--Content-->

            <!--Login Check-->
<?php 
    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        //$_SESSION['tr_isprincipal'] == "yes" || $_SESSION['tr_isdirector'] == "yes" || $_SESSION['tr_iswebmaster'] == "yes") {
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
       //include("security.php");
       gen_header( "$libertyEagle Hour - Main Page");
       $constr = "dbname=".Common::DB_NAME." user=".Common::DB_USER." password=".Common::DB_USER_PASSWORD." host=".Common::HOST;
       $dbh = pg_connect($constr) or die("Unable to connect to database\n\n");

       if (Common::isStudent()) {
           header('Location: studentAssignments.php');
       }

       if (isset($_GET['viewCurrentYear']) && ($_GET['viewCurrentYear'] == "yes")) {
          entry_html();
          show_results_html($dbh, exec("date +%Y"));
       } elseif (isset($_POST['submit']) && ($_POST['submit'] == "View Previous Year")) {
          entry_html();
          show_results_html($dbh, $_POST);
       } elseif (isset($_GET['viewPreviousYear']) && ($_GET['viewPreviousYear'] == "yes")) {
          entry_html();
          selectYear();
       } elseif (isset($_GET['createNewIncident']) && ($_GET['createNewIncident'] == "yes")) {
          entry_html();
          request_html();
       } elseif (isset($_GET['edit']) && ($_GET['edit'] == "yes") && (isset($_GET['id']))) {
          entry_html();
          request_html($dbh, $_GET['id']);
       } else {
          entry_html();
          assignments();
       }

    } else {

       gen_header("");
       login_html();

    }
         
    gen_footer();
?>