$( function() {
    var absentObj = absent();
    absentObj.bindObjects();
});

function absent() {
    return new function() {
    	this.bindObjects = function() {
            var obj = this;
            
            obj.moveToDetention();
        },
        
        this.moveToDetention = function(){
            $('.move-to-detention-btn').on('click', function(){
                var assignmentId = $(this).data('assignmentId');
                var studentId = $(this).data('studentId');
                
                var promise = $.ajax({
                    url: 'php/updateAssignmentDetention.php',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        assignment_id : assignmentId,
                        student_id : studentId
                    },
                    beforeSend: function(){
                        showLoader();
                    }
                });

                promise.done(function(response, textStatus, jqXHR){
                    if(response.error == false) {
                        location.reload();
                    } else {
                        // alert(response.message);
                    }
                }).always(function() {
                    hideLoader();
                });

                return false;
            });
        }
    };
}
