<?php

session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_ASSIGNMENTS_LIST);

// This is the previous version of session timeout--changed back to test
if (time() - $_SESSION['LAST_ACTIVITY'] < 900)
{
    $_SESSION['LAST_ACTIVITY'] = time();
}

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900))
{    
    $_SESSION = array();

    if (ini_get("session.use_cookies"))
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_unset();
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

//echo nl2br(print_r($_SESSION, 1)); exit;


//BEGIN entry_html();
?>
<?php function entry_html($title ,$dbh) { ?>

<!-- begin content -->

<?php
if (empty($_SESSION['LAST_ACTIVITY']))
{
    $_SESSION['LAST_ACTIVITY'] = time();
}
?>

<!-- Content here -->
<script>
    $(document).ready(function(){
    
        //  select date for assignment list
        $('.from , .to').datepicker({
                            format: "mm-dd-yyyy",
                            todayHighlight: true
                        }).datepicker('setDate', [new Date()]); 

        <?php
            if( isset( $_GET['fromDate'] )) echo " $('.from').datepicker().val('".$_GET['fromDate']."');";  
            if( isset( $_GET['toDate'] )) echo " $('.to').datepicker().val('".$_GET['toDate']."');";  
        ?>
        // end of date function 
     })
</script>

<div style="margin-top: 20px;">
    <h4><b> Assigment List</b></h4>
    <hr/>
    <div class="form-group ">
        <form>

            <div class="col-sm-2">
                From:  
                <input type="text"  name="fromDate" class="form-control from" readonly
                   value="<?php echo isset($_GET['fromDate'])?$_GET['fromDate']:""; ?>" >
            </div>

            <div class="col-sm-2">                    
                To:     
                <input type="text" name="toDate" class="form-control to" readonly
                 value="<?php echo isset($_GET['toDate'])?$_GET['toDate']:""; ?>" >
            </div>

            <div class="col-sm-3">
                Student Name:     
                <input type="text" 
                    placeholder="Student Name" 
                    name="sname" 
                    class="form-control"
                    value="<?php echo isset($_GET['sname'])?$_GET['sname']:""; ?>" >
            </div>

            <div class="col-sm-3">
                <?php if(! Common::isTeacher()) { ?>
                    Teacher Name :    
                    <input type="text" 
                        name="tname"  
                        placeholder="Teacher Name"  
                        class="form-control" 
                        value="<?php echo isset($_GET['tname'])?$_GET['tname']:""; ?>" >
                <?php } ?>
            </div>

            <div class="col-sm-2">
                <div class="pull-right">
                    <input  type="submit" id="assignmentSearch" class="btn btn-primary" value="Search"
                      style="margin-top:20px"  >
                    
                    <a class="btn btn-default" href="assignment-list.php"  style="margin-top:20px"  >  Reset </a>
                </div>
            </div>
        </form> <!-- End of FIlter Form -->  
    </div>

    <br/><br/><br/> 
    
    <table class="table table-bordered" style="margin-top: 20px;">
        <colgroup>
            <col style="width: 20%;" />
            <col style="width: 15%;" />
            <col style="width: 15%;" />
            <col style="width: 15%;" />
            <col style="width: 20%;" />
            <col style="width: 15%;" />
        </colgroup>
        <thead>
            <tr>
                <th>Date</th>
                <th>Student</th>
                <th>Teacher</th>
                <th>Note</th>
                <th>Assigment Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="showAssignmentList">


    <?php 
        $linkData="";
        $num_page=10;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
        $start_from = ($page-1) * $num_page; 

        // create object and call  make query and assignment list 
        $objAssignment = new Assignment();
        if((isset($_REQUEST['fromDate'] ) && isset($_REQUEST['toDate'])) 
            || isset($_REQUEST['sname'] ) 
            || isset($_REQUEST['tname'] ) )
        {
            $tName = isset($_REQUEST['tname']) ? $_REQUEST['tname'] : '';

            $fromDateArr = explode('-', $_REQUEST['fromDate']);
            $toDateArr = explode('-', $_REQUEST['toDate']);
            
            $fromDate = $fromDateArr[1] . '-' . $fromDateArr[0] . '-' . $fromDateArr[2];
            $toDate = $toDateArr[1] . '-' . $toDateArr[0] . '-' . $toDateArr[2];
            
            $query= $objAssignment->makeQuery($fromDate, $toDate,
            $_REQUEST['sname'],$tName);

            //create link for pagination
            $linkData="&fromDate=".$_REQUEST['fromDate'];
            $linkData.="&toDate=".$_REQUEST['toDate'];
            $linkData.="&sname=".$_REQUEST['sname'];
            $linkData.="&tname=".$tName;
        }
        else
        {
            $query = $objAssignment->makeQuery();
        }
                
        $assigns = $objAssignment->getAssignmentListPagination($query, $num_page,$start_from);       

        if (count($assigns)>0) {
            foreach ((array)$assigns as $key => $assign) {
                $id = $assign['id'];
                $date = $assign['date'];
                $attendence = $assign['absent'];
                  // invoke static function of class
                $assignType = Common::assignmentType($assign['assignment_type']);
                $note = empty($assign['note'])?"N/A": $assign['note']; 


               // get student ID and teacher ID to find student and teacher name  
                $stdName = $assign['student_name'];
                $techName =$assign['teacher_name'];
                $student_id = $assign['student_id'];
                $assignDate = $assign['date'];
                $currentDate = date("Y-m-d");
       
    ?>

            <tr>
                <td><?php echo date("m-d-Y", strtotime($date) ); ?></td>
                <td><?php echo $stdName; ?></td>
                <td><?php echo $techName; ?></td>
                <td><?php echo $note; ?></td>
                <td> 
                    <?php if ($assign['group_id']) {
                            echo "Club Assigment";
                        } else if ($assign['unassigned'] == 't') {
                            echo "Requested";
                        } else {
                            echo "Assigment - " . $assignType; 
                        }
                    ?>
                </td>
                <td class="text-center"> 
                    <?php   
                         
                    if(empty($attendence))
                    {
                        if( strtotime($assignDate) <= strtotime($currentDate) && $assign['unassigned'] != 't')
                        {
                            echo " <button 
                             data-aid='{$id}'
                             data-tname='{$techName}'
                             data-adate='{$assignDate}'
                             data-sid = '{$student_id}'
                             class='presentButton btn btn-sm btn-success' > Present </button>  " ;       

                            echo " <button 
                             data-aid='{$id}'
                             data-tname='{$techName}'
                             data-adate='{$assignDate}'
                             data-sid = '{$student_id}'
                             class=' absentButton btn btn-sm btn-danger' > Absent </button>  " ;       

                        } elseif( strtotime($assignDate) >= strtotime($currentDate) 
                            && $assign['unassigned'] == 't'
                            && $assign['teacher_id'] == $_SESSION['user_id'])
                        {
                            echo " <button 
                             data-aid='{$id}'
                             data-adate='{$assignDate}'
                             data-sid = '{$student_id}'
                             class='allowAssignmentRequestButton btn btn-sm btn-success' > Allow </button>  " ;       

                            echo " <button 
                             data-aid='{$id}'
                             data-adate='{$assignDate}'
                             data-sid = '{$student_id}'
                             class=' denyAssignmentRequestButton btn btn-sm btn-danger' > Deny </button>  " ;       

                        } else {
                            echo "------";
                        }
                    }
                    else if( $attendence=='t')
                    {
                        echo " <button 
                             class=' disabled  btn btn-sm btn-danger' > Absent </button>  " ;       
                    }
                   else
                   {
                        echo " <button 
                             class=' disabled  btn btn-sm btn-success' > Present </button>  " ;          
                   }
                    
                    ?>  
                </td>
            </tr>
<?php


            } // end of foreach
        } // end of if
        else
        {
             echo "<tr><td colspan='6' class='text-center'>No Record Found</td></tr>";
        }
?>
 
        </tbody>
    </table>
</div>


<div class="text-center">
    <ul class="pagination" id="paginationID">
        <?php 
            $paginationquery = $query;
            $res = pg_query($dbh, $paginationquery);
            if (!$res) { echo "An error occurred.\n"; exit; }
            $total_pages = pg_num_rows($res); 

            if($total_pages>10) { 
                //$total_pages = count($assigns); 
                $totalPaginationNumber =ceil($total_pages/$num_page);
                $active="";

                $prev = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

                $disablePrev='';
                if( $prev==1) {
                    $disablePrev='class="btn disabled"';
                }

                $currentPageNumber = isset($_REQUEST['page'])?$_REQUEST['page']:1;

                $showPageNumber= array(1 ,$totalPaginationNumber);
                $showDots= array();

                if($totalPaginationNumber > 10) {
                    $counter=1;
                    for($j=-2 ; $j<=2; $j++){
                        if($currentPageNumber <= 3)
                        {
                            array_push($showPageNumber, $counter++); // 1 2 3 4 5
                            array_push($showDots, $totalPaginationNumber-1);  // 12345....15             
                        }
                        else if($currentPageNumber>3 && $currentPageNumber <= $totalPaginationNumber-5)
                        {
                            array_push($showDots, 2 , $totalPaginationNumber-1);  // 1...5...15
                            array_push($showPageNumber, $currentPageNumber+$j); // 6 7 [8] 9 10                
                        }
                        else 
                        {
                            array_push($showDots, 2);  // 12345....15                             
                            array_push($showPageNumber, $totalPaginationNumber - ($counter++)); // 1 2 3 4 5                
                        }
                    }
                }
                else {
                    for($j=1 ; $j<=$totalPaginationNumber; $j++){  
                            array_push($showPageNumber, $j); // 1 2 3 4 5          
                    }      
                }

                echo "<li><a $disablePrev href='assignment-list.php?page=".($prev-1).$linkData."'> &lt; </a> </li>"; 


                for ($i=1; $i<= $totalPaginationNumber; $i++) { 
                    $active='';
                    $disable='';

                    if((isset($_REQUEST['page']) && $_REQUEST['page']==$i) || 
                        (! isset($_REQUEST['page'])&& $i==1))
                    { 
                      $active = "active";  
                      $disable= "class='btn disabled'";
                    }

                    if (in_array($i, $showPageNumber)){
                       echo "<li  class='{$active}' ><a {$disable} href='assignment-list.php?page=".$i.$linkData."'>".
                          $i."</a> </li>"; 
                    }
                    else if( in_array($i, $showDots)){
                        echo "<li><a  href='#'>.....</a> </li>"; 
                    }

                } // end of for loop 

                if(!isset($_REQUEST['page']) ){  $_REQUEST['page']=1; }

                $next = isset($_REQUEST['page']) ? $_REQUEST['page']:1;

                $disableNext='';
                if( $prev==$totalPaginationNumber) {
                    $disableNext='class="btn disabled"';
                }
    
                echo "<li><a $disableNext href='assignment-list.php?page=".($prev+1).$linkData."'> &gt; </a> </li>"; 
            }           
        ?>    
    </ul> 
</div>


<?php
} //END entry_html();
?>

<!--Content-->

<!--Login Check-->
<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") {
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Assignment List";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include('footer.php');
?>