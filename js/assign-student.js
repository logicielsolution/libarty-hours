$( function() {
    if ($('.assign-student-page').length < 1) {
        return false;
    }
    
    var studentAssignObj = studentAssignments();
    studentAssignObj.bindObjects();
});

function studentAssignments() {
    return new function() {
        this.$searchStudentInput    = $( "#search-student" ),
        this.$searchTeacherInput    = $( "#search-teacher" ),
        this.$searchTeacherForm     = this.$searchTeacherInput.closest('form'),
        this.$saveAssignmentForm    = $('#saveAssignmentForm'),
        this.$fullCalenderObj       = $('#full-calendar'),
        this.$saveAssignmentButton  = $('.submit-assignment-btn'), 
        this.getStudentRequestPromise = null;
        this.blackoutDays             = [];
        this.blackoutDaysData         = [];
        this.selectedStudentSchoolId  = null;
        this.bindObjects = function() {
            var obj = this;
            obj.filterStudentsList();
            obj.saveAssignment();
            obj.blackoutDays = obj.getblackoutDays();
        },
        this.filterStudentsList = function() {
            var obj = this;
            if (obj.$searchStudentInput.length < 1) {
                return false;
            }
            obj.$searchStudentInput.autocomplete({
                source: function( request, response ) {

                    if(obj.getStudentRequestPromise) {
                        obj.getStudentRequestPromise.abort();
                    }
                    obj.getStudentRequestPromise = $.ajax({
                        url: "php/getStudents.php",
                        dataType: "json",
                        data: {
                            name: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });

                    obj.getStudentRequestPromise.fail(function(jqXHR, textStatus) {
                        alert("Error: " + textStatus);
                    });
                },
                minLength: 2,
                select: function( event, ui ) {
                    console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );
                    setTimeout(function() {
                        $( "#search-student" ).val(ui.item.full_name);
                        $( "#search-student" ).attr("data-student-id", ui.item.id);
                    }, 1);

                    obj.selectedStudentSchoolId = ui.item.building_id;

                    var selectedStudent = {
                        id: ui.item.id,
                        name: ui.item.full_name,
                    }

                    $(".student-calendar").show();
                    $(".selected-student-id").val(ui.item.id);
                    $(".selected-teacher-id").val('');
                    $(".selected-student-name").text(ui.item.full_name);
                    obj.fullCalenderObj(selectedStudent);

                    // var currentDate = moment().format('YYYY-MM-DD');
                    var date = obj.$fullCalenderObj.fullCalendar('getDate');
                    var startDateOfMonth = moment(date).format("YYYY-MM-01")

                    obj.getAssignmentsRequest(startDateOfMonth, selectedStudent.id, null, true);
                },
                focus: function( event, ui) {
                    $( "#search-student" ).val(ui.item.full_name);
                    // $(this).data("autocomplete").search($(this).val());
                    event.preventDefault();
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li>" ).data("item.autocomplete", item)
                .append( item.full_name )
                .appendTo( ul );
            };
        },
        this.saveAssignment = function() {
            var obj = this;
            obj.$saveAssignmentForm.unbind();
            obj.$saveAssignmentForm.on('submit', function() {
                var $this = $(this);
         
                obj.sendSaveAssignmentRequest($this);
                return false;
            });
        },
        this.sendSaveAssignmentRequest = function($form) {
            var obj = this;
            var events = obj.$fullCalenderObj.fullCalendar('clientEvents');
            // var events = $('#full-calendar').fullCalendar( 'getEventSources' );
            // events = events[0].events;

            var startDate = moment(events[0].start).format();
            var selectedDatesForAssignment = [];
            
            $form.find('.selected-dates').remove(); //remove hidden inputs
            var studentId = $('.selected-student-id').val();
            for (var i = 0; i < events.length; i++) {

                if (events[i].color == 'green') {
                    events[i].start = moment(events[i].start).format();
                    events[i].student_id = studentId;
                    // selectedDatesForAssignment.push(moment(events[i].start).format());
                    events[i].isSaved = true;
                    selectedDatesForAssignment.push(events[i]);
                    
                    //set hidden input values for set selected event date
                    $form.append( "<input type='hidden' class='submit-assignment-hidden assignment-type' name='assignments[" + i + "][assignment_type]' value='" + events[i].assignment_type + "' >" );
                    $form.append( "<input type='hidden' class='submit-assignment-hidden selected-dates' name='assignments[" + i + "][date]' value='" + moment(events[i].start).format('YYYY-MM-DD') + "' >" );
                    $form.append( "<input type='hidden' class='submit-assignment-hidden selected-student' name='assignments[" + i + "][student_id]' value='" + events[i].student_id + "' >" );
                    $form.append( "<input type='hidden' class='submit-assignment-hidden selected-teacher' name='assignments[" + i + "][teacher_id]' value='" + events[i].teacher_id + "' >" );
                    $form.append( "<input type='hidden' class='submit-assignment-hidden hidden-notes' name='assignments[" + i + "][note]' value='" + events[i].note + "' >" );
                }
            }
            var url = $form.attr('action');
            var params = $form.serialize();
            
            var saveAssignmentPromise = $.ajax({
                url: url,
                dataType: "json",
                type: 'POST',
                data: params
            });

            saveAssignmentPromise.done(function(response, textStatus, jqXHR) {
            
                // if assignments saved then reload calendar data.
                 if(response.error == false) {
                    var studentId = $form.find('.selected-student-id').val();
                    
                    var date = obj.$fullCalenderObj.fullCalendar('getDate');
                    var startDateOfMonth = moment(date).format("YYYY-MM-01")

                    //remove hidden inputs
                    $form.find('.submit-assignment-hidden').remove();

                    showFlashMessage(response.message_type, response.message);

                    obj.getAssignmentsRequest(startDateOfMonth, studentId, null, true);
                 } else {
                    showFlashMessage(response.message_type, response.message);
                 }

            })
            
            return false;
        },
        this.fullCalenderObj = function(sStudent) {
            var obj = this;
            obj.nextMonthClick();
            obj.prevMonthClick();
            obj.$fullCalenderObj.fullCalendar({
                // put your options and callbacks here
                header: {
                    left:   '',
                    center: 'title',
                    right:  'prev,next'
                },
                editable: false,
                eventRender: function(event, element) {

                    element.find('.fc-title').append("<br/>" + ((event.teacher.length > 23) ? event.teacher.substring(0,23) + '...' : event.teacher));
                    
                    var isGroupAssignment = false;
                    var isRequestedAssignment = false;

                    if(event.group_id) {
                        isGroupAssignment = true;
                    }

                    if (event.assignment_unassigned && event.assignment_unassigned == 't') {
                        isRequestedAssignment = true;
                    }

                    var note = event.note;
                    if(note.length > 15) {
                        note = event.note.substring(0,15) + '...';
                    };
                    

                    if (!isGroupAssignment) {

                        var noteString = '';
                        if(event.note) {
                            noteString = 'NOTE: ' + note;
                        }
                        element.find('.fc-title').append("<br/><span class='note-in-event'>" + noteString + "</span>");
                        
                        if(event.note) {
                            // show full note in toottip
                            element.tooltip({
                                title: 'Note: ' + event.note,
                                placement: 'bottom'
                            });
                        }
                    } else {
                        element.find('.fc-title').append("<br/><span class='note-in-event'>Club Note: " + note + "</span>");
                        
                        // show full note in toottip
                        element.tooltip({
                            title: 'Club Note: ' + event.note,
                            placement: 'bottom'
                        });
                    }

                    // get difference between current date and event date.
                    // var isFutureDates = (event.start.diff(moment().format('YYYY-MM-DD HH:mm:ss'), 'days') <= 0) ? false : true;
                    var currentDate = moment().format('YYYY-MM-DD');
                    var isFutureDates = (currentDate >= moment(event.start).format('YYYY-MM-DD')) ? false : true;

                    //If the event is on a Saturday and Sunday, don't render it.
                    if(!isWorkingDays(event.start) || obj.isBlackoutDay(event._start._i, obj.blackoutDays))
                    {
                        return false;
                    }

                    // add icons
                    if (event.isSaved && USER.type != 'teacher' && isFutureDates && !isGroupAssignment && !isRequestedAssignment) {
                        console.log('dfs sdf');
                        event.color = '';
                        obj.addRemoveIcon(event, element);
                        obj.addEditNoteIcon(event, element);
                    } else if (event.isSaved && USER.type == 'teacher' && isFutureDates && !isGroupAssignment && !isRequestedAssignment) {
                        event.color = '';
                        if (event.teacher_id == USER.id) {
                            obj.addRemoveIcon(event, element);
                            obj.addEditNoteIcon(event, element);
                        }
                    } else if (!event.isSaved && isFutureDates && !isGroupAssignment) {
                            obj.addEditNoteIcon(event, element);
                    } else if (!isFutureDates && event.teacher_id == USER.id && !event.absent && !isRequestedAssignment) {
                        obj.addAttendanceIcon(event, element);
                    }

                    if(isGroupAssignment) {
                        event.color = "#41bef4";
                    }
                    
                    if(!isFutureDates) {
                        event.color = "#f49241";
                    }

                    if (isRequestedAssignment) {
                        event.color = "grey";
                    }

                    if (event.absent == 't') {
                        event.color = '#e57373';
                    } else if (event.absent == 'f') {
                        event.color = '#81c784';

                    }

                },
                dayClick: function(date, jsEvent, view) {
                    var currentDate = moment().format('YYYY-MM-DD');
                    var selectedDate = date.format();

                    //get day event
                    var dayEvents = obj.getDayEvents(date);

                    if (isWorkingDays(date) 
                        && selectedDate > currentDate 
                        && !IsDateHasSelectedEvent(date) 
                        && !obj.isBlackoutDay(selectedDate, obj.blackoutDays)
                        && !isDateHasSelectedMultipleEvent(dayEvents)) {
                        
                        $('#teacherSelectPopForm').find(':submit').attr('disabled', 'disabled');
                        if (typeof USER != 'undefined' && USER.type == 'teacher') {
                            /*if (!IsDateHasEvent(date)) {
                                obj.getAssignmentsRequest(moment(date).format(), sStudent.id, USER.id, null, 'Edit note');
                            }*/
                            obj.selectTeacherModal(date.format('DD/MM/YYYY'), dayEvents, USER.type); //open modal for select teacher
                        } else if (typeof USER_TYPE == 'undefined' || USER.type == 'super_admin' || USER.type == 'admin') {
                            //disable teacher popup add button
                            
                            obj.selectTeacherModal(date.format('DD/MM/YYYY'), dayEvents); //open modal for select teacher
                        }
                    };
                },
                eventClick: function(event, jsEvent, view ) {
                    var currentDate = moment().format('YYYY-MM-DD');
                    
                    if (!event.isSaved) {
                        event.color = (event.color == 'red') ? 'green' : 'red';
                        $('#full-calendar').fullCalendar( 'rerenderEvents' );
                    }


                    if (event.assignment_unassigned 
                        && event.assignment_unassigned == 't'
                        && event.teacher_id == USER.id
                        && moment(event.start).format('YYYY-MM-DD') >= currentDate) {
                        obj.openAllowDenyAssignmentPopup(event);
                    }

                    //enable disable submit assignment button
                    obj.enableDisableAssignmentSubmitButton();
                },
                dayRender: function (date, cell) {
                    if (obj.isBlackoutDay(moment(date._d).format('YYYY-MM-DD'), obj.blackoutDays)) {

                        var heading;
                        for (var i = 0; i < obj.blackoutDaysData.length; i++) {
                            if (moment(date).format() == obj.blackoutDaysData[i].date
                                && obj.blackoutDaysData[i].building_id == obj.selectedStudentSchoolId) {
                                
                                var blackoutDayName = obj.blackoutDaysData[i].holyday;
                                if( blackoutDayName.length > 100) {
                                    blackoutDayName = obj.blackoutDaysData[i].holyday.substring(0,100) + '...';
                                };
                                heading = "<div class='holidayTooltip'><span style='cursor: pointer;'>" + blackoutDayName + "</span></div>"
                                
                                cell.css({"background-color" : "#efefef", 
                                    "padding" : "15px", 
                                    "text-align" : "center",
                                    "vertical-align" : "middle",
                                    "z-index" : "9",
                                    "position": "relative"
                                });
                                
                                cell.append(heading);

                                // show full holiday name
                                if(obj.blackoutDaysData[i].holyday.length > 0) {
                                    cell.find('.holidayTooltip').tooltip({
                                        title: 'Holiday: ' + obj.blackoutDaysData[i].holyday,
                                        placement: 'bottom'
                                    });
                                }
                            }
                        }
                    }
                }/*,
                viewRender: function(view, element) {
                    obj.getblackoutDays();
                }*/
            });
        },
        this.openAllowDenyAssignmentPopup = function(event){
            var obj = this;
            $('#allowDenyAssignmentRequestModal').modal();

            var studentName = $('#search-student').val();

            $('#requested-assignment-id').val(event.assignment_id);
            $('.requestl-assignment-note').text(event.note);
            $('.requestl-assignment-student-name').text(studentName);

            // submit form
            $('.assignment-request-submit-button').off('click');
            $('.assignment-request-submit-button').on('click', function(e){
                e.preventDefault();
                
                $('#allowDenyAssignmentRequestModal').modal('hide');

                var submitType = $(this).val();
                $allowDenyAssingnmentForm = $('#allowDenyAssignmentRequestPopupForm');
                var selectedStudentId = obj.$saveAssignmentForm.find('.selected-student-id').val();        
                var eventDate = moment(event.event_date).format('DD-MM-YYYY');

                var url = $allowDenyAssingnmentForm.attr('action');
                var params = $allowDenyAssingnmentForm.serialize();
                params += "&submit_type=" + submitType + "&student_id=" + selectedStudentId + "&date=" + eventDate;                
                
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    data: params,
                    beforeSend: function() {
                        showLoader();
                    }
                })
                .done(function(response, textStatus, jqXHR) {
                    
                    if (response.error == false) {

                        var date = obj.$fullCalenderObj.fullCalendar('getDate');
                        var startDateOfMonth = moment(date).format("YYYY-MM-01")
                        obj.getAssignmentsRequest(startDateOfMonth, selectedStudentId, null, true);
                    
                    } else {
                        alert(response.message);
                    }

                })
                .always(function() {
                    hideLoader();
                });

                    return false;   
                });
        },
        this.nextMonthClick = function() {
            var obj = this;
            var $calenderObj = obj.$fullCalenderObj;

            $('#full-calendar').off('click', 'button.fc-next-button');
            $('#full-calendar').on('click', 'button.fc-next-button', function() {
                var date = $calenderObj.fullCalendar('getDate');
                var studentId = $('.selected-student-id').val();
                var formatedDate = moment(date).format('YYYY-MM-DD');
                obj.getAssignmentsRequest(formatedDate, studentId, null, true);
            });
        },
        this.prevMonthClick = function() {
            var obj = this;
            var $calenderObj = obj.$fullCalenderObj;

            $('#full-calendar').off('click', 'button.fc-prev-button');
            $('#full-calendar').on('click', 'button.fc-prev-button', function() {
                var date = $calenderObj.fullCalendar('getDate');
                var studentId = $('.selected-student-id').val();
                var formatedDate = moment(date).format('YYYY-MM-DD');
                obj.getAssignmentsRequest(formatedDate, studentId, null, true);
            });
        },
        this.selectTeacherModal = function(sDate, dayEvent, userType=null) {
            var obj = this;
            var sStudent = {
                id: $('.selected-student-id').val(),
                name: $('.selected-student-name').text()
            };
            $('#selectTeacherModal').modal(); //open select teacher popup
            $('#selected-date').text(sDate); //set date in modal
            $('#student-name').text(sStudent.name); //set date in modal
            
            // radio buttons setting
            if (dayEvent.length > 0 && dayEvent[0].group_id == null && dayEvent[0].assignment_unassigned == null) {
                $(".assignment-type[type=radio][value='full-day']").attr("disabled",true);
                $(".assignment-type[type=radio][value=" + dayEvent[0].assignment_type + "]").attr("disabled",true);
                $(".assignment-type[type=radio]").prop("checked",false);
                
                if (dayEvent[0].assignment_type == GlobalObj.FIRST_HALF_ASSIGNMENT) {
                    $(".assignment-type[type=radio][value=" + GlobalObj.SECOND_HALF_ASSIGNMENT + "]").attr("disabled",false);
                    $(".assignment-type[type=radio][value=" + GlobalObj.SECOND_HALF_ASSIGNMENT + "]").prop("checked",true);
                } else if (dayEvent[0].assignment_type == GlobalObj.SECOND_HALF_ASSIGNMENT) {
                    $(".assignment-type[type=radio][value=" + GlobalObj.FIRST_HALF_ASSIGNMENT + "]").attr("disabled",false);
                    $(".assignment-type[type=radio][value=" + GlobalObj.FIRST_HALF_ASSIGNMENT + "]").prop("checked",true);
                }
            } else {
                $(".assignment-type[type=radio]").attr("disabled",false);
                $(".assignment-type[type=radio][value=" + GlobalObj.FULL_DAY_ASSIGNMENT + "]").prop("checked",true);

            }
            // end radio buttons setting
            
            if (userType == 'teacher') {
                //enable teacher popup add button
                $('#teacherSelectPopForm').find(':submit').removeAttr('disabled');
                
                obj.getStudentAssignments(sStudent.id, sDate, USER.id);
                /*setTimeout(function() {
                }, 1);*/                

            } else {
                obj.getTeachersListing(sStudent, sDate);
            }
        }
        // get the list of all the filtered teachers and select teacher to get assignments.
        this.getTeachersListing = function(sStudent, sDate) {

            var obj = this;
            obj.$searchTeacherInput.autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "php/getTeachers.php",
                        dataType: "json",
                        data: {
                            name: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) {
                    console.log( "Selected: " + ui.item.full_name + " aka " + ui.item.id );
                    
                    //enable teacher popup add button
                    $('#teacherSelectPopForm').find(':submit').removeAttr('disabled');

                    var selectedTeacher = {
                        id: ui.item.id,
                        name: ui.item.full_name
                    };

                    $(".selected-teacher-id").val(ui.item.id);

                    setTimeout(function() {
                        obj.$searchTeacherInput.val(ui.item.full_name);
                        obj.getStudentAssignments(sStudent.id, sDate, selectedTeacher.id);
                    }, 1);

                },

                focus: function( event, ui) {
                    obj.$searchTeacherInput.val(ui.item.full_name);
                    event.preventDefault();
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                ul.css('z-index', 1100);
                return $( "<li>" ).data("item.autocomplete", item)
                .append( item.full_name )
                .appendTo( ul );
            };
        },
        this.getStudentAssignments = function(sStudentId, sDate, sTeacherId) {
            var obj = this;
            // var $searchTeacherForm = obj.$searchTeacherForm;
            var $searchTeacherForm = $('#teacherSelectPopForm');
            
            $searchTeacherForm.unbind();
            $searchTeacherForm.on('submit', function() {
                var $this = $(this);
                var url = $this.attr('action');
                var note = $('#assignment-note').val();
                var assignmentType = $('.assignment-type:checked').val();
                var fromDate = moment(sDate, 'DD/MM/YYYY').format('YYYY-MM-DD');

                obj.getAssignmentsRequest(fromDate, sStudentId, sTeacherId, null, note, assignmentType);

                return false;   
            });
        },
        this.deleteSingleEvent = function(assignment_id, calenderEvent){
            var obj = this;
            var $promise = $.ajax({
                url: 'php/deleteAssignment.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: assignment_id
                }
            });

            $promise.done(function(response, textStatus, jqXHR) {
                if(response.error == true) {
                    alert(response.message);
                } else {

                    // if celender event not empty then remove event from calender.
                    if(calenderEvent) {
                        $('#full-calendar').fullCalendar('removeEvents',calenderEvent._id);

                        var selectedStudentId = obj.$saveAssignmentForm.find('.selected-student-id').val();
                        var date = obj.$fullCalenderObj.fullCalendar('getDate');
                        var startDateOfMonth = moment(date).format("YYYY-MM-01")
                        obj.getAssignmentsRequest(startDateOfMonth, selectedStudentId, null, true);
                    }
                }
            });
        },
        this.getAssignmentsRequest = function(sDate, sStudentId, sTeacherId, isSaved, note, assignmentType) {
            var obj = this;
            setTimeout(function() {

                // obj.sendRequest(sDate, sStudentId, sTeacherId, isSaved);
                $.ajax({
                    url: 'php/getStudentAssignments.php',
                    dataType: 'json',
                    type: 'get',
                    data: {
                        'date'              : sDate,
                        'student_id'        : sStudentId,
                        'teacher_id'        : sTeacherId,
                        'isSaved'           : isSaved,
                        'note'              : note,
                        'assignment_type'   : assignmentType 
                    },
                    beforeSend: function() {
                        showLoader();
                    }
                })
                .done(function(response, textStatus, jqXHR) {
                    var assignments = [];
                    var existAssignments = obj.$fullCalenderObj.fullCalendar('clientEvents');
                    assignments = obj.filterAssignments(response, existAssignments);

                    if (assignments.length < 1 || (isSaved == true)) {
                        assignments = response;   
                    }
                    
                    destroySelectModel();
                    obj.$fullCalenderObj.fullCalendar('removeEvents');
                    obj.$fullCalenderObj.fullCalendar('addEventSource', assignments);
                    obj.$fullCalenderObj.fullCalendar('rerenderEvents' );
                    
                    //both prev next used for day renders
                    obj.$fullCalenderObj.fullCalendar('prev');
                    obj.$fullCalenderObj.fullCalendar('next');

                })
                .always(function() {
                    hideLoader();
                });
            }, 1000);
        },
        this.filterAssignments = function(requestedAssignments, existingAssignments) {
            var obj = this;
            var unselectedEvent = [];
            
            obj.$saveAssignmentButton.attr('disabled', 'disabled');
            for (var i = 0; i < requestedAssignments.length; i++) {
                if (requestedAssignments[i].color == 'red' 
                    || requestedAssignments[i].color == 'green') {
                    unselectedEvent.push(requestedAssignments[i]);
                }

                //enable disable submit assignment button
                if (requestedAssignments[i].color == 'green') {
                    obj.$saveAssignmentButton.removeAttr('disabled');
                }
            }

            // select uniqe unselected obj
            var unselectedEventNewObj = {};
            for ( var i=0; i < unselectedEvent.length; i++ ) {
                unselectedEventNewObj[unselectedEvent[i].event_date] = unselectedEvent[i];
            }
            unselectedEvent = new Array();
            for ( var key in unselectedEventNewObj ) {
                unselectedEvent.push(unselectedEventNewObj[key]);
            }

            var existSelectedEvent = [];
            var existUnselectedEvent = [];
            var newAssignmentArray = [];
            for (var i = 0; i < existingAssignments.length; i++) {

                existingAssignments[i].start = moment(existingAssignments[i].start).format();
                if (existingAssignments[i].color == 'green' 
                    || existingAssignments[i].color == '' 
                    || existingAssignments[i].color == '#41bef4') {
                    existSelectedEvent.push(existingAssignments[i]);
                } else {
                    existUnselectedEvent.push(existingAssignments[i]);
                }
            }

            for (var i = 0; i < existSelectedEvent.length; i++) {
                newAssignmentArray.push(existSelectedEvent[i]);
            }
            
            for (var i = 0; i < unselectedEvent.length; i++) {
                if(!obj.isAssignmentAlreadyExists(newAssignmentArray, unselectedEvent[i])) {
                    newAssignmentArray.push(unselectedEvent[i]);
                }
            }

            for (var i = 0; i < existUnselectedEvent.length; i++) {
                if(!obj.isAssignmentAlreadyExists(newAssignmentArray, existUnselectedEvent[i])) {
                    newAssignmentArray.push(existUnselectedEvent[i]);
                }
            }

            return newAssignmentArray;
        },
        this.isAssignmentAlreadyExists = function(assignmentsObj, existAssignmentObj) {
            var isAlreadyExists = false;
            for (var i = 0; i < assignmentsObj.length; i++) {
                if(assignmentsObj[i].event_date == existAssignmentObj.event_date) {
                    if ((!assignmentsObj[i].allDay && existAssignmentObj.assignment_type == GlobalObj.FULL_DAY_ASSIGNMENT)
                        || (assignmentsObj[i].allDay || assignmentsObj[i].assignment_type == existAssignmentObj.assignment_type)
                        && assignmentsObj[i].group_id == null) {
                        isAlreadyExists = true;
                    }
                }
            }

            return isAlreadyExists;
        },
        this.enableDisableAssignmentSubmitButton = function() {
            var obj = this;
            var existAssignments = obj.$fullCalenderObj.fullCalendar('clientEvents');

            obj.$saveAssignmentButton.attr('disabled', 'disabled');
            for (var i = 0; i < existAssignments.length; i++) {
                if (existAssignments[i].color == 'green') {
                    obj.$saveAssignmentButton.removeAttr('disabled');
                }
            }
        },
        this.getblackoutDays = function() {
            var obj = this;
            
            $.ajax({
                url: 'php/getBlackoutDays.php',
                dataType: 'json',
                type: 'get',
                beforeSend: function() {
                    showLoader();
                }
            })
            .done(function(response, textStatus, jqXHR) {
                obj.blackoutDaysData = response;
                obj.blackoutDays = [];
                for (var i = 0; i < response.length; i++) {
                    obj.blackoutDays.push(response[i].date);
                }

                //use for array uniqe
                obj.blackoutDays = obj.blackoutDays.filter(function(el, index, arr) {
                    return index === arr.indexOf(el);
                });
            })
            .always(function() {
                hideLoader();
            });
        },
        this.addRemoveIcon = function(event, element) {
            var obj = this;
            
            element.find('.fc-content').append( "<span class='closeon glyphicon glyphicon-remove'></span>" );
            element.find(".closeon").click(function() {
                if (confirm("Do you want to delete this event?")) {
                    obj.deleteSingleEvent(event.assignment_id, event);
                }
                return false;
            });
        },
        this.addEditNoteIcon = function(event, element) {
            var obj = this;

            if (event.color == 'green' || event.color == '') {
                element.find('.note-in-event').append("<span class='glyphicon glyphicon-pencil edit-note " +event.color+ " '></span>")                        
                element.find(".edit-note").click(function(eve) {
                    eve.stopPropagation(); //for stop parent click
                    obj.updateNote(event);
                });
            }
        },
        this.addAttendanceIcon = function(event, element) {
            var obj = this;
            
            element.find('.fc-content').append( "<span class='assignment-attendance glyphicon glyphicon-check'></span>" );
            element.find(".assignment-attendance").click(function() {
                obj.openAssignmentAttendanceModal(event);
            });
        }
        this.updateNote = function(event) {
            var obj = this;
            var selectedStudentId = obj.$saveAssignmentForm.find('.selected-student-id').val();

            $('#updateNoteModal').modal();
            $('#update-note').val(event.note);

            $('#updateNoteForm').off('submit');
            $('#updateNoteForm').on('submit', function() {
                var $this = $(this);

                if (!event.isSaved) {
                    var allEvents = [];
                    allEvents = $('#full-calendar').fullCalendar('clientEvents');

                    for (var i = 0; i < allEvents.length; i++) {
                        if (allEvents[i]._id == event._id) {
                            allEvents[i].note = $('#update-note').val();
                        }
                    }

                    destroyModel($('#updateNoteModal'));
                    obj.$fullCalenderObj.fullCalendar('removeEvents');
                    obj.$fullCalenderObj.fullCalendar('addEventSource', allEvents);
                    obj.$fullCalenderObj.fullCalendar('rerenderEvents' );
                    
                    return false;
                }
                var url = $this.attr('action');
                var params = $this.serialize();
                params += "&assignment_id=" + event.assignment_id;
           
                var promise = $.ajax({
                    url: url,
                    dataType: "json",
                    type: 'POST',
                    data: params,
                    beforeSend: function() {
                        showLoader();
                    }
                });

                promise.done(function(response, textStatus, jqXHR) {

                    destroyModel($('#updateNoteModal'));

                    if(response.error == false) {
                        var date = obj.$fullCalenderObj.fullCalendar('getDate');
                        var startDateOfMonth = moment(date).format("YYYY-MM-01")
                        obj.getAssignmentsRequest(startDateOfMonth, selectedStudentId, null, true);
                    } else {
                        alert(response.message);
                    }


                }).always(function() {
                    hideLoader();
                });

                return false;   
            });
        },
        this.openAssignmentAttendanceModal = function(event){
            var obj = this;
            var selectedStudentId = obj.$saveAssignmentForm.find('.selected-student-id').val();
            var assignmentDate = moment(event.start).format('DD-MM-YYYY');
            
            $('#assignmentAttendanceModal').modal();
            $('#attendance-assignment-id').val(event.assignment_id);
            
            $('.present-radio').prop('checked', true);

            $('#assignmentAttendanceRequestForm').off('submit');
            $('#assignmentAttendanceRequestForm').on('submit', function(){
                var $this = $(this);
                
                $('#assignmentAttendanceModal').modal('hide');
                
                var url = $this.attr('action');
                var params = $this.serialize();
                params += '&student_id='+selectedStudentId+'&teacher='+event.teacher+'&assignment_date='+assignmentDate;

                var promise = $.ajax({
                    url: url,
                    dataType: "json",
                    type: 'POST',
                    data: params,
                    beforeSend: function() {
                        showLoader();
                    }
                });

                promise.done(function(response, textStatus, jqXHR) {
                    
                    if(response.error == false) {
                        var date = obj.$fullCalenderObj.fullCalendar('getDate');
                        var startDateOfMonth = moment(date).format("YYYY-MM-01")
                        obj.getAssignmentsRequest(startDateOfMonth, selectedStudentId, null, true);
                    } else {
                        alert(response.message);
                    }


                }).always(function() {
                    hideLoader();
                });

                return false;
            });
        },
        this.getDayEvents = function(date) {
            var dayEvents = $('#full-calendar').fullCalendar( 'clientEvents', function(event){
                return moment(event.start).isSame(date,'day');
            });

            return dayEvents;
        },
        this.isBlackoutDay = function(date, blackoutDays) {
            var obj = this;
            date = moment(date).format('YYYY-MM-DD');
            
            flag = false;
            for (var i = 0; i < obj.blackoutDaysData.length; i++) {
                if (date == obj.blackoutDaysData[i].date
                    && obj.blackoutDaysData[i].building_id == obj.selectedStudentSchoolId) {
                    flag = true;
                }
            }

            // return blackoutDays.includes(date);
            return flag;
        }

    };
}

//Hide Show loader when ajax call
$( function() {

    $(document).ajaxStart(function(){
        showLoader();
    }); 

    $(document).ajaxComplete(function(){
        hideLoader();
    });

    //on teacher modal close reset inputs
    $('#selectTeacherModal').on('hidden.bs.modal', function (e) {
      $(this).find("input[type=text],textarea,select")
           .val('')
           .end();
    })

});

/*
 * Check is working day or not
 */
function isWorkingDays(date) {
    //If the event is on a Saturday and Sunday, don't render it.
    if(moment(date).isoWeekday() == 6 
        || moment(date).isoWeekday() == 7) {

        return false;
    }

    return true;
}

/*
 * Check date has any event or not
 */
function IsDateHasEvent(date) {
    var allEvents = [];
    allEvents = $('#full-calendar').fullCalendar('clientEvents');
    
    var event = $.grep(allEvents, function (v) {
        return +v.start === +date;
    });

    return event.length > 0;
}

/*
 * Check date has selected event or not
 */
function IsDateHasSelectedEvent(date) {
    var allEvents = [];
    allEvents = $('#full-calendar').fullCalendar('clientEvents');
    var event = $.grep(allEvents, function (v) {

        if (v.color == 'red') {
            return ;
        }
        
        return +v.start === +date;

    });

    return event.length > 0 
            && (event[0].group_id == 'undefined' 
                || event[0].group_id == null 
                && event[0].assignment_unassigned == null);
}

/**
 * check day has half day and select or saved events
 */
function isDateHasSelectedMultipleEvent(events) {
    var flag = false;

    if (events.length == 2) {
        for (var i = 0; i < events.length; i++) {
            if ((!events[i].allDay && events[i].color != 'red')
                || events[i].color == 'green') {
                flag = true;
            } else {
                flag = false;
            }
        }
    }

    return flag;   
}

/*
 * close modal and empty all the inputs.
 */
function destroySelectModel() {
    $("#selectTeacherModal").modal('hide');
    $("#selectTeacherModal").on('hidden.bs.modal', function () {
        $(this).find('input[type=text]').val(''); //set date in modal
        $(this).data('bs.modal', null);
    });
}

function destroyModel(element) {
    element.modal('hide');
    element.on('hidden.bs.modal', function () {
        $(this).find('input[type=text]').val(''); //set date in modal
        $(this).find('#assignment-note').val('');
        $(this).data('bs.modal', null);
    });
}

