<?php

if(!isset($_SESSION)) {
     session_start();
}
include_once('Constants.php');
// include_once "Student.php";
include_once "Mailer.php";

class Teacher
{
	private $dbh;

	function __construct()
	{
		include($_SERVER['DOCUMENT_ROOT'].'/dbConnection.php');
		$this->dbCon = $dbh;
	}

	/**
	 * get list of teachers.
	 * 
	 * @param  $teacherName: string of teacher name by which get list of teachers.
	 * @return array of teachers.
	 */
	public function getTeachers($teacherName, $buildingId=null)
	{
		$buildingId = (array)$buildingId;
		if (!$buildingId) {
			$buildingId = (array)Common::getSchoolId();
		}
		$schoolId = (array)Common::getSchoolId();

		// if get teachers list for student then get building code using school id.
		if(Common::isStudent()) {
			$schoolId = (array)Common::getSchoolIdByBuildingCode(Common::getSchoolId());
		}

		$query = "SELECT * FROM teachers WHERE 1=1";
		if ($teacherName) {
			$query .= " AND full_name ILIKE '%$teacherName%'";
		}

		// is logged in user has building id 
		// then get that perticular builder teachers.
		if($buildingId) {
			$ids = implode(',', array_merge($buildingId, $schoolId));
			$query .= " AND building_id in ($ids)";
		}

		$SQLresult = pg_query($this->dbCon, $query);

		$teachers = [];
		if(pg_numrows($SQLresult)) {
			$teachers = pg_fetch_all($SQLresult);
		}
		echo json_encode($teachers);
	}

	/**
	 * get teacher detail by id
	 * @param  int  $id
	 * @return object $teacher
	 */
	public function getTeacherById($id)
	{
		if(empty($id)) {
			return false;
		}
		
		$query = "SELECT * FROM teachers WHERE id = '$id'";
		
		$SQLresult = pg_query($this->dbCon, $query);

	   	$teacher = [];
	   	if(pg_numrows($SQLresult)) {
			$teacher = pg_fetch_object($SQLresult);
		}

		return $teacher;
	}

	/**
	 * Send assignment email to teachers
	 * @param  array  $teacherIds  Selected teacher ids
	 * @param  arrat  $assignments
	 */
	public function sendEmail($teacherIds, $assignments)
	{

		$mail = new Mailer;
		
		foreach ($teacherIds as $key => $id) {
			$teacher = $this->getTeacherById($id);

			$records = [];
			foreach ($assignments as $key => $assignment) {
				$date = $assignment['date'];
				$student = Student::getStudentById($assignment['student_id']);

				if ($assignment['teacher_id'] == $id) {
					$data = "<tr>
						<td>$date</td>
						<td>$student->full_name</td>
					</tr>";

					array_push($records, $data);
				}
			}

			$records = implode(" ",$records);

			$email = $teacher->username . Common::LIBERTY_EMAIL_DOMAIN;
			
			$enviroment = Common::APP_ENV;
			if ($enviroment == 'local') {
				$email = Common::TEACHER_TEST_EMAIL;
			}
			
			$mail->addAddress($email);
			$mail->sendAssignmentMailToTeacher($teacher, $records);
		}

	}
}