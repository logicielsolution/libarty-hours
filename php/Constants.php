<?php

class Common
{	
	// Roles
	const SUPER_ADMIN 	= 1;
	const ADMIN 		= 2;
	const TEACHER 		= 3;
	const STUDENT 		= 4;

	// Buildings
	const LIBERTY_HIGH_SCHOOL = 3;
	const LIBERTY_NORTH_HIGH_SCHOOL = 22;

	// enviroment
	const APP_ENV = 'local'; // local/live

	// emails
	const STUDENT_TEST_EMAIL = 'gurpreet.singh@logicielsolutions.co.in'; 
	const TEACHER_TEST_EMAIL = 'testdata777+teacher@gmail.com';

	// For Staging server
   	/*const STUDENT_TEST_EMAIL = 'rthaler@student.lps53.org';
   	const TEACHER_TEST_EMAIL = 'rthaler@liberty.k12.mo.us'; */
	
	// email domain
	const LIBERTY_EMAIL_DOMAIN = '@lps53.org';

	//Local Database configration
	const HOST 				= 'liberty-school-postgres';
	const DB_NAME 			= 'liberty_school';	
	const DB_USER 			= 'dbadmin';
	const DB_USER_PASSWORD 	= 'root';
	
	//Live Database configration
	/*const HOST 				= 'localhost';
	const DB_NAME 			= 'liberty_school';	
	const DB_USER 			= 'postgres';
	const DB_USER_PASSWORD 	= 'root';*/

	// request status
	const STATUS_PENDING 	= 'pending';
	const STATUS_ACCEPT 	= 'accept';
	const STATUS_DENY 		= 'deny';

	//student request type
	const GROUP_REQUETS 		= 'group';
	const ASSIGNMENT_REQUETS 	= 'assignment';

	//Assignment Type
	const FULL_DAY_ASSIGNMENT = 'full-day';
	const FIRST_HALF_ASSIGNMENT = 'first-half';
	const SECOND_HALF_ASSIGNMENT = 'second-half';

	// Assignment request allow deny
	const ASSIGNMENT_REQUETS_ALLOW 	= 'allow';
	const ASSIGNMENT_REQUETS_DENY	= 'deny';

	//pages constants
	const PAGE_ASSIGNMENTS_LIST 	= 'assignment-list';
	const PAGE_STUDENT_ASSIGNMENTS 	= 'student-assignments';
	const PAGE_ASSIGN_STUDENT 		= 'assign-student';
	const PAGE_TEACHER_CALENDER 	= 'teacher-calender';
	const PAGE_ABSENCES 			= 'absences';
	const PAGE_CLUBS 				= 'clubs';
	const PAGE_BLACKOUT_DAYS 		= 'blackout-days';
	const PAGE_STUDENT_REQUEST 		= 'student_request_history';
	const PAGE_DETENTION_LIST 		= 'detention_list';
	const PAGE_ASSIGNMENT_REPORTLIST= 'assignment_report_list';
	const PAGE_CLUB_REPORT			= 'club_report';

	//report types
	const REPORT_TYPE_ASSIGNMENT = 'assignment';
	const REPORT_TYPE_CLUB 		= 'club';


	private $dbh;

	function __construct($con=null)
	{
		if ($con) {
			$this->dbCon = $con;
		} else {
			include('dbConnection.php');
			$this->dbCon = $dbh;
		}
	}

	public function isSuperAdmin()
	{
		return isset($_SESSION['role']) ? $_SESSION['role'] == Common::SUPER_ADMIN : false;
	}

	public function isAdmin()
	{
		return isset($_SESSION['role']) ? $_SESSION['role'] == Common::ADMIN : false;
	}

	public function isTeacher()
	{
		return isset($_SESSION['role']) ? $_SESSION['role'] == Common::TEACHER : false;
	}
	
	public function isStudent()
	{
		return isset($_SESSION['role']) ? $_SESSION['role'] == Common::STUDENT : false;
	}


	/**
	 * set user role is user Super Admin/Admin/Teacher/Student
	 * 
	 * @param array $user user detail
	 */
	public function setUserRole($user)
	{
		$schoolId = $_SESSION['school_id'];
		
		$_SESSION['role'] = self::TEACHER;
		$query = "SELECT job_title_id FROM jobs WHERE user_id='".$user[0]."'";
            
        $SQLresult2 = pg_exec($this->dbCon, $query) or die("My query faild: " . $query);
        while($jobs = pg_fetch_row($SQLresult2)) {

            if($jobs[0] == '41' && empty($schoolId)) {
                $_SESSION['user_type'] = "super_admin";
                $_SESSION['role'] = self::SUPER_ADMIN;
                break;
            } else if($jobs[0] == '41' && !empty($schoolId)) {
                $_SESSION['user_type'] = "admin";
                $_SESSION['role'] = self::ADMIN;
                break;
            } else if($jobs[0] == 1) {
            	$_SESSION['user_type'] = "teacher";
                $_SESSION['role'] = self::TEACHER;
            }
        }

        /*if($schoolId) {
        	$query = "select * from schools where id = $schoolId";
        	$schoolQuery = pg_exec($this->dbCon, $query);
        	$result = pg_fetch_assoc($schoolQuery);

        	if($result) {
        		$_SESSION['school_id'] = $result['sasi_building_code'];
        	}
        	
        }*/
	}

	/**
	 * get school id from session.
	 * 
	 * @return integer of school id.
	 */
	public function getSchoolId()
	{
		return isset($_SESSION['school_id']) ? $_SESSION['school_id'] : null;
	}

	/**
	 * Get all schools
	 * @return json  response
	 */
	public function getSchools()
	{
		$query = "SELECT * FROM schools";
		$SQLresult = pg_query($this->dbCon, $query);

	   	$schools = [];
	   	if(pg_numrows($SQLresult)) {
	      	$schools = pg_fetch_all($SQLresult);
		}

		return $schools;	
	}


	/**
	 * Get building names match with id
	 * @param  int  $buildingId
	 * @return names;
	 */
	public function getBuildingNames($buildingId)
	{	
		$schools = self::getSchools();
		
		foreach ($schools as $key => $school) {
			if ($school['sasi_building_code'] == $buildingId) {
				return $school['school_name'];
			}
		}
	}

	/**
	 * Get sasi builiding code
	 * @param  int  $buildingId
	 * @return code;
	 */
	public function getBuildingCodeById($schoolId)
	{	
		$schools = self::getSchools();
		
		foreach ($schools as $key => $school) {
			if ($school['id'] == $schoolId) {
				return $school['sasi_building_code'];
			}
		}
	}

	/**
	 * Get sasi builiding code
	 * @param  int  $buildingId
	 * @return code;
	 */
	public function getSchoolIdByBuildingCode($buildingCode)
	{	
		$schools = self::getSchools();
		
		foreach ($schools as $key => $school) {
			if ($school['sasi_building_code'] == $buildingCode) {
				return $school['id'];
			}
		}
	}

	/**
	 * get user detail by id
	 * @param  int  $id
	 * @return object $user
	 */
	public function getUserById($id)
	{
		if(empty($id)) {
			return false;
		}
		
		$query = "SELECT * FROM users WHERE id = $id";
		
		$SQLresult = pg_query($this->dbCon, $query);

	   	$user = [];
	   	if(pg_numrows($SQLresult)) {
			$user = pg_fetch_object($SQLresult);
		}

		return $user;
	}
	
   	public function assignmentType($val)
   	{
      
      	if( $val == self::SECOND_HALF_ASSIGNMENT) {
      		return "Second Half";
      	}
      	else if( $val == self::FIRST_HALF_ASSIGNMENT) {
      		return "First Half";
      	} else {
      		return "Full Day";
      	}
   	 
   	}

   	/**
   	 * check is teacher has club or not 
   	 * @param  int  $teacherId  teacher id
   	 * @return boolean
   	 */
   	public function isTeacherHasClub($teacherId)
	{
		if (!$teacherId) {
			return false;
		}
		
		$query = "SELECT atg.* from admin_to_group as atg inner join groups as g on atg.group_id = g.id  where atg.admin_id=$teacherId";
		
		$SQLresult = pg_query($this->dbCon, $query);

		$count = 0;
	   	if(pg_numrows($SQLresult)) {
			$count = pg_num_rows($SQLresult);
		}

		return ($count > 0) ? true : false;
	}
   


}


