<?php
session_start();

include("header.php");

// check page permission
Authentication::hasPermission(Common::PAGE_ABSENCES);

?>
<?php function entry_html($title, $dbh) { ?>

<div class="">

    <!-- Search teacher -->
    <div style="margin-top: 50px;">
       <h4><b>Absent Students</b></h4>
    </div>

    <hr>

    <!-- filter -->
     <form class="form-inline" style="margin-top: 20px;">
        <div class="form-group">
            <label for="sName">Student Name:</label>
            <input type="text" 
                class="form-control" 
                id="sName"
                name="sName"
                value="<?php echo isset($_GET['sName']) ? $_GET['sName'] : null; ?>">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
        <a class="btn btn-default" href="absent-students.php">  Reset </a>
    </form> 

    <!--Start Table -->
     <div style="margin-top: 25px;">
        <table class="table table-bordered" style="margin-top: 20px;">
            <colgroup>
                <col style="width: 10%;" />
                <col style="width: 35%;" />
                <col style="width: 25%;" />
                <col style="width: 20%;" />
                <?php if (!Common::isTeacher()) { ?>
                    <col style="width: 10%;" />
                <?php } ?>
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Student Name</th>
                    <th>Assignment Date</th>
                    <th>School Name</th>
                    <?php if (!Common::isTeacher()) { ?>
                        <th class="text-center">Action</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                    $commonObj = new Common;

                    $sName = isset($_GET['sName']) ? $_GET['sName'] : null;
                    
                    $buildingId = Common::getSchoolId();
                    $buildingId = $commonObj->getBuildingCodeById($buildingId);

                    $query="SELECT s.id,
                                    s.full_name as student_name, 
                                    a.id as assignment_id, 
                                    a.date, 
                                    a.detention,
                                    sc.school_name 
                            FROM students as s 
                            INNER JOIN assignments as a on cast(a.student_id as int) = s.id
                            INNER JOIN schools as sc on sc.sasi_building_code = s.building_id
                            WHERE a.absent = 't'";

                    if ($buildingId) {
                        $query .= " AND s.building_id=$buildingId";
                    }

                    if (Common::isTeacher()) {
                        $teacherId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
                        $query .= " AND a.teacher_id=$teacherId";
                    }

                    if ($sName) {
                        $query .= " AND s.full_name ILIKE '$sName%'";
                    }

                    $query .= " ORDER BY a.date desc";
                    
                    $result = pg_query($dbh, $query);
                    if (!$result) {
                        echo "An error occurred.\n";
                        exit;
                    }

                    $records = pg_fetch_all($result);

                    if ($records) {
                        foreach ($records as $key => $record) {
                            
                ?>
                    <tr>
                        <td class="assigned-admin-id"><?php echo $record['id']; ?></td>
                        <td><?php echo $record['student_name']; ?></td>
                        <td><?php echo date("d-m-Y", strtotime($record['date'])); ?></td>
                        <td><?php echo $record['school_name']; ?></td>
                        <?php if (!Common::isTeacher()) { ?>
                            <td class="text-center">
                                <?php if ($record['detention'] == 't') { ?>
                                    <button class="btn btn-danger btn-xs disabled">
                                        Detentioned
                                    </button>
                                <?php  } else {  ?>
                                <button class="btn btn-danger btn-xs move-to-detention-btn" 
                                    data-assignment-id="<?php echo $record['assignment_id']; ?>"
                                    data-student-id="<?php echo $record['id']; ?>">
                                    Move to Detention
                                </button>
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php 
                        }
                    } else {
                        $colspan = 5;
                        if (Common::isTeacher()) {
                            $colspan = 4;
                        }
                        echo "<tr><td colspan=$colspan class='text-center'>No Record Found</td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
    <!--End Table -->

</div>

<?php
}
?>


<?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != "") { 
        if ($_SESSION['school_id'] == Common::LIBERTY_NORTH_HIGH_SCHOOL){
            $libertyEagle = "Eagle" ;
        }elseif($_SESSION['school_id'] == Common::LIBERTY_HIGH_SCHOOL){
            $libertyEagle = "Liberty";
        }elseif ($_SESSION['school_id'] != ""){
            $libertyEagle  = "Liberty/Eagle";
        }else{
            $libertyEagle = "No School";
        }
   
        if(!isset($libertyEagle)) {
            $libertyEagle = "";
        }

        $title = "$libertyEagle Hour - Absence";

        gen_header($title);
       
        entry_html($title, $dbh);

    } else {
        header('Location: login.php');
        exit;
    }
    include("footer.php");
?>